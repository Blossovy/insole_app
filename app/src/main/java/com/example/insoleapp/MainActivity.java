package com.example.insoleapp;

import android.Manifest;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.ParcelUuid;
import android.provider.Settings;
import android.util.Log;
import android.view.Menu;
import android.widget.ImageButton;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.example.insoleapp.Fragment.AnalysisFragment;
import com.google.android.material.navigation.NavigationView;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MainActivity extends AppCompatActivity implements AnalysisFragment.ImportFileListener {

    private AppBarConfiguration mAppBarConfiguration;
    public static ExportData exportData;
    private ShoeInfo shoeInfo = ShoeInfo.getInstance();
    private static SensorData sensorData = SensorData.getInstance();
    private static AnalysedData analysedData = AnalysedData.getInstance();
    private static final String TAG = "Main Activity";

    //bind bluetooth service
    public BluetoothService btService = null;
    public boolean btBound = false;
    public static Handler mHandler = null;

    //User interface
    private ImageButton btLeft;
    private ImageButton btRight;

    //Bluetooth
    BluetoothSocket btSocket1;
    OutputStream outputStream1;
    InputStream inputStream1;

    BluetoothSocket btSocket2;
    OutputStream outputStream2;
    InputStream inputStream2;

    public static boolean isBtConnectedL = false;
    public static boolean isBtConnectedR = false;

    public static final int BLUETOOTH_CONNECTION_FAILED = 1;
    public static final int BLUETOOTH_CONNECTED = 2;
    public static final int BLUETOOTH_SOCKET_NULL = 3;
    public static final int BLUETOOTH_CONNECTION_ERROR = 4;

    public static final int MODE_UNKNOWN = 0;
    public static final int MODE_SOFT = 1;
    public static final int MODE_MED = 2;
    public static final int MODE_STIFF = 3;

    public static final int BATTERY_UNKNOWN = 0;
    public static final int BATTERY_LOW = 1;
    public static final int BATTERY_MED = 2;
    public static final int BATTERY_FULL = 3;

    // for extracting analysis data i.e. 10 sec valid data every 5 min
    public static boolean analysisOn = false; // controlled through user interface, Analysis Fragment
    public static boolean manualSMVClock = false;
    public static boolean clock5min = false; // triggered by the 5-min Clock
    public static boolean clock1min = false; // triggered by the 5-min Clock
    public static boolean isNextIn5min = true;
    public static boolean dataCheckerFail = false;  //if re-collection of data is needed, i.e. the previous collected 10s is not valid.
    public static boolean dataLogDelayed = false; // if checkerfail is true even until next clock5min arrives
    private static Runnable analysisRunnable;
    private static Handler analysisHandler = new Handler();

    private static Runnable analysisCountRunnable;
    private static Handler analysisCountHandler = new Handler();
    private static Runnable analysisCountRunnable_1min;
    private static Handler analysisCountHandler_1min = new Handler();
    public static boolean analysisInProcess = false;
    private static int numAnalysisLoop = 0;
    private static boolean firstAnalysisSet = true;
    public static boolean analysisDebug = false;
    public static File directory ;
    public static int analysisNextInSec = 5*60*1000;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        btLeft = findViewById(R.id.BtLeft);
        btRight = findViewById(R.id.BtRight);
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home, R.id.nav_shoe, R.id.nav_signal, R.id.nav_analysis, R.id.nav_bluetooth)
                .setDrawerLayout(drawer)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);
        setFilter();
        logCat();
        setHandler();
        analysisLoop();
        if(exportData == null)
            exportData = new ExportData(this);
        directory= new File(getExternalFilesDir(null) + "/Insole");

    }
    // for reading file from external storage
    @Override
    public void importFile() {
        if (isPermissionGranted()){
            readFile();
        }else{
            takePermission();
        }
    }


    private boolean isPermissionGranted() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            return Environment.isExternalStorageManager();
        } else {
            int readExternalStorage = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
            return readExternalStorage== PackageManager.PERMISSION_GRANTED;
        }
    }

    private void takePermission(){
        if(Build.VERSION.SDK_INT == Build.VERSION_CODES.R){
            try {
                Intent intent = new Intent(Settings.ACTION_MANAGE_APP_ALL_FILES_ACCESS_PERMISSION);
                intent.addCategory("android.intent.category,DEFAULT");
                intent.setData(Uri.parse(String.format("package%s",getApplicationContext().getPackageName())));
                startActivityForResult(intent, 100);
            }catch (Exception exception){
                Intent intent = new Intent();
                intent.setData(Uri.parse(String.format("package%s",getApplicationContext().getPackageName())));
                startActivityForResult(intent, 100);
            }
        }else{
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 100);
        }
    }

    private void readFile(){
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("text/plain");
        intent.setFlags(intent.FLAG_GRANT_READ_URI_PERMISSION);
        startActivityForResult(intent, 102);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, "request code: " + requestCode);
        if (resultCode == RESULT_OK){
            if (requestCode == 100){
                if (Build.VERSION.SDK_INT == Build.VERSION_CODES.R){
                    if (Environment.isExternalStorageManager()){
                        readFile();
                    }else{
                        takePermission();
                    }
                }
            }else if (requestCode == 102){
                if(data != null){
                    Uri url = data.getData();
                    if (url != null){
                        Log.d(TAG,"path is " + url.getPath());
                        //StringBuilder sb = new StringBuilder();
                        try{
                            //for reading sensor data from file
                            int findIndex = url.getPath().lastIndexOf("/");
                            String directory_sub = url.getPath().substring(findIndex+1);
                            File textFile = new File(directory + "/" + directory_sub);
                            Log.d(TAG,"open file " + directory + directory_sub);
                            FileInputStream fis = new FileInputStream(textFile);

                            if (fis != null){
                                InputStreamReader isr = new InputStreamReader(fis);
                                BufferedReader buff = new BufferedReader(isr);
                                    // for file of raw sensor data of about 10 sec
                                if (MainActivity.analysisDebug) {

                                    SensorData.dataLogProcess = true;
                                    //analysisLoopCount5min();  //activate 5-min clock
                                    exportData.startLogging_analysis();
                                    analysisOn = true;
                                    String line = null;

                                    while ((line = buff.readLine()) != null) {
                                        SensorData.parseDataL(line);
                                    }
                                }

                                // for file of processed sensor data
                                if (!MainActivity.analysisDebug){
                                    Log.d(TAG,"Not Debug Load file");
                                    String line , last = null;
                                    while ((line = buff.readLine()) != null) {
                                        last = line;
                                    }
                                    Log.d(TAG,"last: "+last);
                                    analysedData.resetAnalysisVar();
                                    analysedData.parseAnalysisData(last);

                                }
                                fis.close();
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length >0){
            if (requestCode == 101){
                boolean readExternalStorage = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                if (readExternalStorage){
                    readFile();
                }else{
                    takePermission();
                }
            }
        }
    }
    //read file ends

    //bound bluetooth service
    @Override
    protected void onStart() {
        super.onStart();
        Intent intent = new Intent(this, BluetoothService.class);
        bindService(intent, connection, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (btService != null) {
            unbindService(connection);
            btBound = false;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        closeBT(1);
        closeBT(2);

    }

    private ServiceConnection connection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            BluetoothService.LocalBinder binder = (BluetoothService.LocalBinder) service;
            btService = binder.getService();
            btBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            btBound = false;
        }
    };


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
//
//    @Override
//    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
//        switch (item.getItemId()) {
//            case R.id.action_export:
//                if (exportData == null) {
//                    exportData = new ExportData(this);
//                }
//                exportData.exportAll(true);
//                exportData.exportAll(false);
//                break;
//            default:
//                return super.onOptionsItemSelected(item);
//        }
//        return true;
//    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }


    public static void analysisLoop(){
        Log.d(TAG, "analysis loop starts");
        analysisRunnable = new Runnable() {
            @Override
            public void run() {
                try {
                   if (analysisOn) {
                       if (analysisLogNeeded()) {
                           Log.d(TAG, "analysisLogNeeded");
                           //sensorData.resetAnalysisL();
                           //dataCheckerFail = false;
                          // SensorData.dataLogProcess = true;
                           //MainActivity.analysisInProcess = true;
                           //Log.d(TAG, "dataLogProcess " + SensorData.dataLogProcess );
                           //sensorData.smvCount = 0;
                           //clock5min = false;
                           if (firstAnalysisSet) {
                               Log.d(TAG, "first entry");
                               firstAnalysisSet = false;
                           }
                       }
                   }
                }finally {
                    analysisHandler.postDelayed(this, 1000*15);
                }
            }
        };
        analysisRunnable.run();
    }

    public static void analysisLoopCount5min(){ // counting for 5 mins
        Log.d("MainAct Ana", "5min Clock starts");
        analysisCountRunnable = new Runnable() {
            @Override
            public void run() {
                    clock5min = true;
                    analysisCountHandler.postDelayed(this, 60*1000*5);
            }
        };
        analysisCountRunnable.run();
    }
    public static void analysisLoopStop5min(){ // stop counting for 5 mins
        if (analysisCountRunnable != null) {
            analysisCountHandler.removeCallbacks(analysisCountRunnable);
        }
    }

    public static void analysisLoopCount1min(){ // counting for 5 mins
        Log.d("MainAct Ana", "5min Clock starts");
        analysisCountRunnable_1min = new Runnable() {
            @Override
            public void run() {
                clock1min = true;
                analysisCountHandler_1min.postDelayed(this, 60*1000);
            }
        };
        analysisCountRunnable_1min.run();
    }
    public static void analysisLoopStop1min(){ // stop counting for 1 mins
        if (analysisCountRunnable_1min != null) {
            analysisCountHandler_1min.removeCallbacks(analysisCountRunnable_1min);
        }
    }
    public static boolean analysisLogNeeded(){
        //4 cases considering 5min-clock On, and if data check failed
       // Log.d(TAG, "check if log3rdfiltered needed: analysisInPrcess " +analysisInProcess+ "dataCheckerFail: "+ dataCheckerFail + " clock5min: " + clock5min);
        if (analysisInProcess){
            return false;
        }else {
          //  boolean analysisPreCount = (sensorData.smvCount > (sensorData.sensorFreq*(5*60+10)));
            boolean counterOn = false;
            if(isNextIn5min){
                counterOn = clock5min;
            }else{
                counterOn = clock1min;
            }
            if (counterOn && dataCheckerFail) {
                dataLogDelayed = true;
                return true;
            } else if (counterOn && !dataCheckerFail) {
                return true;
            } else if (!counterOn && !dataCheckerFail) {
                return false;
            } else if (!counterOn && dataCheckerFail) {
                return true;
            }
            return false;
        }
    }

//    static void stopAnalysisLog(){
//        new Timer().schedule(new TimerTask() {
//            @Override
//            public void run() {
//                sensorData.dataLogProcess = false;
//                if (sensorData.getAnalysisL() != null)
//                    analysedData.setSensorRaw(SensorData.getAnalysisL());
//
//                exportData.singleEntry_analysis(SensorData.getAnalysisL());
//            }
//        },1000*10);//stop logging data for analysis after 10 sec
//    }

    void logCat() {
        try {
            Log.d(TAG, "file directory: " + directory);
            boolean var = false;
            if (!directory.exists()) {
                var = directory.mkdirs();
            }
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy_hh:mm:ss");
            String filename = simpleDateFormat.format(new Date()) + "logcat.txt";
            Process process = Runtime.getRuntime().exec("logcat -c");
            process = Runtime.getRuntime().exec("logcat -f " + directory.toString() + "/" + filename);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getBTService1(Handler btHandler, BluetoothSocket mmSocket, BluetoothDevice mmDevice, final InputStream mmInputStream, final OutputStream mmOutputStream) {
        mHandler = btHandler;
        btService.setHandler(mHandler);
        if (btService != null) {
            btService.btCommunicateL(mmSocket, mmDevice, mmInputStream, mmOutputStream);
         //   btService.btSend(mmOutputStream);
            Log.d(TAG, "btservice starts reading");
        } else
            Log.d(TAG, "btservice is null");

    }

    public void getBTService2(BluetoothSocket mmSocket, BluetoothDevice mmDevice, final InputStream mmInputStream, final OutputStream mmOutputStream) {

        if (btService != null) {
            btService.btCommunicateR(mmSocket, mmDevice, mmInputStream, mmOutputStream);
            Log.d(TAG, "btservice starts reading");
        } else
            Log.d(TAG, "btservice is null");

    }

    public void passHandler(Handler handler) {
        mHandler = handler;
        btService.setHandler(handler);
    }

    private void setFilter() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(BluetoothDevice.ACTION_ACL_CONNECTED);
        filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECTED);
        this.registerReceiver(mReceiver, filter);
    }

    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
            String deviceName = device.getName();

            if (BluetoothDevice.ACTION_ACL_CONNECTED.equals(action)) {
                if ((shoeInfo.getBluetoothDeviceL() != null) && (shoeInfo.getBluetoothDeviceL().getName().equals(deviceName))) {
                    btLeft.setImageResource(R.drawable.footprint_yellow_lxs);
                    isBtConnectedL = true;
                }
                else if ((shoeInfo.getBluetoothDeviceR() != null) && (shoeInfo.getBluetoothDeviceR().getName().equals(deviceName))) {
                    btRight.setImageResource(R.drawable.footprint_yellow_rxs);
                    isBtConnectedR = true;
                }
                Toast.makeText(context, deviceName + " is connected", Toast.LENGTH_LONG).show();
            } else if (BluetoothDevice.ACTION_ACL_DISCONNECTED.equals(action)) {
                if ((shoeInfo.getBluetoothDeviceL() != null) && (shoeInfo.getBluetoothDeviceL().getName().equals(deviceName))) {
                    btLeft.setImageResource(R.drawable.footprint_grey_lxs);
                    isBtConnectedL = false;
                    try {
                        bluetoothClearL();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                else if ((shoeInfo.getBluetoothDeviceR() != null) && (shoeInfo.getBluetoothDeviceR().getName().equals(deviceName))){
                    btRight.setImageResource(R.drawable.footprint_grey_rxs);
                    isBtConnectedR = false;
                    try {
                        bluetoothClearR();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                Toast.makeText(context, deviceName + " is disconnected", Toast.LENGTH_LONG).show();
            }
        }
    };


    public int openBT(int device, BluetoothDevice bluetoothDevice) {
        if (device == 1) {//left
            shoeInfo.setBluetoothDeviceL(bluetoothDevice);
            if (shoeInfo.getBluetoothDeviceL() != null && shoeInfo.getBluetoothDeviceL().getUuids() != null) {
                ParcelUuid[] uuids = shoeInfo.getBluetoothDeviceL().getUuids();
                Log.d(TAG, "uuid is " + uuids[0].getUuid());
                try {
                    btSocket1 = shoeInfo.getBluetoothDeviceL().createRfcommSocketToServiceRecord(uuids[0].getUuid());
                    btSocket1.connect();
                    outputStream1 = btSocket1.getOutputStream();
                    inputStream1 = btSocket1.getInputStream();
                } catch (IOException e) {
                    e.printStackTrace();
                    Log.d(TAG, "bluetooth socket1 left is not connected");
                    return BLUETOOTH_CONNECTION_FAILED;
                }
                if (SensorData.getDisplay_FSRL() != null) {
                    SensorData.initialiseData();
                }
                getBTService1(mHandler, btSocket1, shoeInfo.getBluetoothDeviceL(), inputStream1, outputStream1);
                Log.d(TAG,"get output stream id: "+ outputStream1);
                isBtConnectedL = true;
                return BLUETOOTH_CONNECTED;

            } else {
                Log.d(TAG, "btDevice1 is null");
                return BLUETOOTH_SOCKET_NULL;
            }

        } else if (device == 2) {//right
            shoeInfo.setBluetoothDeviceR(bluetoothDevice);
            if (shoeInfo.getBluetoothDeviceR() != null && shoeInfo.getBluetoothDeviceR().getUuids() != null) {
                ParcelUuid[] uuids = shoeInfo.getBluetoothDeviceR().getUuids();
                Log.d(TAG, "uuid is " + uuids[0].getUuid());
                try {
                    btSocket2 = shoeInfo.getBluetoothDeviceR().createRfcommSocketToServiceRecord(uuids[0].getUuid());
                    btSocket2.connect();
                    outputStream2 = btSocket2.getOutputStream();
                    inputStream2 = btSocket2.getInputStream();
                } catch (IOException e) {
                    e.printStackTrace();
                    Log.d(TAG, "bluetooth socket2 right is not connected");
                    isBtConnectedR = false;
                    return BLUETOOTH_CONNECTION_FAILED;
                }
                if (SensorData.getDisplay_FSRR() != null) {
                    SensorData.initialiseData();
                }
                getBTService2(btSocket2, shoeInfo.getBluetoothDeviceR(), inputStream2, outputStream2);
                Log.d(TAG,"get output stream id: "+ outputStream2);
                isBtConnectedR = true;
                return BLUETOOTH_CONNECTED;

            } else {
                Log.d(TAG, "btDevice2 is null");
                isBtConnectedR = false;
                return BLUETOOTH_SOCKET_NULL;
            }
        }
        return BLUETOOTH_CONNECTION_ERROR;

    }

    public boolean closeBT(int device) {
        try {
            if (exportData == null) {
                exportData = new ExportData(this);
            }
            if (device == 1) {//left
                if (outputStream1 != null && inputStream1 != null && btSocket1 != null) {
                    btService.stopWorker = true;
                    outputStream1.close();
                    inputStream1.close();
                    btSocket1.close();
                    //exportData.exportAll(true);
                    isBtConnectedL = false;
                    bluetoothClearL();
                    btLeft.setImageResource(R.drawable.footprint_grey_lxs);
                }
            } else if (device == 2) {//right
                if (outputStream2 != null && inputStream2 != null && btSocket2 != null) {
                    btService.stopWorker2 = true;
                    outputStream2.close();
                    inputStream2.close();
                    btSocket2.close();
                    //exportData.exportAll(false);
                    isBtConnectedR = false;
                    bluetoothClearR();
                    btRight.setImageResource(R.drawable.footprint_grey_rxs);
                }
            }
            return true;
        } catch (IOException e) {
            isBtConnectedL = true;
            isBtConnectedR = true;
            e.printStackTrace();
            return false;
        }
    }

    void bluetoothClearL() throws IOException {
        btSocket1 = null;
        inputStream1 = null;
        outputStream1 = null;
        shoeInfo.setBluetoothDeviceL(null);
        shoeInfo.shoeInfoRestL();
        exportData.endLoggingL();
        btService.firstEntry1 = true;
    }
    void bluetoothClearR() throws IOException {
        btSocket2 = null;
        inputStream2 = null;
        outputStream2 = null;
        shoeInfo.setBluetoothDeviceR(null);
        shoeInfo.shoeInfoRestR();
        exportData.endLoggingR();
        btService.firstEntry2 = true;
    }


    public final class MessageConstants {
        public static final int MESSAGE_READ = 0;
        public static final int MESSAGE_WRITE = 1;
        public static final int MESSAGE_CONNECTED = 2;
        public static final int MESSAGE_NO_DATA = 3;
        public static final int MESSAGE_DATA_INVALID = 4;
        public static final int MESSAGE_ADD_ENTRY_L = 5;
        public static final int MESSAGE_ADD_ENTRY_R = 6;
        public static final int MESSAGE_BATTERY_STATUS = 7;
    }


    public void setHandler() {
        mHandler = new Handler() {
            @Override
            public void handleMessage(@NonNull final Message msg) {
                switch ((msg.what)) {
                    case MessageConstants.MESSAGE_BATTERY_STATUS:
                        try {
                            Log.d(TAG, "battery status");
                            Toast.makeText(getApplicationContext()," battery", Toast.LENGTH_SHORT).show();
                        } catch (Exception e) {
                            Log.d(TAG, "handler went wrong: " + e);
                        }
                        break;

                }
            }
        };
    }


}