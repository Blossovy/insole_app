package com.example.insoleapp;

import android.content.Context;
import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import libsvm.svm;
import libsvm.svm_model;
import libsvm.svm_node;
import libsvm.svm_parameter;
import libsvm.svm_problem;

public class SVModel {
    private final static String TAG = "SVModel";
    private static SVModel instance;
    private static svm_node[][] x;
    private static Context context;
    //for test/ phase one data
    public static int testDataSize = 5400;
    public static int featuresNo = 7;
    public static svm_node[][] nodeX;
    public static int nodeXIndex = 0;
    public static double []labelY = new double[testDataSize];
    public static int nodeYIndex = 0;
    public static svm_model experiModel;
    public static AnalysedData analysedData = AnalysedData.getInstance();

    public SVModel() throws IOException {
    }

    public static SVModel getInstance() throws IOException {
        if (instance == null) {
            instance = new SVModel();
            //model is loaded initially --> model 1
            experiModel = svm.svm_load_model(new File(MainActivity.directory + "/saveSVM_P1").toString());
            //experiModel = svm.svm_load_model(new File(MainActivity.directory + "/saveSVM_P3").toString());
        }
        Log.d(TAG, "new SVModel instance created");
        return instance;
    }

    public static svm_model buildModel(svm_node[][] nodes, double [] labels) {
        // Build Parameters
        svm_parameter param = new svm_parameter();
        param.svm_type = svm_parameter.C_SVC;
        param.kernel_type = svm_parameter.LINEAR;
        param.gamma = 1/featuresNo;
        param.nu = 0.5;
        param.C = 1;
        param.cache_size = 1000;
        param.eps = 0.001;
        // Build Problem

        //double[] label = new double[]{-1,-1,1,1};
        svm_problem problem = new svm_problem();
        problem.x = nodes;
        problem.l = nodes.length;
        problem.y = labels;

        // Build Model
        return svm.svm_train(problem, param);
    }
    public static double svmPredict(double[] xtest){
        double yPred;

            svm_node[] nodes = new svm_node[xtest.length];
           // AnalysedData.errorMsg.append("node length:  "+ xtest.length);
            for (int i = 0; i < xtest.length; i++)
            {
                svm_node node = new svm_node();
                node.index = i;
                node.value = xtest[i];
                nodes[i] = node;
            }
            int totalClasses = 2;
            int[] labels = new int[totalClasses];
            svm.svm_get_labels(experiModel,labels);

            double[] prob_estimates = new double[totalClasses];
            yPred = svm.svm_predict_probability(experiModel, nodes, prob_estimates);


        return yPred;
    }
    public static svm_model buildModelFromTxt (){
        svm_model model;
        nodeX = new svm_node[testDataSize][featuresNo];
        for (int i = 0; i < testDataSize; i++) {
            for (int j = 0; j < featuresNo; j++) {
                nodeX[i][j] = new svm_node();
            }
        }
        loadSavedNodes();
        Log.d(TAG, "loaded ");
        model = buildModel(nodeX, labelY);
        Log.d(TAG, "build");
        try {
            File directory = new File(MainActivity.directory + "/saveSVM_P3");
            svm.svm_save_model(directory.toString(), model);

            Log.d(TAG, "SVM model ends");
        }catch (IOException e){
            e.printStackTrace();

            Log.d(TAG, "SVM model build error");
        }
            MainActivity.exportData.smvPredictLog(labelY, svmModelValidate(model));
        return model;
    }

    public static void loadSavedNodes(){
        try{
        File directoryNodeX = new File(MainActivity.directory + "/phaseOne_nodeX3.txt");
        FileInputStream fis = new FileInputStream(directoryNodeX);

            if (fis != null){
                InputStreamReader isr = new InputStreamReader(fis);
                BufferedReader buff = new BufferedReader(isr);
                String line = null;
                while((line = buff.readLine()) != null ){
                    Log.d(TAG, "read: " + line);
                    if (line == null) {
                        Log.d(TAG, "this is null");
                    }else {
                        loadNodeXParse(line);
                    }
                }
                fis.close();
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        try{
            File directoryNodeY = new File(MainActivity.directory + "/phaseOne_nodeY3.txt");
            FileInputStream fis = new FileInputStream(directoryNodeY);

            if (fis != null){
                InputStreamReader isr = new InputStreamReader(fis);
                BufferedReader buff = new BufferedReader(isr);
                String line = null;
                labelY = new double[testDataSize];
                while((line = buff.readLine()) != null){
                        labelY[nodeYIndex] = Integer.parseInt(line);
                        nodeYIndex++;
                }
                fis.close();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static void  loadNodeXParse (String d){

        Pattern dataA = Pattern.compile("-?\\d+(\\.\\d+)?");
        Matcher matcherA = dataA.matcher(d);
            for (int i = 0; i < featuresNo; i++) {
                if (matcherA.find()) {
                    nodeX[nodeXIndex][i].index = i + 1;
                    nodeX[nodeXIndex][i].value = Double.parseDouble(matcherA.group());
            }

        }
        nodeXIndex++;
        Log.d(TAG, "nodeX index: " + nodeXIndex);
    }

    public static void testData(){
        x = new svm_node[4][2];
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 2; j++) {
                x[i][j] = new svm_node();
            }
        }
        x[0][0].index = 1;
        x[0][0].value = 0;
        x[0][1].index = 2;
        x[0][1].value = 0;

        x[1][0].index = 1;
        x[1][0].value = 1;
        x[1][1].index = 2;
        x[1][1].value = 1;

        x[2][0].index = 1;
        x[2][0].value = 0;
        x[2][1].index = 2;
        x[2][1].value = 1;

        x[3][0].index = 1;
        x[3][0].value = 1;
        x[3][1].value = 0;
        x[3][1].index = 2;
        double[] labels = new double[]{-1,-1,1,1};
        svm_model model = buildModel(x, labels);
        try {
            File directory = new File(MainActivity.directory + "/saveSVM");
            svm.svm_save_model(directory.toString(), model);

            Log.d(TAG, "SVM model ends");
        }catch (IOException e){
            e.printStackTrace();

            Log.d(TAG, "SVM model error");
        }
    }

    public static double[] svmModelValidate( svm_model model){
        double[] yPred = new double[labelY.length];

        for(int k = 0; k < testDataSize; k++){

            int totalClasses = 2;
            int[] labels = new int[totalClasses];
            svm.svm_get_labels(model,labels);

            double[] prob_estimates = new double[totalClasses];
            yPred[k] = svm.svm_predict_probability(model, nodeX[k], prob_estimates);

        }
        return yPred;
    }

    public static void setContext (Context c){
        context = c;
    }
    public static Context getContext (){return context;}
}
