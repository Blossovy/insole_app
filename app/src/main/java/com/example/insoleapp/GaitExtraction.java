package com.example.insoleapp;

import android.util.Log;
import android.util.Pair;

import java.util.ArrayList;
import java.util.Arrays;

public class GaitExtraction {
    AnalysedData analysedData = AnalysedData.getInstance();
    int sensorFreq = SensorData.sensorFreq;
    ToolMath toolMath = new ToolMath();
    double[] time;
    double [][] sensorsFSR, sensorsIMU;
    int sensorsLen;
    private static final String TAG = " GaitExtraction";

    public GaitExtraction(double[] timeDiff, double [][] fsr, double[][] imu){
        time = timeDiff;
        sensorsFSR = fsr;
        sensorsIMU = imu;
        sensorsLen = time.length;
    }

    public boolean isWalking_imu (){
        try {

            Log.d(TAG, "is walking imu starts");
            double[] acc_total = new double[sensorsLen];
            int count = 0;
            for (int j = 0; j < sensorsLen; j++) {
                acc_total[j] = Math.pow(sensorsIMU[0][j], 2) + Math.pow(sensorsIMU[1][j], 2) + Math.pow(sensorsIMU[2][j], 2);
                if (acc_total[j] > 10) {
                    count++;
                }
            }
            if (count < 8){
                return false;
            }else
                return true;
        }catch (Exception e){
            AnalysedData.errorMsg.append("gait extraction-isWalking_imu: " + e + "\n");
            e.printStackTrace();
            return false;
        }
    }

    private double thre_force_auto_7(double t_thre, double t_thre1, double t_thre2, double t_thre3, double t_thre4, double t_thre5, double t_thre6, double t_thre7){
        double thre_force =35;
        int n = sensorsFSR[7].length;
        int num_thre1 = 0;
        int num_thre2 = 0;
        int num_thre3 = 0;
        int num_thre4 = 0;
        int num_thre5 = 0;
        int num_thre6 = 0;
        int num_thre7 = 0;

        for (int i = 0; i<n; i++){
            if (sensorsFSR[7][i] >t_thre1) {
                num_thre1++;
                num_thre2++;
                num_thre3++;
                num_thre4++;
                num_thre5++;
                num_thre6++;
                num_thre7++;
            }else if (sensorsFSR[7][i] >t_thre2) {
                num_thre2++;
                num_thre3++;
                num_thre4++;
                num_thre5++;
                num_thre6++;
                num_thre7++;
            }else if (sensorsFSR[7][i] >t_thre3) {
                num_thre3++;
                num_thre4++;
                num_thre5++;
                num_thre6++;
                num_thre7++;
            }else if (sensorsFSR[7][i] >t_thre4) {
                num_thre4++;
                num_thre5++;
                num_thre6++;
                num_thre7++;
            }else if (sensorsFSR[7][i] >t_thre5) {
                num_thre5++;
                num_thre6++;
                num_thre7++;
            }else if (sensorsFSR[7][i] >t_thre6) {
                num_thre6++;
                num_thre7++;
            }else  {
                num_thre7++;
            }
        }
        int num_thre = (int) Math.floor(n*t_thre);

        if (num_thre1 > num_thre){
            thre_force =  t_thre1;
        }else if (num_thre2 > num_thre){
            thre_force =  t_thre2;
        }else if (num_thre3 > num_thre){
            thre_force =  t_thre3;
        }else if (num_thre4 > num_thre){
            thre_force =  t_thre4;
        }else if (num_thre5 > num_thre){
            thre_force =  t_thre5;
        }else if (num_thre6 > num_thre){
            thre_force =  t_thre6;
        }else {
            thre_force =  t_thre7;
        }

        thre_force=35;
        return thre_force;
    }


    public ArrayList<Integer>[] gaitRecognitionRough(){
        try {
            Log.d(TAG, "gait recongnition starts");
           // double y_thre = 15;    //N
            double y_thre = thre_force_auto_7(0.58, 35,30,25,20,15,12,10);
            analysedData.debugMsg.append("GRough ythre: " + y_thre + "\n");
            double contact_t_max = 0.78;    //s
            double contact_t_min = 0.48;    //s
            double stride_t_max = 1.3;
            double stride_t_min = 0.8;
            double t_half = 0.52;

            int m = sensorsLen;

            boolean flag_last_step_check = true;
            ArrayList<Integer>[] gait_n = new ArrayList[2];
            gait_n[0] = new ArrayList<>();
            gait_n[1] = new ArrayList<>();
            int flag_first_step = 0;
            int id_star = 0;
            int id_end = 0;


            for (int ii = 0; ii < m - (int) (0.3 * sensorFreq); ii++) {

                //1.0 first step
                if (flag_first_step < 1) {
                    if (sensorsFSR[7][ii] > y_thre) {
                        continue;
                    }
                }

                if (ii < id_end) { //the step is not fully recognised
                    continue;
                }

                flag_first_step = 1;
                //1.1 max_id
                double[] data_tem = Arrays.copyOfRange(sensorsFSR[7], ii, Math.min(ii + sensorFreq + 1, m));
                int id_tem = toolMath.maxInArray(data_tem).first;
                int id_max = Math.max(ii + id_tem, 0);

                Log.d(TAG, "index ii = " + ii + "index id_max = " + id_max + "id_tem " + id_tem);
                //1.2 start_id
                id_star = 0;
                double[] data_tem2 = Arrays.copyOfRange(sensorsFSR[7], ii, id_max + 1);
                for (int j = data_tem2.length - 1; j >= 0; j--) {
                    if (data_tem2[j] < y_thre) {
                        id_star = ii + j + 1;
                        break;
                    }
                }

                if (id_star > m - 10) {
                    flag_last_step_check = false;
                    break;
                }

                //1.3 end_id
                id_end = 0;
                double[] data_tem3 = Arrays.copyOfRange(sensorsFSR[7], id_max, Math.min(id_max + sensorFreq + 1, m));
                for (int j = 0; j < data_tem3.length; j++) {
                    if (data_tem3[j] < y_thre) {
                        id_end = id_max + j - 1;
                        break;
                    }
                }

                //1.4 add id to gait
                if (id_star == 0 || id_end == 0) {
                    id_end = ii;
                    continue;
                }
                if (id_max - id_star < 2 || id_end - id_star < 2) {
                    continue;
                }
                gait_n[0].add(id_star);
                gait_n[1].add(id_end);

            }

            if (flag_last_step_check && gait_n[0].size() != 0 && gait_n[1].size() != 0) {
                if (time[m - 1] - time[id_end] > t_half && id_star != -1 && time[id_star] - time[gait_n[0].get(gait_n[0].size() - 1)] > stride_t_min && time[id_star] - time[gait_n[0].get(gait_n[0].size() - 1)] < stride_t_max) {
                    gait_n[0].add(id_star);
                    gait_n[1].add(id_star + 2);
                }
            }

            analysedData.debugMsg.append("GRough gait_n: " + gait_n[0].size() + "\n");
            return gait_n;
        }catch (Exception e){
            e.printStackTrace();
            AnalysedData.errorMsg.append("gait extraction-isWalking_imu: " + e + "\n");
            return null;
        }
    }

    public void gaitRecognitionMid (ArrayList<Integer>[] gait_n_seg_rough) {
        try {
            Log.d(TAG, "gait recognition mid starts");
            if (gait_n_seg_rough[0] == null) {
                MainActivity.dataCheckerFail = true;
            } else {
                int n_gait_rough = gait_n_seg_rough[0].size();
                for (int i = 0; i < 8; i++) {
                    for (int j = Math.max(gait_n_seg_rough[0].get(0) - 36, 0); j < Math.max(gait_n_seg_rough[0].get(0) - 6, 0) + 1; j++) {
                        sensorsFSR[i][j] = 0;
                    }
                }
                if (n_gait_rough > 1) {
                    for (int kk = 0; kk < n_gait_rough - 1; kk++) {
                        for (int i = 0; i < 8; i++) {
                            for (int j = gait_n_seg_rough[1].get(kk) + 5; j < gait_n_seg_rough[0].get(kk + 1) - 5 + 1; j++) { // reserve 4 data points at both sides
                                sensorsFSR[i][j] = 0;
                            }
                        }
                    }

                    int n_temp_tmp = sensorsFSR[0].length;
                    if (gait_n_seg_rough[1].get(gait_n_seg_rough[1].size()-1) < n_temp_tmp-1){
                        for (int i = 0; i <8; i++ ){
                            for (int j = Math.min(gait_n_seg_rough[1].get(gait_n_seg_rough[1].size()-1)+5, n_temp_tmp-1); j< n_temp_tmp; j++) {
                                sensorsFSR[i][j] = 0;
                            }
                        }
                    }
                }


                analysedData.setSensorFSR(sensorsFSR);
            }
        }catch (Exception e){
            e.printStackTrace();
            AnalysedData.errorMsg.append("gaitRecognitionMid: " +e + "\n");
        }
    }

    public ArrayList<Integer>[] gaitRecognitionFine(){
        try {
            Log.d(TAG, "gait recognition fine starts");
            double y_thre = 10;    //N
            double contact_t_max = 0.78;    //s
            double contact_t_min = 0.48;    //s
            double stride_t_max = 1.3;
            double stride_t_min = 0.8;
            double t_half = 0.52;

            boolean flag_last_step_check = true;
            ArrayList<Integer>[] gait_n = new ArrayList[2];
            gait_n[0] = new ArrayList<Integer>(); //for start index
            gait_n[1] = new ArrayList<Integer>(); //for end index

            boolean flag_1st_step_found = false;
            int id_end = 0;
            int id_star = 0;
            for (int j = 0; j < sensorsLen - (int) (0.3 * sensorFreq); j++) {
                //1.0: recognise the first step
                if (!flag_1st_step_found) {
                    if (sensorsFSR[7][j] > y_thre)
                        continue;
                }

                //if step recognition is not completed
                if (j < id_end) {
                    continue;
                }

                flag_1st_step_found = true;
                //1.1 max id
                double[] data_temp = Arrays.copyOfRange(sensorsFSR[7], j, Math.min(j + sensorFreq, sensorsLen));
                Pair<Integer, Double> maxP = toolMath.maxInArray(data_temp);
                int id_max = j + maxP.first;

                //1.2 start id
                id_star = 0;
                double[] data_temp2 = Arrays.copyOfRange(sensorsFSR[7], j, id_max + 1);
                for (int k = data_temp2.length - 1; k >= 0; k--) {
                    if (data_temp2[k] < y_thre) {
                        id_star = j + k + 1;
                        break;
                    }
                }

                if (id_star > sensorsLen - 10) {
                    flag_last_step_check = false;
                    break;
                }

                //1.3 end id
                id_end = 0;
                double[] data_temp3 = Arrays.copyOfRange(sensorsFSR[7], id_max, Math.min(id_max + sensorFreq, sensorsLen));
                for (int k = 0; k < data_temp3.length; k++) {
                    if (data_temp3[k] < y_thre) {
                        id_end = id_max + k - 1;
                        break;
                    }
                }

                //1.4 add id to gait
                if (id_end == 0 || id_star == 0) {
                    id_end = j;
                    continue;
                }

                if (id_max - id_star < 2 || id_end - id_star < 2) {
                    continue;
                }
                if (time[id_end] - time[id_star] < contact_t_max && time[id_end] - time[id_star] > contact_t_min) {
                    gait_n[0].add(id_star);
                    gait_n[1].add(id_end);
                }

            }
            // for the very last uncompleted step
            if (flag_last_step_check && gait_n[0].size() != 0 && gait_n[1].size() != 0) {
                if (time[sensorsLen - 1] - time[id_end] > t_half && id_star != -1 && time[id_star] - time[gait_n[0].get(gait_n[0].size() - 1)] > stride_t_min && time[id_star] - time[gait_n[0].get(gait_n[0].size() - 1)] < stride_t_max) {
                    gait_n[0].add(id_star);
                    gait_n[1].add(id_star + 2);
                }
            }
            analysedData.debugMsg.append("GAccur gait_n: " + gait_n[0].size() + "\n");
            return gait_n;
         }catch (Exception e){
            e.printStackTrace();
            AnalysedData.errorMsg.append("gaitRecognitionMid: " +e + "\n");
            return null;
        }

    }

    public boolean isGaitRegValid (ArrayList<Integer>[] gait){
        analysedData.debugMsg.append("GValid gait: " + gait[0].size() + "\n");
        if (gait[0].size() < 3)
            return false; // need to get another valid 10-sec to process if valid steps are less than 3
        else
            return true;
    }
}
