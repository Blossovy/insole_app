package com.example.insoleapp.Fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.example.insoleapp.PersonalInfo;
import com.example.insoleapp.R;

public class PersonalFragment extends Fragment {

    private EditText nameET, ageET, weightET, heightET;
    private RadioGroup genderRadioGp;
    private RadioButton maleRadio, femaleRadio;
    private Button saveBtn, clearBtn;
    private PersonalInfo personalInfo;
    private final String TAG = "Personal Frag";

    public View onCreateView(@NonNull LayoutInflater inflater,
            ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_personal, container, false);
        nameET = root.findViewById(R.id.personal_name);
        ageET = root.findViewById(R.id.personal_age);
        weightET = root.findViewById(R.id.personal_weight);
        heightET = root.findViewById(R.id.personal_height);
        genderRadioGp = root.findViewById(R.id.gender);
        maleRadio = root.findViewById(R.id.male);
        femaleRadio = root.findViewById(R.id.female);
        saveBtn = root.findViewById(R.id.saveBtn);
        clearBtn = root.findViewById(R.id.clearBtn);

        personalInfo = PersonalInfo.getInstance();
        nameET.setText(personalInfo.getName());
        if (personalInfo.getAge() != 0) {
            ageET.setText(String.valueOf(personalInfo.getAge()));
        }
        if (personalInfo.getWeight() != 0) {
            weightET.setText(String.valueOf(personalInfo.getWeight()));
        }
        if (personalInfo.getHeight() != 0) {
            heightET.setText(String.valueOf(personalInfo.getHeight()));
        }
        Log.d(TAG, "start = " + personalInfo.getGender());
        if(personalInfo.getGender() == false){
            maleRadio.setChecked(true);
        }else
            femaleRadio.setChecked(true);

        clearBtn.setOnClickListener((new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                nameET.setText("");
                ageET.setText("");
                weightET.setText("");
                heightET.setText("");
                maleRadio.setChecked(true);
                saveInfo();
            }
        }));

        saveBtn.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
//               if (nameET.getText().toString().isEmpty() || ageET.getText().toString().isEmpty() || weightET.getText().toString().isEmpty()){
//                   Toast.makeText(getActivity(), R.string.personal_no_data_alert, Toast.LENGTH_SHORT).show();
//               }else {
                   saveInfo();
//               }
           }
       });
        return root;
    }

    public void saveInfo(){
        personalInfo.setName(nameET.getText().toString());

        if (ageET.getText().toString().isEmpty())
            personalInfo.setAge(0);
        else
            personalInfo.setAge(Integer.parseInt(ageET.getText().toString()));

        if (weightET.getText().toString().isEmpty())
            personalInfo.setWeight(0);
        else
            personalInfo.setWeight(Float.parseFloat(weightET.getText().toString()));

        if (heightET.getText().toString().isEmpty())
            personalInfo.setHeight(0);
        else
            personalInfo.setHeight(Float.parseFloat(heightET.getText().toString()));

        RadioButton selectedBtn = (RadioButton) genderRadioGp.findViewById(genderRadioGp.getCheckedRadioButtonId());
        String selectedText = selectedBtn.getText().toString();
        Log.d(TAG, "gender " + selectedText);
        if (selectedText.equals("M")){
            personalInfo.setGender(false);
        } else
            personalInfo.setGender(true);

    }
}