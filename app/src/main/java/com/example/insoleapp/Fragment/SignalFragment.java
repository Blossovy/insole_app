package com.example.insoleapp.Fragment;

import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.example.insoleapp.ExportData;
import com.example.insoleapp.MainActivity;
import com.example.insoleapp.R;
import com.example.insoleapp.SensorData;
import com.example.insoleapp.ToolMath;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.google.android.material.chip.Chip;
import com.google.android.material.chip.ChipGroup;

import java.util.ArrayList;
import java.util.Objects;

public class SignalFragment extends Fragment {

    private Handler handler = new Handler();
    private Handler refreshHandler = new Handler();
    private static Handler mHandler;
    private Runnable refreshLoop;
    private boolean runnableStop = false;

    //user interface
    private TextView leftDataTV, rightDataTV;
    private LineChart mChart;
    private ChipGroup chipPressure, chipIMU, chipPressureR, chipIMUR;
    private RadioButton leftBtn, rightBtn;
    //private Chip C1, C2, C3, C4, C5, C6, C7, C8;

    private SensorData sensorData;
    ToolMath toolMath = new ToolMath();
    private ArrayList<ILineDataSet> dataSets = new ArrayList<>();
    private int[] lineChart_color;
    private final String[] lineChart_nameFSR = {"Toe", "M1", "M5", "BelowMet", "M Arch", "L Arch", "Heel"};
    private final String[] lineChart_nameIMU = {"Ax1","Ay1","Az1", "Gx1", "Gy1", "Gz1", "∠x1", "∠y1", "∠z1", "Ax2" ,"Ay2","Az2", "Gx2", "Gy2", "Gz2", "∠x2", "∠y2", "∠z2"};
    private int [] displaySet = {0,1,2,3,4,5,6};
    private boolean displayZoneSwitch = true;
    private boolean displayingLeft = true;
    private boolean displayIMU = false;
    private boolean chartRefresh = true;
    Button refreshBtn;

    private ExportData exportData;
    String TAG = "Signal Fragment";


    public View onCreateView(@NonNull LayoutInflater inflater,
            ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_signal, container, false);
        leftDataTV = root.findViewById(R.id.left_data);
        rightDataTV = root.findViewById(R.id.right_data);
        mChart = root.findViewById(R.id.chart);
        chipPressure = root.findViewById(R.id.PressureChips);
        chipIMU = root.findViewById(R.id.IMUChips);
        leftBtn = root.findViewById(R.id.radioLeft);
        rightBtn = root.findViewById(R.id.radioRight);
        refreshBtn = root.findViewById(R.id.signalRefreshBtn);
        lineChart_color = Objects.requireNonNull(getActivity()).getResources().getIntArray(R.array.lineChart);
        setHandler();

        refreshBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                chartRefresh = false;
                sensorData.resetDisplayL();
                Log.d(TAG, "refresh signal chart");
                chartRefresh = true;
            }
        });

        refreshLoop = new Runnable() {
            @Override
            public void run() {
                if (chartRefresh) {
                    try {
                        if (displayingLeft) {
                            if (displayIMU) {
                                if (!sensorData.getIsIMUshiftingL() && dataNullCheck(sensorData.getDisplay_IMUL()))
                                    refreshChart(sensorData.getDisplay_IMUL());
                            } else {
                                if (!sensorData.getIsFSRshiftingL() && dataNullCheck(sensorData.getDisplay_FSRL()))
                                    refreshChart(sensorData.getDisplay_FSRL());
                            }
                        } else {
                            if (displayIMU) {
                                if (!sensorData.getIsIMUshiftingR() && dataNullCheck(sensorData.getDisplay_IMUR()))
                                    refreshChart(sensorData.getDisplay_IMUR());
                            } else {
                                if (!sensorData.getIsFSRshiftingR() && dataNullCheck(sensorData.getDisplay_FSRR()))
                                    refreshChart(sensorData.getDisplay_FSRR());
                            }
                        }
                    } finally {
                        refreshHandler.postDelayed(this, 500);
                    }
                }
            }
        };

        refreshLoop.run();

        ((MainActivity) requireActivity()).passHandler(mHandler);
        setupChart();
        leftBtn.setChecked(true) ;
        leftBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                displayingLeft = true;
            }
        });

        rightBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                displayingLeft = false;
            }
        });

        chipPressure.setOnCheckedChangeListener(new ChipGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(ChipGroup chipGroup, int i) {
                displayIMU = false;
                Chip chip = chipGroup.findViewById(i);
                if (chip != null) {
                    String selectedChip = chip.getText().toString();
                    Log.d(TAG, "chip is clicked " + selectedChip);
                    switch (selectedChip) {
                        case "Toe":
                            displaySet = new int[]{0};
                            break;
                        case "M1":
                            displaySet = new int[]{1};
                            break;
                        case "M5":
                            displaySet = new int[]{2};
                            break;
                        case "BelowMet":
                            displaySet = new int[]{3};
                            break;
                        case "M Arch":
                            displaySet = new int[]{4};
                            break;
                        case "L Arch":
                            displaySet = new int[]{5};
                            break;
                        case "Heel":
                            displaySet = new int[]{6};
                            break;
                        case "All":
                            displaySet = new int[]{0,1,2,3,4,5,6};
                            break;
                    }
                    displayZoneSwitch = true;
                    if(displayingLeft) {
                        if (!sensorData.getIsFSRshiftingL() && dataNullCheck(sensorData.getDisplay_FSRL()))
                            refreshChart(sensorData.getDisplay_FSRL());
                    }else{
                        if (!sensorData.getIsFSRshiftingR() && dataNullCheck(sensorData.getDisplay_FSRR()))
                            refreshChart(sensorData.getDisplay_FSRR());
                    }
                }
            }
        });
        chipIMU.setOnCheckedChangeListener(new ChipGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(ChipGroup chipGroup, int i) {
                displayIMU = true;
                Chip chip = chipGroup.findViewById(i);
                if (chip != null) {
                    String selectedChip = chip.getText().toString();
                    Log.d(TAG, "chip is clicked " + selectedChip);
                    switch (selectedChip) {
                        case "Acc":
                            displaySet = new int[]{0,1,2};
                            break;
                        case "Gyro":
                            displaySet = new int[]{3,4,5};
                            break;
                        case "Angle":
                            displaySet = new int[]{6,7,8};
                            break;
//                        case "Acc2":
//                            displaySet = new int[]{9,10,11};
//                            break;
//                        case "Gyro2":
//                            displaySet = new int[]{12,13,14};
//                            break;
//                        case "Angle2":
//                            displaySet = new int[]{15,16,17};
//                            break;
                    }
                    displayZoneSwitch = true;
                    Log.d(TAG, "display switch: " + displayZoneSwitch);
                    if(displayingLeft) {
                        if (!sensorData.getIsIMUshiftingL() && dataNullCheck(sensorData.getDisplay_IMUL()))
                            refreshChart( sensorData.getDisplay_IMUL());
                    }else{
                        if (!sensorData.getIsIMUshiftingR() && dataNullCheck(sensorData.getDisplay_IMUR()))
                            refreshChart( sensorData.getDisplay_IMUR());
                    }
                }
            }
        });

        return root;
    }
    @Override
    public void onDestroy() {
        Log.d(TAG,"is destroyed");
        refreshHandler.removeCallbacks(refreshLoop);
        ((MainActivity) requireActivity()).passHandler(null);
        super.onDestroy();
    }

    void setupChart (){

        Description description =new Description();
        description.setText("Insole Data");
        description.setTextColor(ContextCompat.getColor(getContext(), R.color.textLight));
        description.setTextSize(16);

        mChart.setDescription(description);
        mChart.setTouchEnabled(true);
        mChart.setDragEnabled(true);
        mChart.setDragEnabled(true);
        mChart.setDrawGridBackground(true);
        mChart.setPinchZoom(true);
        mChart.setBackgroundColor(Color.WHITE);

        Legend legend = mChart.getLegend();
        legend.setForm(Legend.LegendForm.LINE);
        legend.setTextColor(ContextCompat.getColor(getContext(), R.color.textDark));

        XAxis x1 = mChart.getXAxis();
        x1.setTextColor(ContextCompat.getColor(getContext(), R.color.textDark));
        x1.setDrawGridLines(true);
        x1.setDrawLabels(true);
        x1.setAvoidFirstLastClipping(true);
        x1.setPosition(XAxis.XAxisPosition.BOTTOM);

        YAxis y1 = mChart.getAxisLeft();
        y1.setTextColor(ContextCompat.getColor(getContext(), R.color.textDark));
        y1.setDrawGridLines(true);
        YAxis y2 = mChart.getAxisRight();
        y2.setEnabled(false);

       // addEntry();
    }


    LineDataSet createSet(int k){
        LineDataSet set;
        if(displayIMU) {
            set = new LineDataSet(null, lineChart_nameIMU[k]);
        }else{
            set = new LineDataSet(null, lineChart_nameFSR[k]);
        }
        set.setDrawIcons(false);
        set.setColor(lineChart_color[k]);
        set.setLineWidth(2f);
        set.setDrawCircles(false);
        set.setValueTextSize(9f);
        set.setMode(LineDataSet.Mode.HORIZONTAL_BEZIER);
        set.setHighLightColor(lineChart_color[k]);

        return set;
    }

    private void refreshChart(ArrayList<Entry>[] display_data) {
        try {
            //based on the sampling frequency, display data of the latest 5-sec interval
            Log.d(TAG,(display_data == null)? "display_fsrdata is null":"display_fsrdata is not null");

            if (display_data != null) {
                double [] displayDataSize = new double[display_data.length];
                for (int i = 0; i< display_data.length; i++){
                    displayDataSize[i] = display_data[i].size();
                }

                int end =  (int) Math.round(toolMath.minInArray(displayDataSize).second);
                int start = 0;
                if (end > sensorData.sensorFreq*5+1)
                    start = end - sensorData.sensorFreq*5-1;
                Log.d(TAG, "start" + start);
                Log.d(TAG, "end" + end);
                if (sensorData.rawDataL != null)
                    leftDataTV.setText(sensorData.rawDataL);
                if (sensorData.rawDataR != null)
                    rightDataTV.setText(sensorData.rawDataR);
                ArrayList<Entry> sensor1;
                Log.d(TAG, "display switch: " + displayZoneSwitch);
                LineDataSet[] sensorSet = new LineDataSet[displaySet.length];
                if (mChart.getData() != null && mChart.getData().getDataSetCount() > 0 && displayZoneSwitch == false) {
                    try {
                        for (int k = 0; k < displaySet.length; k++) {
                            sensor1 = new ArrayList<Entry>(display_data[displaySet[k]].subList(start, end));
                            sensorSet[k] = (LineDataSet) mChart.getData().getDataSetByIndex(k);
                            sensorSet[k].setValues(sensor1);

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    try {
                        mChart.getData().notifyDataChanged();
                        mChart.notifyDataSetChanged();

                        mChart.setVisibleXRangeMaximum(5000);
                        mChart.moveViewToX(mChart.getLineData().getXMax() - 5000);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    // Log.d(TAG, "new 7 set created");
                    for (int k = 0; k < displaySet.length; k++) {
                        Log.d(TAG, "display set length: " + displaySet.length);
                        sensorSet[k] = createSet(displaySet[k]);
                    }
                    LineData data = new LineData(sensorSet);
                    mChart.setData(data);

                    displayZoneSwitch = false;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    boolean dataNullCheck (ArrayList<Entry>[] data){
        for (int i = 0; i < data.length; i++){
            if (data[i].size() == 0){
                Log.d(TAG, "data null check failed at" + i);
                return false;
            }
        }
        return true;
    }
    void setHandler(){ //communicate with main activity
//        mHandler = new Handler(){
//            @Override
//            public void handleMessage(@NonNull Message msg) {
//                switch ((msg.what)){
//                    case MainActivity.MessageConstants.MESSAGE_ADD_ENTRY_L:
//                        try {
//                            if (displayingLeft)
//                            refreshChart(true);
//                            Log.d(TAG, "handler passed to signal fragment L");
//                        }catch (Exception e){
//                            Log.d(TAG, "handler went wrong: " + e);
//                        }
//                        break;
//                    case MainActivity.MessageConstants.MESSAGE_ADD_ENTRY_R:
//                        try {
//                            if (!displayingLeft)
//                            refreshChart(false);
//                            Log.d(TAG, "handler passed to signal fragment R");
//                        }catch (Exception e){
//                            Log.d(TAG, "handler went wrong: " + e);
//                        }
//                        break;
//                }
//            }
//        };
    }


}
