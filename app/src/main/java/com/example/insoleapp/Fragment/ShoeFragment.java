package com.example.insoleapp.Fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.example.insoleapp.MainActivity;
import com.example.insoleapp.R;
import com.example.insoleapp.ShoeInfo;
import com.google.android.material.chip.Chip;
import com.google.android.material.chip.ChipGroup;

public class ShoeFragment extends Fragment {

    private ShoeInfo shoeInfo = ShoeInfo.getInstance();
    //Fragment element
    View root;
    TextView shoeHeaderL, shoeHeaderR;
    ChipGroup shoeModeL, shoeModeR;
    Chip chip0shoeL, chip1shoeL, chip2shoeL, chip0shoeR, chip1shoeR, chip2shoeR;
    ImageView [] iv_sensorL = new ImageView[3];
    ImageView [] iv_sensorR = new ImageView[3];
    TextView [] tv_sensorL = new TextView[3];
    TextView [] tv_sensorR = new TextView[3];
    ImageView batLvlL, batLvlR;
    Button refreshBtn;
    private final static String TAG = "Shoe Fragment";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        root = inflater.inflate(R.layout.fragment_shoe, container, false);
        fragment_setup();
        widget_refresh();
        shoeModeL.setOnCheckedChangeListener(new ChipGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(ChipGroup group, int checkedId) {
                Chip chip = group.findViewById(checkedId);
                if (chip != null) {
                    String selectedChip = chip.getText().toString();
                    switch (selectedChip){
                        case "Soft":
                            shoeInfo.setInsoleModeL(MainActivity.MODE_SOFT);
                            break;
                        case "Medium":
                            shoeInfo.setInsoleModeL(MainActivity.MODE_MED);
                            break;
                        case "Stiff":
                            shoeInfo.setInsoleModeL(MainActivity.MODE_STIFF);
                            break;
                    }
                    shoeInfo.setModeChangedL(true);
                }
            }
        });
        shoeModeR.setOnCheckedChangeListener(new ChipGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(ChipGroup group, int checkedId) {
                Chip chip = group.findViewById(checkedId);
                if (chip != null) {
                    String selectedChip = chip.getText().toString();
                    switch (selectedChip){
                        case "Soft":
                            shoeInfo.setInsoleModeR(MainActivity.MODE_SOFT);
                            break;
                        case "Medium":
                            shoeInfo.setInsoleModeR(MainActivity.MODE_MED);
                            break;
                        case "Stiff":
                            shoeInfo.setInsoleModeR(MainActivity.MODE_STIFF);
                            break;
                    }
                    shoeInfo.setModeChangedR(true);
                }
            }
        });

        refreshBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                widget_refresh();
            }
        });
        return  root;
    }

    public void widget_refresh(){
        if(shoeInfo.getBluetoothDeviceL() != null)
            shoeHeaderL.setText(shoeInfo.getBluetoothDeviceL().getName());
        if(shoeInfo.getBluetoothDeviceR() != null)
            shoeHeaderR.setText(shoeInfo.getBluetoothDeviceR().getName());
        switch(shoeInfo.getInsoleModeL()){
            case MainActivity.MODE_SOFT:
                chip0shoeL.setChecked(true);
                break;
            case MainActivity.MODE_MED:
                chip1shoeL.setChecked(true);
                break;
            case MainActivity.MODE_STIFF:
                chip2shoeL.setChecked(true);
                break;
        }
        switch(shoeInfo.getInsoleModeR()){
            case MainActivity.MODE_SOFT:
                chip0shoeR.setChecked(true);
                break;
            case MainActivity.MODE_MED:
                chip1shoeR.setChecked(true);
                break;
            case MainActivity.MODE_STIFF:
                chip2shoeR.setChecked(true);
                break;
        }
        for (int i = 0; i < 3; i++){
            if (shoeInfo.getSensorsAvailL(i)){
                iv_sensorL[i].setImageResource(R.drawable.ic_check_blue);
                tv_sensorL[i].setTextColor(ContextCompat.getColor(getContext(), R.color.tintBlue));
            }else{
                iv_sensorL[i].setImageResource(R.drawable.ic_check_grey);
                tv_sensorL[i].setTextColor(ContextCompat.getColor(getContext(), R.color.primaryDarkColor));
            }
        }
        for (int i = 0; i < 3; i++){
            if (shoeInfo.getSensorsAvailR(i)){
                iv_sensorR[i].setImageResource(R.drawable.ic_check_blue);
                tv_sensorR[i].setTextColor(ContextCompat.getColor(getContext(), R.color.tintBlue));
            }else{
                iv_sensorR[i].setImageResource(R.drawable.ic_check_grey);
                tv_sensorR[i].setTextColor(ContextCompat.getColor(getContext(), R.color.primaryDarkColor));
            }
        }

        if (shoeInfo.getBatterylvlL() <0){
            batLvlL.setImageResource(R.drawable.ic_battery_unknown);
        }else if (shoeInfo.getBatterylvlL() <2150){
            batLvlL.setImageResource(R.drawable.ic_battery_low);
        }else if (shoeInfo.getBatterylvlL() <2400){
            batLvlL.setImageResource(R.drawable.ic_battery_med);
        }else{
            batLvlL.setImageResource(R.drawable.ic_battery_full);
        }
        if (shoeInfo.getBatterylvlR() <0){
            batLvlR.setImageResource(R.drawable.ic_battery_unknown);
        }else if (shoeInfo.getBatterylvlR() <2150){
            batLvlR.setImageResource(R.drawable.ic_battery_low);
        }else if (shoeInfo.getBatterylvlR() <2400){
            batLvlR.setImageResource(R.drawable.ic_battery_med);
        }else {
            batLvlR.setImageResource(R.drawable.ic_battery_full);
        }

    }


    public void fragment_setup(){
        shoeHeaderL = (TextView) root.findViewById(R.id.shoeHeaderL);
        shoeHeaderR = (TextView) root.findViewById(R.id.shoeHeaderR);
        shoeModeL = (ChipGroup) root.findViewById(R.id.leftActuatorChips);
        shoeModeR = (ChipGroup) root.findViewById(R.id.rightActuatorChips);
        chip0shoeL = (Chip) root.findViewById(R.id.shoeModeL0);
        chip1shoeL = (Chip) root.findViewById(R.id.shoeModeL1);
        chip2shoeL = (Chip) root.findViewById(R.id.shoeModeL2);
        chip0shoeR = (Chip) root.findViewById(R.id.shoeModeR0);
        chip1shoeR = (Chip) root.findViewById(R.id.shoeModeR1);
        chip2shoeR = (Chip) root.findViewById(R.id.shoeModeR2);
        iv_sensorL [0] = (ImageView) root.findViewById(R.id.shoeFSRcheckL);
        iv_sensorL [1] = (ImageView) root.findViewById(R.id.shoeIMU1checkL);
        iv_sensorL [2]= (ImageView) root.findViewById(R.id.shoeIMU2checkL);
        iv_sensorR [0] = (ImageView) root.findViewById(R.id.shoeFSRcheckR);
        iv_sensorR [1] = (ImageView) root.findViewById(R.id.shoeIMU1checkR);
        iv_sensorR [2] = (ImageView) root.findViewById(R.id.shoeIMU2checkR);
        tv_sensorL [0] = (TextView) root.findViewById(R.id.shoeFSRL);
        tv_sensorL [1] = (TextView) root.findViewById(R.id.shoeIMU1L);
        tv_sensorL [2]= (TextView) root.findViewById(R.id.shoeIMU2L);
        tv_sensorR [0] = (TextView) root.findViewById(R.id.shoeFSRR);
        tv_sensorR [1] = (TextView) root.findViewById(R.id.shoeIMU1R);
        tv_sensorR [2] = (TextView) root.findViewById(R.id.shoeIMU2R);
        batLvlL = (ImageView) root.findViewById(R.id.batteryLevelL);
        batLvlR = (ImageView) root.findViewById(R.id.batteryLevelR);
        refreshBtn = (Button) root.findViewById(R.id.refreshBtn);
    }


}
