package com.example.insoleapp.Fragment;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.Bundle;
import android.os.ParcelUuid;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.example.insoleapp.MainActivity;
import com.example.insoleapp.R;
import com.example.insoleapp.SensorData;
import com.example.insoleapp.ShoeInfo;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

public class BluetoothFragment extends Fragment {

    private static final int REQUEST_ENABLE_BLUETOOTH = 1;
    private BluetoothAdapter bluetoothAdapter;
    private ArrayList<BluetoothDevice> listItems = new ArrayList<>();
    private ArrayAdapter<BluetoothDevice> listAdapter;
    private ListView deviceLVL;
    private ListView deviceLVR;
    private TextView statusTVL;
    private TextView statusTVR;
    private TextView btstatusTV;
    private Button connectBtnL;
    private Button connectBtnR;
    private ShoeInfo shoeInfo = ShoeInfo.getInstance();

    private final String TAG = "Bluetooth Device";

    private BluetoothDevice device_Left = null;
    private BluetoothDevice device_Right = null;

    public View onCreateView(@NonNull LayoutInflater inflater,
            ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_bluetooth, container, false);
        statusTVL = root.findViewById(R.id.text_bluetooth);
        statusTVR = root.findViewById(R.id.text_bluetooth2);
        btstatusTV = root.findViewById(R.id.btStatusTV);
        deviceLVL = root.findViewById(R.id.device_list);
        deviceLVR = root.findViewById(R.id.device_list2);
        connectBtnL = root.findViewById(R.id.button1);
        connectBtnR = root.findViewById(R.id.button2);
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        isBtEnabled();
        btstatusUpdate();
        buttonUpdate(connectBtnL, MainActivity.isBtConnectedL);
        buttonUpdate(connectBtnR, MainActivity.isBtConnectedR);

        connectBtnL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //openBT();
                if (!MainActivity.isBtConnectedL) {
                    String text = openBtn(((MainActivity) requireActivity()).openBT(1, device_Left), 1);
                    statusTVL.setText(text);
                    buttonUpdate(connectBtnL, MainActivity.isBtConnectedL);
                }else if(MainActivity.isBtConnectedL){
                    boolean isBTstop = ((MainActivity)requireActivity()).closeBT(1);
                    statusTVL.setText(closeBtn(isBTstop));
                    buttonUpdate(connectBtnL, MainActivity.isBtConnectedL);
                }
            }
        });

        connectBtnR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!MainActivity.isBtConnectedR) {
                    String text = openBtn(((MainActivity) requireActivity()).openBT(2, device_Right), 2);
                    statusTVR.setText(text);
                    buttonUpdate(connectBtnR, MainActivity.isBtConnectedR);
                }else if(MainActivity.isBtConnectedR){
                    boolean isBTstop = ((MainActivity)requireActivity()).closeBT(2);
                    statusTVR.setText(closeBtn(isBTstop));
                    buttonUpdate(connectBtnR, MainActivity.isBtConnectedR);
                }
            }
        });

        listAdapter = new ArrayAdapter<BluetoothDevice>(getActivity(), 0, listItems) {
            @Override
            public View getView(int position, View view, ViewGroup parent) {
                BluetoothDevice device = listItems.get(position);
                if (view == null)
                    view = getActivity().getLayoutInflater().inflate(R.layout.device_list_item, parent, false);
                TextView text1 = view.findViewById(R.id.text1);
                TextView text2 = view.findViewById(R.id.text2);
                text1.setText(device.getName());
                text2.setText(device.getAddress());
                return view;
            }
        };
        deviceLVL.setAdapter(listAdapter);
        deviceLVL.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (!MainActivity.isBtConnectedL) {
                    device_Left = listItems.get(position);
                    statusTVL.setText("Selected: " + device_Left.getName());
                }
            }
        });

        deviceLVR.setAdapter(listAdapter);
        deviceLVR.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (!MainActivity.isBtConnectedR) {
                    device_Right = listItems.get(position);
                    statusTVR.setText("Selected: " + device_Right.getName());
                }
            }
        });
        return root;
    }

    String closeBtn(boolean isDisconnected){
        if (isDisconnected)
            return getResources().getString(R.string.bluetooth_closed);
        else
            return getResources().getString(R.string.bluetooth_error);
    }

    String openBtn(int status, int device){
        switch (status){
            case MainActivity.BLUETOOTH_CONNECTED:
                if (device == 1){
                    shoeInfo.setBluetoothDeviceL(device_Left);
                    return device_Left.getName() + " is connected";
                }else if (device == 2){
                    shoeInfo.setBluetoothDeviceR(device_Right);
                    return device_Right.getName() + " is connected";
                }
                break;
            case MainActivity.BLUETOOTH_CONNECTION_FAILED:
                return getResources().getString(R.string.bluetooth_connection_failed);
            case MainActivity.BLUETOOTH_SOCKET_NULL:
                return getResources().getString(R.string.bluetooth_select);
            case MainActivity.BLUETOOTH_CONNECTION_ERROR:
                return getResources().getString(R.string.bluetooth_error);
        }
        return null;
    }

    void buttonUpdate(Button button, boolean isConnected){
        if(isConnected){
            button.setText("Stop");
        }else
            button.setText("Connect");
    }
    void btstatusUpdate(){
        if (MainActivity.isBtConnectedL)
            statusTVL.setText(shoeInfo.getBluetoothDeviceL().getName() + " is connected");
        else
            statusTVL.setText(getResources().getString(R.string.bluetooth_select));
        if (MainActivity.isBtConnectedR)
            statusTVR.setText(shoeInfo.getBluetoothDeviceR().getName() + " is connected");
        else
            statusTVR.setText(getResources().getString(R.string.bluetooth_select));
    }

    @Override
    public void onResume() {
        super.onResume();
        isBtEnabled();
        refresh();
    }

    void isBtEnabled(){
        if (!bluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BLUETOOTH);
        }
    }

    void refresh() {
        if(bluetoothAdapter == null)
            btstatusTV.setText("<NO BLUETOOTH SUPPORT>");
        else if(!bluetoothAdapter.isEnabled())
            btstatusTV.setText("<BLUETOOTH DISABLED>");
        listItems.clear();
        if(bluetoothAdapter != null) {
            for (BluetoothDevice device : bluetoothAdapter.getBondedDevices()) {
                if (device.getType() != BluetoothDevice.DEVICE_TYPE_LE)
                    listItems.add(device);
            }
        }
        if (listItems == null){
            btstatusTV.setText("NO DEVICES FOUND");
        }else if (listItems != null && bluetoothAdapter.isEnabled())
            btstatusTV.setText("<BLUETOOTH ENABLED>");


        listAdapter.notifyDataSetChanged();
    }

}