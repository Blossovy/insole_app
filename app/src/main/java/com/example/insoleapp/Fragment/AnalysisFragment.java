package com.example.insoleapp.Fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Switch;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.example.insoleapp.AnalysedData;
import com.example.insoleapp.ExportData;
import com.example.insoleapp.MainActivity;
import com.example.insoleapp.R;
import com.example.insoleapp.SVModel;
import com.example.insoleapp.SensorData;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;

import java.io.IOException;
import java.util.ArrayList;

public class AnalysisFragment extends Fragment {

    private LineChart fsrChart, imuChart;
    private BarChart ctBarChart;
    private TableLayout fatigueTableLO;
    private Button startBtn, refreshBtn, loadFileBtn, runSMVBtn, rawBtn;
    private ImageButton pressureBtn, imuBtn, fatigueBtn;
    private ScrollView pressureBlkSV, imuBlkSV, fatigueBlkSV, debugBlkSV;
    private LinearLayout fatigueLayout;
    private TextView fsrCount, errTV, anaDisplay, debugTV, fatigueTV;
    private Switch debugBtn, buildModelBtn;
    private ImageView dataRecordingIV;
    private static AnalysedData analysedData = AnalysedData.getInstance();
    private ExportData exportData;
    private final String TAG = "Analysis Frag";
    ImportFileListener importFileListener;
    Runnable runnableUI;
    Handler handlerUI = new Handler();
    SensorData sensorData = SensorData.getInstance();
    ArrayList<Entry> display_fsrdata =  new ArrayList();
    ArrayList<Entry> display_imudata =  new ArrayList();
    private final static int display_pressure_block = 0;
    private final static int display_imu_block = 1;
    private final static int display_fatigue_block = 2;
    private final static int display_debug_block = 3;
    private static int displayBlock = 0;

    public interface ImportFileListener{
         void importFile ();
    }
    @Nullable
    @Override

    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_analysis, container, false);
        if (exportData == null){
            exportData = MainActivity.exportData;
        }
        debugBtn = root.findViewById(R.id.debugSwitch);
        startBtn = root.findViewById(R.id.startBtn);
        refreshBtn = root.findViewById(R.id.analysisRefreshBtn);
        loadFileBtn = root.findViewById(R.id.loadBtn);
        fsrCount = root.findViewById(R.id.analysisFsrCount);
        errTV = root.findViewById(R.id.errMsgTV);
        dataRecordingIV = root.findViewById(R.id.dataInProcessIM);
        runSMVBtn = root.findViewById(R.id.sMVBtn);
        anaDisplay = root.findViewById(R.id.analysisTV);
        buildModelBtn = root.findViewById(R.id.BuildModelSwitch);
        debugTV = root.findViewById(R.id.analysis2TV);
        fsrChart = root.findViewById(R.id.fsrchart);
        ctBarChart = root.findViewById(R.id.ct_chart);
        fatigueTV = root.findViewById(R.id.fatigueState);
        imuChart = root.findViewById(R.id.imuchart);
        fatigueTableLO = root.findViewById(R.id.fatigueTable);
        pressureBtn = root.findViewById(R.id.analysis_pressure);
        imuBtn = root.findViewById(R.id.analysis_imu);
        fatigueBtn = root.findViewById(R.id.analysis_fatigue);
        rawBtn = root.findViewById(R.id.analysis_raw);
        pressureBlkSV = root.findViewById(R.id.pressureSV);
        imuBlkSV = root.findViewById(R.id.IMUSV);
        fatigueBlkSV = root.findViewById(R.id.fatigueSV);
        debugBlkSV = root.findViewById(R.id.debugSV);
        fatigueLayout = root.findViewById(R.id.fatigueStateLayout);

        refreshWidget();
        updateUI();
        debugBtn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                Log.d(TAG, "debug is " + b);
                MainActivity.analysisDebug = b;
            }
        });
        buildModelBtn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                try {
                    SVModel model = SVModel.getInstance();
                    if (model.getContext() == null) {
                        model.setContext(getContext());
                    }
                    Log.d(TAG, "SVM model starts");
                    model.buildModelFromTxt();

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        startBtn.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            if (MainActivity.analysisOn) {
                                                Log.d(TAG,"analysis was on");
                                               // try {
                                                    //MainActivity.exportData.endLogging_analysis();
                                                    //MainActivity.exportData.endSMVLog();
                                                    MainActivity.analysisLoopStop5min();
                                                    MainActivity.analysisLoopStop1min();
                                              //  } catch (IOException e) {
                                              //      e.printStackTrace();
                                              //  }
                                            }else {
                                                Log.d(TAG, "analysis was off");
                                                MainActivity.analysisLoopCount5min();  //activate 5-min clock
                                                if(MainActivity.exportData.outputStream_analysis == null) {
                                                    MainActivity.exportData.startLogging_analysis();
                                                }
                                                if (MainActivity.exportData.outputStreamSMV == null) {
                                                    MainActivity.exportData.startSMVLog();
                                                }
                                            }
                                            MainActivity.analysisOn = !MainActivity.analysisOn;
                                            startBtn.setText((MainActivity.analysisOn)?"Stop":"Start");

                                        }
                                    }

        );
        refreshBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                refreshWidget();

                /*try {
                    SVModel model = SVModel.getInstance();
                    if (model.getContext() == null) {
                        model.setContext(getContext());
                    }
                    Log.d(TAG, "SVM model starts");
                    model.buildModelFromTxt();
                } catch (IOException e) {
                    e.printStackTrace();
                }*/
            }
        });

        runSMVBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sensorData.dataLogProcess = true;
                MainActivity.analysisInProcess = true;
            }
        });

        loadFileBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                importFileListener.importFile();

            }
        });

        pressureBtn.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               displayBlock = display_pressure_block;
               pressureBlkSV.setVisibility(View.VISIBLE); //set visible
               imuBlkSV.setVisibility(View.GONE);
               fatigueBlkSV.setVisibility(View.GONE);
               fatigueLayout.setVisibility(View.GONE);
               debugBlkSV.setVisibility(View.GONE);
               setupFSRChart();
               refreshFSRChart();
               refreshCTBarChart();
           }
       });

        imuBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                displayBlock = display_imu_block;
                pressureBlkSV.setVisibility(View.GONE); //set visible
                imuBlkSV.setVisibility(View.VISIBLE);
                fatigueBlkSV.setVisibility(View.GONE);
                fatigueLayout.setVisibility(View.GONE);
                debugBlkSV.setVisibility(View.GONE);
                setupIMUChart();
                refreshIMUChart();
            }
        });

        fatigueBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                displayBlock = display_fatigue_block;
                pressureBlkSV.setVisibility(View.GONE); //set visible
                imuBlkSV.setVisibility(View.GONE);
                fatigueBlkSV.setVisibility(View.VISIBLE);
                fatigueLayout.setVisibility(View.VISIBLE);
                debugBlkSV.setVisibility(View.GONE);
                setupFatigueTable();
            }
        });

        rawBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                displayBlock = display_debug_block;
                pressureBlkSV.setVisibility(View.GONE); //set visible
                imuBlkSV.setVisibility(View.GONE);
                fatigueBlkSV.setVisibility(View.GONE);
                fatigueLayout.setVisibility(View.GONE);
                debugBlkSV.setVisibility(View.VISIBLE);
            }
        });
        setupFSRChart();

        return  root;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        handlerUI.removeCallbacks(runnableUI);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        Activity activity = (Activity) context;
        try{

            importFileListener = (ImportFileListener) activity;
        }catch (ClassCastException e){
            throw  new ClassCastException(activity.toString() + " must Override importFile");
        }
    }

    private void updateUI(){
        runnableUI = new Runnable() {
            @Override
            public void run() {

                refreshWidget();

                handlerUI.postDelayed(this, 1000);
            }
        };
        runnableUI.run();
    }
    private void refreshWidget(){
        startBtn.setText((MainActivity.analysisOn)?"Stop":"Start");
        int sensorNo = analysedData.getSensorDataNum();
        fsrCount.setText(String.valueOf(sensorNo));
        if (sensorNo != 0){
            double fatigue = analysedData.getSvm_yPre()[sensorNo-1];
            Log.d(TAG, "fatigue level:" +fatigue);
            if (fatigue>1.5) {
                fatigueTV.setText("Yes");
            }else
                fatigueTV.setText("No");
        }
        errTV.setText(AnalysedData.errorMsg.toString());
        debugTV.setText(AnalysedData.debugMsg.toString());
        debugBtn.setChecked(MainActivity.analysisDebug);
        //buildModelBtn.setChecked(MainActivity.manualSMVClock);
        //analysisNum.setText();
        analysedData.updateYpreMsg();
        anaDisplay.setText(analysedData.yPreMsg.toString());
        dataRecordingIV.setImageResource(MainActivity.analysisInProcess? R.drawable.ic_recording_red:R.drawable.ic_recording_grey);
        Log.d(TAG, "sensorDataNum: " + analysedData.getSensorDataNum());
        Log.d(TAG, "display_fsrdata: " + display_fsrdata.size());
        if(displayBlock == display_pressure_block){
            refreshFSRdata();
        }
        if(displayBlock == display_imu_block) {
            refreshIMUdata();
        }
        if(displayBlock == display_fatigue_block) {
            setupFatigueTable();
        }
        if (analysedData.getSensorDataNum() > 0 && display_fsrdata.size() > 500) {
            if(displayBlock == display_pressure_block){
                refreshFSRChart();
                refreshCTBarChart();
            }
            if(displayBlock == display_imu_block) {
                refreshIMUChart();
            }
        }
    }
    //for fsr total pressure chart;
    void setupFSRChart(){

        Description description =new Description();
        description.setText("FSR Sum");
        description.setTextColor(ContextCompat.getColor(getContext(), R.color.textLight));
        description.setTextSize(14);

        fsrChart.setDescription(description);
        fsrChart.setTouchEnabled(true);
        fsrChart.setDragEnabled(true);
        fsrChart.setDragEnabled(true);
        fsrChart.setDrawGridBackground(true);
        fsrChart.setPinchZoom(true);

        Legend legend = fsrChart.getLegend();
        legend.setForm(Legend.LegendForm.LINE);
        legend.setTextColor(ContextCompat.getColor(getContext(), R.color.textDark));

        XAxis x1 = fsrChart.getXAxis();
        x1.setTextColor(ContextCompat.getColor(getContext(), R.color.textDark));
        x1.setDrawGridLines(true);
        x1.setDrawLabels(true);
        x1.setAvoidFirstLastClipping(true);
        x1.setPosition(XAxis.XAxisPosition.BOTTOM);

        YAxis y1 = fsrChart.getAxisLeft();
        y1.setTextColor(ContextCompat.getColor(getContext(), R.color.textDark));
        y1.setDrawGridLines(true);
        YAxis y2 = fsrChart.getAxisRight();
        y2.setEnabled(false);

        // addEntry();
    }
    //for all the line chart to set up
    LineDataSet createSet(ArrayList<Entry> display_values, String label){
        LineDataSet set = new LineDataSet(display_values, label);
        set.setDrawIcons(false);
        set.setColor(ContextCompat.getColor(getContext(), R.color.tintBlue));
        set.setLineWidth(2f);
        set.setDrawCircles(false);
        set.setValueTextSize(9f);
        set.setMode(LineDataSet.Mode.HORIZONTAL_BEZIER);
        set.setHighLightColor(ContextCompat.getColor(getContext(), R.color.tintBlue));

        return set;
    }

    public void refreshFSRdata(){
        display_fsrdata =  new ArrayList();
        double[] fsrTotal = analysedData.getFSRTotal();
        double[] time = analysedData.getTimeDiff();
        if (analysedData.getSensorDataNum()!= 0 && fsrTotal!= null && time!= null) {
            for (int i = 0; i < fsrTotal.length; i++){
                display_fsrdata.add(new Entry((float) time[i], (float) fsrTotal[i]));
            }
        }
    }

    private void refreshFSRChart() {

        try {
                LineDataSet sensorSet;
                // if chart is not initialised
                if (fsrChart.getData() != null && fsrChart.getData().getDataSetCount() > 0) {
                    try {
                            sensorSet = (LineDataSet) fsrChart.getData().getDataSetByIndex(0);
                            sensorSet.setValues(display_fsrdata);
                            fsrChart.getData().notifyDataChanged();
                            fsrChart.notifyDataSetChanged();
                            fsrChart.invalidate();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else {
                    if(display_fsrdata.size() >500) {
                        Log.d(TAG, "display_data2: " + display_fsrdata.size());
                        sensorSet = createSet(display_fsrdata, "FSR sum");
                        ArrayList<ILineDataSet> dataSets = new ArrayList<>();
                        dataSets.add(sensorSet);
                        LineData data = new LineData(dataSets);
                        fsrChart.setData(data);
                        Log.d(TAG, "create set");
                    }
                }
            } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private ArrayList<BarEntry> refreshCTdata(){
        ArrayList<BarEntry> display_ctdata = new ArrayList<>();
        double[] ctNow = analysedData.getLatestCT();
        double[] ctFirst = analysedData.getFirstCT();
        double[] percentChange = new double[8];
        for (int i = 0; i<8; i++){
            percentChange[i] = (ctNow[i]-ctFirst[i])/ctFirst[i]*100;
            display_ctdata.add(new BarEntry(i, (float) percentChange[i]));
        }
        return display_ctdata;
    }
    private void refreshCTBarChart(){

        final ArrayList<String> xAxisLabel = new ArrayList<>();
        xAxisLabel.add("Toe");
        xAxisLabel.add("M1");
        xAxisLabel.add("M5");
        xAxisLabel.add("Met");
        xAxisLabel.add("MArch");
        xAxisLabel.add("LArch");
        xAxisLabel.add("Heel");
        xAxisLabel.add("Sum");

        XAxis xAxis = ctBarChart.getXAxis();
        xAxis.setValueFormatter(new IndexAxisValueFormatter(xAxisLabel));

        Description description =new Description();
        description.setText("Contact Time Change");
        description.setTextColor(ContextCompat.getColor(getContext(), R.color.textLight));
        description.setTextSize(14);
        ctBarChart.setDescription(description);

        BarDataSet barDataSet = new BarDataSet(refreshCTdata(), "Regional contact time change compared to first recorded data");
        BarData barData = new BarData();
        barData.addDataSet(barDataSet);
        ctBarChart.setData(barData);
        ctBarChart.invalidate();

    }

    void setupIMUChart(){

        Description description =new Description();
        description.setText("Resultant Acceleration");
        description.setTextColor(ContextCompat.getColor(getContext(), R.color.textLight));
        description.setTextSize(14);

        imuChart.setDescription(description);
        imuChart.setTouchEnabled(true);
        imuChart.setDragEnabled(true);
        imuChart.setDragEnabled(true);
        imuChart.setDrawGridBackground(true);
        imuChart.setPinchZoom(true);

        Legend legend = imuChart.getLegend();
        legend.setForm(Legend.LegendForm.LINE);
        legend.setTextColor(ContextCompat.getColor(getContext(), R.color.textDark));

        XAxis x1 = imuChart.getXAxis();
        x1.setTextColor(ContextCompat.getColor(getContext(), R.color.textDark));
        x1.setDrawGridLines(true);
        x1.setDrawLabels(true);
        x1.setAvoidFirstLastClipping(true);
        x1.setPosition(XAxis.XAxisPosition.BOTTOM);

        YAxis y1 = imuChart.getAxisLeft();
        y1.setTextColor(ContextCompat.getColor(getContext(), R.color.textDark));
        y1.setDrawGridLines(true);
        YAxis y2 = imuChart.getAxisRight();
        y2.setEnabled(false);

    }

    public void refreshIMUdata(){
        display_imudata =  new ArrayList();
        double[] imuCAcc = analysedData.getResultantAcc();
        double[] time = analysedData.getTimeDiff();
        if (analysedData.getSensorDataNum()!= 0 && imuCAcc!= null && time!= null) {
            for (int i = 0; i < imuCAcc.length; i++){
                display_imudata.add(new Entry((float) time[i], (float) imuCAcc[i]));
            }
        }
    }
    private void refreshIMUChart() {

        try {
            LineDataSet sensorSet;
            // if chart is not initialised
            if (imuChart.getData() != null && imuChart.getData().getDataSetCount() > 0) {
                try {
                    sensorSet = (LineDataSet) imuChart.getData().getDataSetByIndex(0);
                    sensorSet.setValues(display_imudata);
                    imuChart.getData().notifyDataChanged();
                    imuChart.notifyDataSetChanged();
                    imuChart.invalidate();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else {
                if(display_imudata.size() >500) {
                    Log.d(TAG, "display_data2: " + display_imudata.size());
                    sensorSet = createSet(display_imudata, "Resultant Acceleration");
                    ArrayList<ILineDataSet> dataSets = new ArrayList<>();
                    dataSets.add(sensorSet);
                    LineData data = new LineData(dataSets);
                    imuChart.setData(data);
                    Log.d(TAG, "create set imu");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setupFatigueTable(){
        fatigueTableLO.removeAllViews();
        if (getActivity()!= null) {
            LayoutInflater inflater = getActivity().getLayoutInflater();
            double[] time = analysedData.getFatigueTime();
            double[] svmState = analysedData.getFatigueState();

            for (int i = 0; i < analysedData.getSensorDataNum(); i++) {

                TableRow row = (TableRow) inflater.inflate(R.layout.table_row, fatigueTableLO, false);
                TextView text = (TextView) row.findViewById(R.id.fatigueText1);
                TextView text2 = (TextView) row.findViewById(R.id.fatigueText2);
                //  String display = time[i] + "                  " + (int) svmState[i];
                text.setText(String.valueOf(time[i]));
                text2.setText(String.valueOf((int) svmState[i]-1));
                // other customizations to the row

                fatigueTableLO.addView(row);
            }
        }
    }

}
