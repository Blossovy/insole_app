package com.example.insoleapp;

import android.util.Log;

import java.util.ArrayList;
import java.util.Arrays;

public class FeatureExtraction {
    int sensorFreq = SensorData.sensorFreq;
    ToolMath toolMath = new ToolMath();
    AnalysedData analysedData = AnalysedData.getInstance();

    double[] time;
    double [][] sensorsFSR, sensorsIMU;
    int sensorLen;
    ArrayList<Integer>[] gait_n = new ArrayList [2];
    double[] ct_ave, f_m_ave, f_mad_ave, fti_ave, fea_imu_ave;
    int flag_fea = 0;
    ArrayList<Integer> id_rep_reg = new ArrayList<>();
    int id_pre = 0;
    double[][] sub_ct_previous, sub_f_m_previous, sub_f_mad_previous, sub_fti_previous;

    private static final String TAG = " FeatureExtraction";

    public FeatureExtraction(double[] timeDiff, double [][] fsr, double[][] imu, ArrayList<Integer>[] gait_n_seg,
                             double[][] sub_ct_p, double[][] sub_f_m_p, double[][] sub_f_mad_p, double[][] sub_fti_p, int pre_id){
        time = timeDiff;
        sensorsFSR = fsr;
        sensorsIMU = imu;
        sensorLen = time.length;
        gait_n = gait_n_seg;
        sub_ct_previous = sub_ct_p;
        sub_f_m_previous = sub_f_m_p;
        sub_f_mad_previous = sub_f_mad_p;
        sub_fti_previous = sub_fti_p;
        id_pre = pre_id;
    }

    public boolean featureCalculation(){
        try {
            double contact_t_max = 0.78;    //s
            double contact_t_min = 0.48;    //s
            double stride_t_max = 1.3;
            double stride_t_min = 0.8;

            int gaitNumber = gait_n[0].size();
            double[][] ct = new double[gaitNumber - 1][8];
            double[][] f_m = new double[gaitNumber - 1][8];
            double[][] f_mad = new double[gaitNumber - 1][8];
            double[][] fti = new double[gaitNumber - 1][8];

            double[][] fea_imu = new double[gaitNumber - 1][21];

            ArrayList<Integer> id_delete = new ArrayList<>();
            double[][] id_replace = new double[gaitNumber - 1][8];
            int n_str = 0;
            int tem_p = 5;

            // 0) if subject is walking
            double[] acc_total = new double[sensorLen];
            for (int j = 0; j < sensorLen; j++) {
                acc_total[j] = Math.pow(sensorsIMU[0][j], 2) + Math.pow(sensorsIMU[1][j], 2) + Math.pow(sensorsIMU[2][j], 2);
            }
            if (toolMath.maxInArray(acc_total).second < 10) {

                analysedData.debugMsg.append("Fea Cal: acc_total:" + toolMath.maxInArray(acc_total).second + "\n");
                return false;
            }

            for (int k = 0; k < gaitNumber - 1; k++) {
                double timeInterval = time[gait_n[0].get(k + 1)] - time[gait_n[0].get(k)];
                if (timeInterval > stride_t_max || timeInterval < stride_t_min) {
                    continue;
                }
                //0.2 pre
                int first = Math.max(gait_n[0].get(k) - tem_p, 0);
                int end = Math.min(gait_n[0].get(k + 1) + tem_p, sensorLen - 1);

                int id_start = tem_p;
                int id_end = gait_n[1].get(k) - gait_n[0].get(k) + tem_p;
                Log.d(TAG, "first : " + first + ", end: " + end);
                //1. force
                double[] flag_delete;
                double[][] data_F = new double[8][end - first + 1];
                for (int i = 0; i < 8; i++) {
                    data_F[i] = Arrays.copyOfRange(sensorsFSR[i], first, end + 1);
                }
                double[][] stepFeatures = forceFeatures(Arrays.copyOfRange(time, first, end + 1), data_F, id_start, id_end);
                ct[n_str] = stepFeatures[0];
                f_m[n_str] = stepFeatures[1];
                f_mad[n_str] = stepFeatures[2];
                fti[n_str] = stepFeatures[3];
                flag_delete = stepFeatures[4];

                double[][] data_imu = new double[9][end - first + 1];
                for (int i = 0; i < 9; i++) {
                    data_imu[i] = Arrays.copyOfRange(sensorsIMU[i], first, end + 1);
                }

                fea_imu[n_str] = imuFeaturesthetaReset(data_imu);

                if (toolMath.maxInArray(flag_delete).second > 0) {
                    id_delete.add(n_str);
                }

                id_replace[n_str] = flag_delete;  //check: flag_delete is array:1*8
                n_str++;
            }

            double[][] id_replace2 = new double[n_str][8];  // id_replace2 is 2d matric?
            for (int i = 0; i < n_str; i++) {
                id_replace2[i] = id_replace[i];
            }
            //2. delete strides/regions
            //2.1 delete strides
            //resize array for features
            if (n_str != gaitNumber - 1) {
                double[][] tmp_ct = new double[n_str][8];
                double[][] tmp_fm = new double[n_str][8];
                double[][] tmp_fmad = new double[n_str][8];
                double[][] tmp_fti = new double[n_str][8];
                double[][] tmp_imu = new double[n_str][21];

                for (int i = 0; i < n_str; i++) {
                    tmp_ct[i] = ct[i];
                    tmp_fm[i] = f_m[i];
                    tmp_fmad[i] = f_mad[i];
                    tmp_fti[i] = fti[i];
                    tmp_imu[i] = fea_imu[i];
                }
                ct = tmp_ct;
                f_m = tmp_fm;
                f_mad = tmp_fmad;
                fti = tmp_fti;
                fea_imu = tmp_imu;
            }
            ct_ave = new double[8];
            f_m_ave = new double[8];
            f_mad_ave = new double[8];
            fti_ave = new double[8];
            fea_imu_ave = new double[21];

            if (id_delete.size() == 0) {
                if (n_str != 0) { //changed to no. of valid stride
                    ct_ave = toolMath.mean1D(ct);
                    f_m_ave = toolMath.mean1D(f_m);
                    f_mad_ave = toolMath.mean1D(f_mad);
                    fti_ave = toolMath.mean1D(fti);
                    fea_imu_ave = toolMath.mean1D(fea_imu);
                } else {
                    ct_ave = toolMath.setZero1DArray(8);
                    f_m_ave = toolMath.setZero1DArray(8);
                    f_mad_ave = toolMath.setZero1DArray(8);
                    fti_ave = toolMath.setZero1DArray(8);
                    fea_imu_ave = toolMath.setZero1DArray(21);

                    flag_fea = 1;
                }
            } else {
                //some stride features have to be deleted
                for (int reg_ii = 0; reg_ii < 8; reg_ii++) {
                    int validStrideCount = 0;
                    ArrayList<Integer> id_right = new ArrayList<>();
                    for (int kk = 0; kk < n_str; kk++) { //check if n_str = num_id_replace
                        if (id_replace2[kk][reg_ii] < 1) {
                            validStrideCount++;
                            id_right.add(kk);
                        }
                    }
                    // at most 2 strides can be invalid && at most 2 strides for region_i can be invalid
                    if (validStrideCount > Math.min(gaitNumber - 3, 2) && validStrideCount > n_str - 2) {
                        ct_ave[reg_ii] = toolMath.meanSelectedIndex(ct, id_right, reg_ii);
                        f_m_ave[reg_ii] = toolMath.meanSelectedIndex(f_m, id_right, reg_ii);
                        f_mad_ave[reg_ii] = toolMath.meanSelectedIndex(f_mad, id_right, reg_ii);
                        fti_ave[reg_ii] = toolMath.meanSelectedIndex(fti, id_right, reg_ii);
                    } else {
                        ct_ave[reg_ii] = 0;
                        f_m_ave[reg_ii] = 0;
                        f_mad_ave[reg_ii] = 0;
                        fti_ave[reg_ii] = 0;

                        id_rep_reg.add(reg_ii);
                    }
                }

                int reg_imp_count = 0;
                boolean isRegion7 = false;
                for (int i = 0; i < id_rep_reg.size(); i++) {
                    if (id_rep_reg.get(i) != 5 || id_rep_reg.get(i) != 6) {
                        reg_imp_count++;
                        if (id_rep_reg.get(i) == 7) {
                            isRegion7 = true;
                        }
                    }
                }

                if (reg_imp_count > 0 && !isRegion7) {
                    id_rep_reg.add(7);
                }
                fea_imu_ave = toolMath.mean1D(fea_imu);
            }

            if (id_rep_reg.size() > 6) {
                flag_fea = 1;
            }


            if (n_str - 1 > Math.min(gaitNumber - 3, 2)) { // at most 2 stride can be wrong  || >=3strides
                if (ct_ave[7] > contact_t_max || ct_ave[7] < contact_t_min)
                    flag_fea = 1;
            } else
                flag_fea = 1;

            return true;
        }catch (Exception e ){
            e.printStackTrace();
            AnalysedData.errorMsg.append("Feature Calculation: " + e+ "\n");
            return false;
        }
    }
    private double thre_force_auto_7(double[] f_sum, double t_thre, double t_thre1, double t_thre2, double t_thre3, double t_thre4, double t_thre5, double t_thre6, double t_thre7){
        double thre_force =35;
        int n = f_sum.length;
        int num_thre1 = 0;
        int num_thre2 = 0;
        int num_thre3 = 0;
        int num_thre4 = 0;
        int num_thre5 = 0;
        int num_thre6 = 0;
        int num_thre7 = 0;

        for (int i = 0; i<n; i++){
            if (f_sum[i] >t_thre1) {
                num_thre1++;
                num_thre2++;
                num_thre3++;
                num_thre4++;
                num_thre5++;
                num_thre6++;
                num_thre7++;
            }else if (f_sum[i] >t_thre2) {
                num_thre2++;
                num_thre3++;
                num_thre4++;
                num_thre5++;
                num_thre6++;
                num_thre7++;
            }else if (f_sum[i] >t_thre3) {
                num_thre3++;
                num_thre4++;
                num_thre5++;
                num_thre6++;
                num_thre7++;
            }else if (f_sum[i] >t_thre4) {
                num_thre4++;
                num_thre5++;
                num_thre6++;
                num_thre7++;
            }else if (f_sum[i] >t_thre5) {
                num_thre5++;
                num_thre6++;
                num_thre7++;
            }else if (f_sum[i] >t_thre6) {
                num_thre6++;
                num_thre7++;
            }else  {
                num_thre7++;
            }
        }
        int num_thre = (int) Math.floor(n*t_thre);

        if (num_thre1 > num_thre){
            thre_force =  t_thre1;
        }else if (num_thre2 > num_thre){
            thre_force =  t_thre2;
        }else if (num_thre3 > num_thre){
            thre_force =  t_thre3;
        }else if (num_thre4 > num_thre){
            thre_force =  t_thre4;
        }else if (num_thre5 > num_thre){
            thre_force =  t_thre5;
        }else if (num_thre6 > num_thre){
            thre_force =  t_thre6;
        }else {
            thre_force =  t_thre7;
        }
        return thre_force;
    }


    private double thre_force_auto_3(double[] f_sum, double t_thre, double t_thre1, double t_thre2, double t_thre3){
        double thre_force =35;
        int n = f_sum.length;
        int num_thre1 = 0;
        int num_thre2 = 0;
        int num_thre3 = 0;

        for (int i = 0; i<n; i++){
            if (f_sum[i] >t_thre1) {
                num_thre1++;
                num_thre2++;
                num_thre3++;

            }else if (f_sum[i] >t_thre2) {
                num_thre2++;
                num_thre3++;

            }else{
                num_thre3++;
            }
        }
        int num_thre = (int) Math.floor(n*t_thre);

        if (num_thre1 > num_thre){
            thre_force =  t_thre1;
        }else if (num_thre2 > num_thre){
            thre_force =  t_thre2;
        }else{
            thre_force =  t_thre3;
        }

        return thre_force;
    }

    private double [][] forceFeatures(double[] time_temp, double [][] data_f, int step_s, int step_e ){
        try {
            //input with force data of just a step and extract the corresponding features
            int m = data_f[0].length;

            double[][] stepFeatures = new double[5][8]; //0: t_region, 1: fmax, 2: f_mad, 3: ft_region, 4: flag_delete

            double[] crite_s = new double[8];
            //double[] t_thre_tem={0.063, 0.215, 0.126, 0.081,0.027, 0.147, 0.185, 0.4905};
            double[] t_thre_tem={0.2892*0.7, 0.4553*0.7, 0.4695*0.7, 0.4137*0.7, 0.3997*0.7, 0.3944*0.7, 0.3287*0.7, 0.6263*0.7};
            //fix threshold
            crite_s[0]=10;
            crite_s[1]=3.3;
            crite_s[2]=6.7;
            crite_s[3]=3.3;
            crite_s[4]=0.7;
            crite_s[5]=0.7;
            crite_s[6]=10;
            crite_s[7]=20;
//            crite_s[0]= thre_force_auto_3(data_f[0],t_thre_tem[0], 10,  5,  2);
//            crite_s[1]= thre_force_auto_3(data_f[1],t_thre_tem[1], 3.3,  3,  2 );
//            crite_s[2]= thre_force_auto_3(data_f[2],t_thre_tem[2],  6.7,  5,  2);
//            crite_s[3]= thre_force_auto_3(data_f[3],t_thre_tem[3],  3.3,  2,  1);
//            crite_s[4]= thre_force_auto_7(data_f[4],t_thre_tem[4], 0.7, 0.5, 0.3, 0.2, 0.15, 0.1, 0.01);
//
//            crite_s[5]= thre_force_auto_7(data_f[5],t_thre_tem[5], 0.7, 0.5, 0.3, 0.2, 0.15, 0.1, 0.01);
//            crite_s[6]= thre_force_auto_3(data_f[6],t_thre_tem[6], 10,  5,  2);
 //           crite_s[7]= 10;
            analysedData.debugMsg.append("Fea Cal: crite_s:" +crite_s[0]+", "+crite_s[1]+", "+crite_s[2]+", "+crite_s[3]+", "+crite_s[4]+", "+crite_s[5]+", "+crite_s[6]+", "+crite_s[7] + "\n");
/*            t_thre_tem=[0.063, 0.215, 0.126, 0.081,0.027, 0.147, 0.185, 0.4905];
            crite_s(1)=thre_force_auto_3( data_f(:,1), t_thre_tem(1), 10,  6.7,  5  );
            crite_s(2)=thre_force_auto_3( data_f(:,2), t_thre_tem(2), 5,  3.3,  3  );
            crite_s(3)=thre_force_auto_3( data_f(:,3), t_thre_tem(3), 6.7,  5,  3.3  );
            crite_s(4)=thre_force_auto_3( data_f(:,4), t_thre_tem(4), 5,  3.3,  3  );
            for ii=5:6
            crite_s(ii)=thre_force_auto_7( data_f(:,ii), t_thre_tem(ii), 0.7, 0.5, 0.3, 0.2, 0.15, 0.12, 0.1 );
            end
            crite_s(7)=thre_force_auto_3( data_f(:,7), t_thre_tem(1), 10,  6.7,  5  );
            crite_s(8)=10;*/
              double [] crite_e = {2,2, 2, 2, 0.7, 0.7, 2, 20};
          //  double[] crite_e = crite_s;
            double[][] data_2 = data_f;

            //0. pre

            for (int ii = 0; ii < 8; ii++) {

                if (toolMath.maxInArray(data_f[ii]).second < 0.01) {
                    stepFeatures[0][ii] = 0;
                    stepFeatures[1][ii] = 0;
                    stepFeatures[2][ii] = 0;
                    stepFeatures[3][ii] = 0;
                    stepFeatures[4][ii] = 1;
                    continue;

                }

                double[] data_temp = data_f[ii];
                for (int i = 0; i < Math.max(step_s - 2, 0) + 1; i++) {
                    data_temp[i] = 0;
                }
                for (int i = Math.min(step_e + 2, m - 1); i < m; i++) {
                    data_temp[i] = 0;
                }
                data_2[ii] = data_temp;

                //1. cal
                int star_id = -1;
                int end_id = -1;

                //1.1 max id
                int max_id = toolMath.maxInArray(data_temp).first;
                //1.2 start_id
                double[] data_temp2 = Arrays.copyOfRange(data_temp, 0, max_id + 1);
                for (int i = max_id - 1; i > 0; i--) {
                    if (data_temp2[i] < crite_s[ii]) {
                        star_id = i + 1;
                        break;
                    }
                }

                //1.3 end_id
                if (star_id > -1) {
                    if (max_id > 0 && star_id < max_id && max_id < m) {
                        double[] data_temp3 = Arrays.copyOfRange(data_temp, max_id, m);
                        for (int i = 0; i < data_temp3.length; i++) {
                            if (data_temp3[i] < crite_e[ii]) {
                                end_id = max_id + i - 1;
                                break;
                            }
                        }
                    }
                }

                //return 0: t_region, 1: fmax, 2: f_mad, 3: ft_region, 4: flag_delete
                if (star_id > 0 && end_id > 0) {
                    if (max_id > star_id && end_id > max_id) {
                        int c_start_id = Math.max(star_id - 1, 0);
                        stepFeatures[0][ii] = time_temp[end_id] - time_temp[star_id];

                        stepFeatures[1][ii] = toolMath.maxInArray(data_temp).second;
                        stepFeatures[2][ii] = toolMath.mad(data_temp);
                        stepFeatures[3][ii] = toolMath.trapz(Arrays.copyOfRange(data_temp, c_start_id, end_id + 1)) / sensorFreq;
                        stepFeatures[4][ii] = 0;
                    } else {
                        stepFeatures[0][ii] = 0;
                        stepFeatures[1][ii] = 0;
                        stepFeatures[2][ii] = 0;
                        stepFeatures[3][ii] = 0;
                        stepFeatures[4][ii] = 1;
                    }
                } else { //delete this step
                    stepFeatures[0][ii] = 0;
                    stepFeatures[1][ii] = 0;
                    stepFeatures[2][ii] = 0;
                    stepFeatures[3][ii] = 0;
                    stepFeatures[4][ii] = 1;
                }
            }
            return stepFeatures;
        }catch (Exception e){
            e.printStackTrace();
            AnalysedData.errorMsg.append("Force features: " + e+ "\n");
            return null;
        }

    }
    private double[] imuFeaturesthetaReset(double[][] data){
        try {
            //data set :column
            int nn = data[0].length;
            // process outlier (wx, wy, wz; gx, gy, gz)

            for (int i = 3; i < 9; i++) {

            }
            double[] diff_tem = new double[nn - 1];

            ArrayList<Integer> idl_out = new ArrayList<>();
            for (int i = 1; i < nn; i++) {
                diff_tem[i - 1] = Math.abs(data[6][i] - data[6][i - 1]);
                if (diff_tem[i - 1] > 20) {
                    idl_out.add(i);
                }
            }
            int num_out = idl_out.size();
            for (int ii = 0; ii < num_out - 1; ii = ii + 2) {
                for (int j = 3; j < 9; j++) {
                    data[j][idl_out.get(ii) + 1] = 0.5 * (data[j][idl_out.get(ii)] + data[j][idl_out.get(ii) + 2]);
                }
            }

            //set theta center to 0
            int id_start = toolMath.maxInArray(Arrays.copyOfRange(data[6], 0, 30)).first;
            int id_start2 = toolMath.maxInArray(Arrays.copyOfRange(data[6], nn - 31, nn)).first;
            id_start2 = id_start2 + nn - 30 - 1; //correct?
            int id_15_per = id_start + Math.round((float) 0.15 * (id_start2 - id_start + 1)) - 1; //15%
            int id_25_per = id_start + Math.round((float) 0.25 * (id_start2 - id_start + 1)) - 1; //25%

            int num_p = id_25_per - id_15_per + 1;
            double[] delta_ave = new double[3];
            for (int i = 0; i < 3; i++) {
                delta_ave[i] = toolMath.sum(Arrays.copyOfRange(data[i + 6], id_15_per, id_25_per + 1)) / num_p;
                for (int j = 0; j < nn; j++) {
                    data[i + 6][j] -= delta_ave[i];
                }
            }

            //make sure theta is (-180,180)
            int overThresCount = 0;
            int belowThresCount = 0;
            for (int i = 6; i < 9; i++) {
                overThresCount = (toolMath.maxInArray(data[i]).second > 180) ? overThresCount + 1 : overThresCount;
                belowThresCount = (toolMath.minInArray(data[i]).second < -180) ? belowThresCount + 1 : belowThresCount;
            }

            if (overThresCount > 0 || belowThresCount > 0) {
                double[][] theta_tem = new double[3][nn];
                for (int i = 0; i < 3; i++) {
                    theta_tem[i] = data[i + 6];
                    for (int j = 0; j < nn; j++) {
                        if (theta_tem[i][j] > 180) {
                            theta_tem[i][j] -= 360;
                        }
                        if (theta_tem[i][j] < -180) {
                            theta_tem[i][j] += 360;
                        }
                    }
                    data[i + 6] = theta_tem[i];
                }

            }

            double[] feas_imu;
            double[][] dataExtracted = new double[9][id_start2 - id_start];
            for (int i = 0; i < 9; i++) {
                dataExtracted[i] = Arrays.copyOfRange(data[i], id_start, id_start2);
            }
            feas_imu = imuFeatures(dataExtracted);

            return feas_imu;
        }catch (Exception e){
            e.printStackTrace();
            AnalysedData.errorMsg.append("imuThetaReset: " + e + "\n");
            return  null;
        }
    }

    private double[] imuFeatures(double[][] data_imu) {
        //input with imu data of just a step and extract the corresponding features

        //for data_imu: index(0-2: acc_x,y,z; 3-5: gyo_x,y,z; 6-8:angle_x,y,z)

        double [] feas_imu = new double[21];
        //Index: 0.acc_max, 1.acc_rms, 2.acc_mad, 3.wx_max, 4.wx_rms, 5.wx_mad, 6.wy_max, 7.wy_rms, 8.wy_mad, 9.wz_max, 10.wz_rms, 11.wz_mad,
        // 12.gx_max, 13.gx_min, 14.gx_rms, 15.gx_mad, 16.gy_max, 17.gy_min, 18.gy_rms, 19.gy_mad,  20.gz_range

        int size = data_imu[0].length;
        //1 acc
        double[] acc = new double[size];
        for  (int i = 0; i < size; i++) {
            acc[i] = Math.sqrt(Math.pow(data_imu[0][i], 2) + Math.pow(data_imu[1][i], 2) + Math.pow(data_imu[2][i], 2));
        }
        feas_imu[0] = toolMath.maxInArray(acc).second; //acc_max
        feas_imu[1] = toolMath.rms(acc); //acc_rms
        feas_imu[2] = toolMath.mad(acc); //acc_mad

        //2. angular velocity
        feas_imu[3] = toolMath.maxInArray(data_imu[3]).second; //wx_max
        feas_imu[4] = toolMath.rms(data_imu[3]); //wx_rms
        feas_imu[5] = toolMath.mad(data_imu[3]); //wx_mad

        feas_imu[6] = toolMath.maxInArray(data_imu[4]).second; //wy_max
        feas_imu[7] = toolMath.rms(data_imu[4]); //wy_rms
        feas_imu[8] = toolMath.mad(data_imu[4]); //wy_mad

        feas_imu[9] = toolMath.maxInArray(data_imu[5]).second; //wz_max
        feas_imu[10] = toolMath.rms(data_imu[5]); //wz_rms
        feas_imu[11] = toolMath.mad(data_imu[5]); //wz_mad

        //3. rotation angle
        feas_imu[12] = toolMath.maxInArray(data_imu[6]).second; //gx_max
        feas_imu[13] = toolMath.minInArray(data_imu[6]).second; //gx_min
        feas_imu[14] = toolMath.rms(data_imu[6]); //gx_rms
        feas_imu[15] = toolMath.mad(data_imu[6]); //gx_mad

        feas_imu[16] = toolMath.maxInArray(data_imu[7]).second; //gy_max
        feas_imu[17] = toolMath.minInArray(data_imu[7]).second; //gy_min
        feas_imu[18] = toolMath.rms(data_imu[7]); //gy_rms
        feas_imu[19] = toolMath.mad(data_imu[7]); //gy_mad

        feas_imu[20] = toolMath.maxInArray(data_imu[8]).second - toolMath.minInArray(data_imu[8]).second; //gz_range

        return feas_imu;
    }


    public boolean featureEntry(){
        if(flag_fea <1 && id_rep_reg.size()<1){ // stride features are all ok, no adjustment needed
            if(id_pre > 2){ // have already >=3 valid sets of features
                for (int i = 0; i < 2; i++) {
                    sub_ct_previous[i] = sub_ct_previous[i+1];
                    sub_f_m_previous[i] = sub_f_m_previous[i+1];
                    sub_f_mad_previous[i] = sub_f_mad_previous[i+1];
                    sub_fti_previous[i] = sub_fti_previous[i+1];
                }
                sub_ct_previous[2] = ct_ave;
                sub_f_m_previous[2] = f_m_ave;
                sub_f_mad_previous[2] = f_mad_ave;
                sub_fti_previous[2] = fti_ave;
            }else{
                sub_ct_previous[id_pre] = ct_ave;
                sub_f_m_previous[id_pre] = f_m_ave;
                sub_f_mad_previous[id_pre] = f_mad_ave;
                sub_fti_previous[id_pre] = fti_ave;
                id_pre++;
            }

        }else if (flag_fea > 0){ // no stride feature
            analysedData.debugMsg.append("Fea Entry: flag_fea:" +flag_fea + "\n");
            return false; //get another 10sec data
        }else{ //some feature(s) is/ are wrong
            if (sub_ct_previous[0][0] < 0.001){
                analysedData.debugMsg.append("Fea Entry: sub_ct_previous null" + "\n");
                return false; //get another 10sec data
            }

            //1) use previous fea to substitute current fea
            for (int rep_i = 0; rep_i < id_rep_reg.size(); rep_i++){
                if (id_pre >2){
                    ct_ave[id_rep_reg.get(rep_i)] = toolMath.mean2D(sub_ct_previous, 3, id_rep_reg.get(rep_i));
                    f_m_ave[id_rep_reg.get(rep_i)] = toolMath.mean2D(sub_f_m_previous, 3, id_rep_reg.get(rep_i));
                    f_mad_ave[id_rep_reg.get(rep_i)] = toolMath.mean2D(sub_f_mad_previous, 3, id_rep_reg.get(rep_i));
                    fti_ave[id_rep_reg.get(rep_i)] = toolMath.mean2D(sub_fti_previous, 3, id_rep_reg.get(rep_i));
                    }else{
                    ct_ave[id_rep_reg.get(rep_i)] = toolMath.mean2D(sub_ct_previous, id_pre, id_rep_reg.get(rep_i));
                    f_m_ave[id_rep_reg.get(rep_i)] = toolMath.mean2D(sub_f_m_previous, id_pre, id_rep_reg.get(rep_i));
                    f_mad_ave[id_rep_reg.get(rep_i)] = toolMath.mean2D(sub_f_mad_previous, id_pre, id_rep_reg.get(rep_i));
                    fti_ave[id_rep_reg.get(rep_i)] = toolMath.mean2D(sub_fti_previous, id_pre, id_rep_reg.get(rep_i));
                }
            }
            // 2) record current seg fea
            // 2).1 assign right fea to previous_fea
            ArrayList<Integer> id_keep_reg = new ArrayList<>();
            boolean same;
            for (int i = 0; i < 8;i++){
                same = false;
                for (int j = 0; j <id_rep_reg.size(); j++){
                    if (id_rep_reg.get(j) == i){
                        same = true;
                        break;
                    }
                }
                if (same == false){
                    id_keep_reg.add(i);
                }
            }
            int flag_stride_new = 1;
            if(id_pre >2){
                for (int j = 0; j < id_keep_reg.size(); j++) {
                    for (int i = 0; i < 2; i++) {
                        sub_ct_previous[i][id_keep_reg.get(j)] = sub_ct_previous[i + 1][id_keep_reg.get(j)];
                        sub_f_m_previous[i][id_keep_reg.get(j)] = sub_f_m_previous[i + 1][id_keep_reg.get(j)];
                        sub_f_mad_previous[i][id_keep_reg.get(j)] = sub_f_mad_previous[i + 1][id_keep_reg.get(j)];
                        sub_fti_previous[i][id_keep_reg.get(j)] = sub_fti_previous[i + 1][id_keep_reg.get(j)];
                    }
                }
                for (int j = 0; j < id_keep_reg.size(); j++) {
                    sub_ct_previous[2][id_keep_reg.get(j)] = ct_ave[id_keep_reg.get(j)];
                    sub_f_m_previous[2][id_keep_reg.get(j)] = f_m_ave[id_keep_reg.get(j)];
                    sub_f_mad_previous[2][id_keep_reg.get(j)] = f_mad_ave[id_keep_reg.get(j)];
                    sub_fti_previous[2][id_keep_reg.get(j)] = fti_ave[id_keep_reg.get(j)];

                }
            }else{
                if (flag_stride_new > 0){
                    id_pre ++;
                    flag_stride_new = 0;
                }
                for (int j = 0; j < id_keep_reg.size(); j++) {
                    sub_ct_previous[id_pre-1][id_keep_reg.get(j)] = ct_ave[id_keep_reg.get(j)];
                    sub_f_m_previous[id_pre-1][id_keep_reg.get(j)] = f_m_ave[id_keep_reg.get(j)];
                    sub_f_mad_previous[id_pre-1][id_keep_reg.get(j)] = f_mad_ave[id_keep_reg.get(j)];
                    sub_fti_previous[id_pre-1][id_keep_reg.get(j)] = fti_ave[id_keep_reg.get(j)];
                }
            }

            //2).2 wrong fea in previous_fea
            if ((id_pre <3 || (id_pre > 2 && flag_stride_new <1)) && id_pre >0){
                for (int i =0; i < id_rep_reg.size(); i++){
                    sub_ct_previous[id_pre-1][id_rep_reg.get(i)] = sub_ct_previous[id_pre-2][id_rep_reg.get(i)];
                    sub_f_m_previous[id_pre-1][id_rep_reg.get(i)] = sub_f_m_previous[id_pre-2][id_rep_reg.get(i)];
                    sub_f_mad_previous[id_pre-1][id_rep_reg.get(i)] = sub_f_mad_previous[id_pre-2][id_rep_reg.get(i)];
                    sub_fti_previous[id_pre-1][id_rep_reg.get(i)] = sub_fti_previous[id_pre-2][id_rep_reg.get(i)];
                }
            }else if (id_pre > 2 && flag_stride_new >0){
                for (int i =0; i < id_rep_reg.size(); i++){
                    for (int j = 0; j < 2; j++) {
                        sub_ct_previous[j][id_rep_reg.get(i)] = sub_ct_previous[j + 1][id_rep_reg.get(i)];
                        sub_f_m_previous[j][id_rep_reg.get(i)] = sub_f_m_previous[j + 1][id_rep_reg.get(i)];
                        sub_f_mad_previous[j][id_rep_reg.get(i)] = sub_f_mad_previous[j + 1][id_rep_reg.get(i)];
                        sub_fti_previous[j][id_rep_reg.get(i)] = sub_fti_previous[j + 1][id_rep_reg.get(i)];
                    }
                    sub_ct_previous[2][id_rep_reg.get(i)] = sub_ct_previous[1][id_rep_reg.get(i)];
                    sub_f_m_previous[2][id_rep_reg.get(i)] = sub_f_m_previous[1][id_rep_reg.get(i)];
                    sub_f_mad_previous[2][id_rep_reg.get(i)] = sub_f_mad_previous[1][id_rep_reg.get(i)];
                    sub_fti_previous[2][id_rep_reg.get(i)] = sub_fti_previous[1][id_rep_reg.get(i)];
                }
            }
           // id_pre++;
        }
        analysedData.setAll_ct(ct_ave);
        analysedData.setAll_f_m(f_m_ave);
        analysedData.setAll_f_mad(f_mad_ave);
        analysedData.setAll_fti(fti_ave);
        analysedData.setSub_ct_previous(sub_ct_previous);
        analysedData.setSub_f_m_previous(sub_f_m_previous);
        analysedData.setSub_f_mad_previous(sub_f_mad_previous);
        analysedData.setSub_fti_previous(sub_fti_previous);
        analysedData.setSub_imu_ave(fea_imu_ave);
        analysedData.setId_pre(id_pre);

        return true;
    }



}
