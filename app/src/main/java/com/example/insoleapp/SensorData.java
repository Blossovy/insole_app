package com.example.insoleapp;

import android.os.SystemClock;
import android.util.Log;

import com.github.mikephil.charting.data.Entry;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SensorData {
    //this class is a singleton
    private static SensorData instance;
    public final static int sensorFreq = 110; //Hz
    public static ToolMath toolMath = new ToolMath();

    // for data processing, collect 10sec data for every 5 min
    public final static int datalength = sensorFreq*10; // 100Hz data for 10 sec
    public static boolean dataLogProcess = false;
    private static int analysisIndexL = -1;
    private static int analysisIndexR = -1;
  //  private static int imuIndexL = 0;
 //   private static int imuIndexR = 0;
    //index 0 = time from circuit box, 1-7 = FSR (Toe, M1, M5, Mid, March, Larch, Heel), 8-16 = IMU (acc_x,y,z; gyo_x,y,z; angle_x,y,z)
    private static double[][] analysisL = new double[datalength][17];
    private static double[][] analysisR = new double[datalength][17];
  //  private static float[][] imuL = new float[10][datalength];
  //  private static float[][] imuR = new float[10][datalength];

    // for signal display, storing last 10 sec data at 20 Hz
    private static int displayLogCountL = 1;
    private static int displayFreq = 5; //display every 5 data, i.e. 100/5 = 20Hz
    private static int arrayListStoreNum = sensorFreq/displayFreq*10;
    private static boolean fsrArrayShiftL = false;
    private static boolean imuArrayShiftL = false;
    private static boolean fsrArrayShiftR = false;
    private static boolean imuArrayShiftR = false;
    private static int fsrLShiftCount = 0;
    private static int imuLShiftCount = 0;

    private static ArrayList<Entry>[] display_FSRL = (ArrayList<Entry>[]) new ArrayList[7];
    private static ArrayList<Entry>[] display_FSRR = (ArrayList<Entry>[]) new ArrayList[7];
    private static ArrayList<Entry>[] display_IMUL = (ArrayList<Entry>[]) new ArrayList[9];
    private static ArrayList<Entry>[] display_IMUR = (ArrayList<Entry>[]) new ArrayList[9];

    public static String rawDataL;
    public static String rawDataR;
    private static final String TAG = "Sensor Data";
    public static ShoeInfo shoeinfo = ShoeInfo.getInstance();
    public static AnalysedData analysedData = AnalysedData.getInstance();

    //parse data Left
    private static boolean firstEntryL = true;
    private static boolean firstIMUL = true;
    private static long beginning_systimeL = -1;
    //parse data Right
    private static boolean firstEntryR = true;
    private static boolean firstIMUR = true;
    private static long beginning_systimeR = -1;

    // for SMV count
    public static int smvCount = 0;

    private SensorData(){}

    public static SensorData getInstance(){
        if (instance == null){
            instance = new SensorData();
            initialiseData();
        }
        return instance;
    }

    public static boolean parseDataL (String data) {
        /*parsing data and stored in two places:
        1) fsr and imu for data processing
        2) display_FSR and display_imu for data display in signal fragment
        */
        int time;
        boolean imuAvail = false;
        double[] last_sensorL = new double[17];
        for (int i = 0; i < 8; i++ ){
            last_sensorL[i] = -1;
        }


        Log.d(TAG, "parse: " + data);
        try {
            if(Pattern.compile("^T=.*#").matcher(data).find()) {

                //Battery level extraction
                Pattern dataB = Pattern.compile("B=\\d*;");
                Matcher matcherB = dataB.matcher(data);
                while (matcherB.find()) {
                    int b_lvl = Integer.parseInt(matcherB.group().replaceAll("[^0-9]", ""));
                    Log.d(TAG, "Battery  match" + b_lvl);
                    shoeinfo.setBatterylvlL(b_lvl);

                    data = data.replaceAll("B=\\d*;", "");
                    Log.d(TAG, "Battery level found");
                }

                Pattern dataA = Pattern.compile("-?\\d+(\\.\\d+)?");
                Matcher matcherA = dataA.matcher(data);

                //first entry is time
                matcherA.find();
                time = Integer.parseInt(matcherA.group());

                //last_fsrL[0] = time;
               // Log.d(TAG, "dataParse: beginning time" + beginning_systimeL);

                Log.d(TAG, "time: " + time);
                if (firstEntryL) {
                    beginning_systimeL = SystemClock.elapsedRealtime();
                    Log.d(TAG, "dataParse: beginning time" + beginning_systimeL);
                    firstEntryL = false;
                    shoeinfo.setSensorsAvailL(0, true);
                }

                displayFSRDataShiftL();
                displayIMUDataShiftL();


                boolean shouldLog = false;
                if (dataLogProcess || (!fsrArrayShiftL && displayLogCountL == displayFreq) || (!imuArrayShiftL && displayLogCountL == displayFreq))
                    shouldLog = true;

                if(shouldLog) {
                    last_sensorL[0] = (double) time;
                    //for FSR data
                    for (int i = 1; i < 8; i++) {
                        if (matcherA.find()) {
                            double fsrRead = Double.parseDouble(matcherA.group());
                            last_sensorL[i] = fsrRead;
                        }
                    }


               /* for (int i = 1; i < 8; i++) {
                    if (matcherA.find()) {
                        double fsrRead = Double.parseDouble(matcherA.group());
                        //last_fsrL[i] = fsrRead;

                        if (dataLogProcess) {
                            // analysisL[analysisIndexL][i] = fsrRead;
                            last_sensorL[i] = fsrRead;
                        }
                        if (!fsrArrayShiftL && displayLogCountL == displayFreq)
                            display_FSRL[i - 1].add(new Entry(time, (float) fsrRead));
                    }
                }*/

                    //check if IMU is available
                    if (data.contains("I") || data.contains("II")) {
                        imuAvail = true;
                        if (firstIMUL) {
                            shoeinfo.setSensorsAvailL(1, true);
                            firstIMUL = false;
                        }
                        for (int i = 0; i < 9; i++) {
                            if (matcherA.find()) {
                                double imuRead = Double.parseDouble(matcherA.group());
                                last_sensorL[i + 8] = imuRead;
                            }
                        }
                    }

                    //check data null
                    boolean dataCheckNull = false;
                    int count = 0;
                    for (int i = 0; i<17; i++){
                        if (last_sensorL[i] == -1 || last_sensorL == null){
                            count++;
                        }
                    }
                    if (count >4) {
                        Log.d(TAG + " DROP", "checker null");
                        dataCheckNull = true;
                    }
                    if (!dataCheckNull){
                        if (dataLogProcess) {
                            int before = analysisIndexL;
                            Log.d(TAG,"before process: " +analysisIndexL);
                            if (analysisIndexL >= 0) {
                                if (analysisL[analysisIndexL][0] != 0.0)
                                    analysisIndexL++;
                            } else if (analysisIndexL == -1)
                                analysisIndexL++;
                            if (before != analysisIndexL-1){
                                Log.d(TAG+" DROP","index not increasing");
                            }
                            analysisL[analysisIndexL] = last_sensorL;
                            Log.d(TAG,"after process: " +analysisIndexL);
                        }

                        if (!fsrArrayShiftL && displayLogCountL == displayFreq){
                            for (int i = 1; i<8; i++){
                                display_FSRL[i - 1].add(new Entry(time, (float) last_sensorL[i]));
                            }
                        }
                        if(!imuArrayShiftL && displayLogCountL == displayFreq){
                            for (int i = 0; i<9; i++) {
                                display_IMUL[i].add(new Entry(time, (float) last_sensorL[i+8]));
                            }
                        }
                    }

                }
                /*if (data.indexOf("I") > -1 || data.indexOf("II") > -1) {
                    imuAvail = true;
                    if (firstIMUL) {
                        shoeinfo.setSensorsAvailL(1, true);
                        firstIMUL = false;
                    }
                    displayIMUDataShiftL();

//                if (imuIndexL == datalength){
//                    imuL = new float[10][datalength];
//                    imuIndexL = 0;
//                }
//                if (dataLogProcess) {
//                    imuL[0][imuIndexL] = time;
//                    imuIndexL++;
//                }
                    //last_imuL[0] = time;
                    for (int i = 0; i < 9; i++) {
                        if (matcherA.find()) {
                            double imuRead = Double.parseDouble(matcherA.group());
                            //last_imuL[i] = imuRead;

                            if (dataLogProcess) {
                                //analysisL[analysisIndexL][i + 8] = imuRead;
                                last_sensorL[i + 8] = imuRead;
                            }
                            if (!imuArrayShiftL && displayLogCountL == displayFreq) {
                                display_IMUL[i].add(new Entry(time, (float) imuRead));
                            }
                        }
                    }
                }*/
                Log.d(TAG, "indexL: " + analysisIndexL +" datalength-1: " +datalength );
                if (analysisIndexL == datalength - 1) {
                    if (dataLogProcess) {
                        Log.d(TAG, "analysis data ready to log3rdfiltered");
                        dataLogProcess = false;
                        final double[][] dataL = analysisL;
                        Thread setAnaDataThread = new Thread(new Runnable() {
                            @Override
                            public void run() {
                                Log.d(TAG, "data ana set Thread ID: " + Thread.currentThread().getId());

                                analysedData.setSensorRaw(dataL);
                            }
                        });
                        setAnaDataThread.start();
                        // MainActivity.exportData.singleEntry_analysis(analysisL);
                        resetAnalysisL();
                    }
                }

                Log.d(TAG, imuAvail ? "IMU detected" : "no IMU");

                if (displayLogCountL < displayFreq)
                    displayLogCountL++;
                else if (displayLogCountL == displayFreq)
                    displayLogCountL = 1;
                else if (displayLogCountL > displayFreq)
                    displayLogCountL = 1;
            }else {
                Log.d(TAG + " DROP", "not fulfilling T=#  find");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return imuAvail;
    }

    static void displayFSRDataShiftL(){
            //shift FSR display array if array reaches assigned store number
        double [] displayDataSize = new double[display_FSRL.length];
        for (int i = 0; i< display_FSRL.length; i++){
            displayDataSize[i] = display_FSRL[i].size();
        }
        int size = (int) Math.round(toolMath.minInArray(displayDataSize).second);
        if (display_FSRL[0].size() == arrayListStoreNum && !fsrArrayShiftL){
            fsrArrayShiftL = true;

            Log.d(TAG, "FSRL shift from " + size/ 2 + "to " + (size-2));
            ArrayList<Entry>[] tmp =(ArrayList<Entry>[]) new ArrayList[7];
            for (int j = 0; j < 7; j++) {
                tmp[j] = new ArrayList<>(display_FSRL[j].subList(size/ 2, size-2));
            }
            display_FSRL = tmp;
        }
        fsrArrayShiftL = false;


    }

    static void displayIMUDataShiftL(){
        double [] displayDataSize = new double[display_IMUL.length];
        for (int i = 0; i< display_IMUL.length; i++){
            displayDataSize[i] = display_IMUL[i].size();
        }
        int size = (int) Math.round(toolMath.minInArray(displayDataSize).second);

        if (display_IMUL[8].size() == (arrayListStoreNum) && !imuArrayShiftL){
            imuArrayShiftL = true;

            ArrayList<Entry>[] tmp =(ArrayList<Entry>[]) new ArrayList[9];
            for (int j = 0; j< 9; j++){
                Log.d(TAG, "IMUL shift from " + size/2 +  "to " + (size-2));
                tmp[j] = new ArrayList<>(display_IMUL[j].subList(size/2,size-2));
            }
            display_IMUL = tmp;
        }
        imuArrayShiftL = false;

    }

    public static boolean parseDataR (String data) {
        /*parsing data and stored in three places:
        1) last_fsr/last_imu for data export - cancelled
        2) fsr and imu for data processing
        3) display_FSR and display_imu for data display in signal fragment
        */
        int time;
        boolean imuAvail = false;
        Log.d(TAG, "parse: " + data);
        try {
            if(dataLogProcess)
                Log.d(TAG,"Analysis data will be logged");
            //Battery level extraction
            Pattern dataB = Pattern.compile("B=\\d*");
            Matcher matcherB = dataB.matcher(data);
            while (matcherB.find()) {
                int b_lvl = Integer.parseInt(matcherB.group().replaceAll("[^0-9]", ""));
                Log.d(TAG, "Battery  match" + b_lvl);
                shoeinfo.setBatterylvlR(b_lvl);

                data = data.replaceAll("B=\\d*;", "");
                Log.d(TAG, "Battery level found");
            }

            Pattern dataA = Pattern.compile("-?\\d+(\\.\\d+)?");
            Matcher matcherA = dataA.matcher(data);

            //first entry is time
            matcherA.find();
            time = Integer.parseInt(matcherA.group());
            //last entry log3rdfiltered
            //last_fsrR[0] = time;
            if (dataLogProcess){
                analysisR[analysisIndexR][0] = time;
            }
            Log.d(TAG, "time: " + time);
            if (firstEntryR) {
                beginning_systimeR = SystemClock.elapsedRealtime();
                Log.d(TAG, "dataParse: beginning time" + beginning_systimeR);
                firstEntryR = false;
                shoeinfo.setSensorsAvailR(0,true);
            }
            displayFSRDataShiftR();

//            if (analysisIndexR == datalength){
//                fsrR = new int[8][datalength];
//                analysisIndexR = 0;
//            }

            for (int i = 1; i < 8; i++) {
                if (matcherA.find()) {
                    int fsrRead = Integer.parseInt(matcherA.group());
                    //last_fsrR[i] = fsrRead;

                    if (dataLogProcess){
                        analysisR[analysisIndexR][i] = fsrRead;
                    }
                    if (!fsrArrayShiftR) {
                        display_FSRR[i - 1].add(new Entry(time, fsrRead));
                    }
                }
            }

            //check if IMU is available
            if (data.indexOf("I") > -1  || data.indexOf("II") > -1 ) {
                imuAvail = true;
                if (firstIMUR) {
                    shoeinfo.setSensorsAvailR(1, true);
                    firstIMUR = false;
                }
                displayIMUDataShiftR();

//                if (imuIndexR == datalength){
//                    imuR = new float[10][datalength];
//                    imuIndexR = 0;
//                }

//                imuR[0][imuIndexR] = time;
//                imuIndexR ++;

                //last_imuR[0] = time;
                for (int i = 0; i < 9; i++) {
                    if (matcherA.find()) {
                        float imuRead = Float.parseFloat(matcherA.group());
                        //last_imuR[i] = imuRead;

                        if (dataLogProcess) {
                            analysisR[analysisIndexR] [i+8]= imuRead;
                        }
                        if (!imuArrayShiftR) {
                            display_IMUR[i].add(new Entry(time, imuRead));
                        }
                    }
                }
            }
            //Log.d(TAG, Arrays.toString(last_fsrR));
           // Log.d(TAG, imuAvail? "IMU detected":"no IMU");
//        if (imuAvail)
//            Log.d(TAG, Arrays.toString(last_imuR));

            if(dataLogProcess)
                analysisIndexR ++;
            fsrArrayShiftR = false;
            imuArrayShiftR = false;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return imuAvail;
    }

    static void displayFSRDataShiftR(){
        if (display_FSRR[0].size() == arrayListStoreNum-1 && !fsrArrayShiftR){
            fsrArrayShiftR = true;
        }
        //shift FSR display array if array reaches assigned store number
        if (display_FSRR[0].size()!=0 && display_FSRR[0].size() == arrayListStoreNum && fsrArrayShiftR) {

            Log.d(TAG,"shift right fsr Thread ID: " + Thread.currentThread().getId());
            ArrayList<Entry>[] tmp =(ArrayList<Entry>[]) new ArrayList[7];
            for (int j = 0; j < 7; j++) {
                Log.d(TAG, "FSRR shift from " + display_FSRR[0].size()/2 +  "to " + (display_FSRR[0].size()-1));
                tmp[j] = new ArrayList<>(display_FSRR[j].subList(display_FSRR[0].size()/2, display_FSRR[0].size()-1));
            }
            display_FSRR = tmp;
        }
    }

    static void displayIMUDataShiftR(){
        if (display_IMUR[0].size() == (arrayListStoreNum)-1 && !imuArrayShiftR){
            imuArrayShiftR = true;
        }
        //shift IMU display array if array reaches assigned store number

        if (display_IMUR[0].size() != 0 && display_IMUR[0].size() == arrayListStoreNum && imuArrayShiftR){
            Log.d(TAG,"shift right imu Thread ID: " + Thread.currentThread().getId());
            ArrayList<Entry>[] tmp =(ArrayList<Entry>[]) new ArrayList[9];
            for (int j = 0; j< 9; j++){
                Log.d(TAG, "IMUR shift from " + display_IMUR[0].size()/4 +  "to " + (display_IMUR[0].size()-1));
                tmp[j] = new ArrayList<>(display_IMUR[j].subList(display_IMUR[0].size()/4, display_IMUR[0].size()-1));
            }
            display_IMUR = tmp;
        }
    }
    public static ArrayList<Entry>[] getDisplay_FSRL(){ return display_FSRL;}
    public static ArrayList<Entry>[] getDisplay_FSRR(){return display_FSRR;}
    public static ArrayList<Entry>[] getDisplay_IMUL(){return display_IMUL;}
    public static ArrayList<Entry>[] getDisplay_IMUR(){return display_IMUR;}
    public static boolean getIsFSRshiftingL(){return fsrArrayShiftL;}
    public static boolean getIsIMUshiftingL(){return imuArrayShiftL;}
    public static boolean getIsFSRshiftingR(){return fsrArrayShiftR;}
    public static boolean getIsIMUshiftingR(){return imuArrayShiftR;}
    public static double[][] getAnalysisL(){return analysisL;}
    public static void resetAnalysisL(){
        analysisIndexL = -1;
        analysisL = new double[datalength][17];

    }

    public static void resetAnalysisR(){
        analysisIndexR = -1;
        analysisR = new double[datalength][17];
    }

    public static void resetDisplayL(){
        display_FSRL = (ArrayList<Entry>[]) new ArrayList[7];
        for(int k=0; k<7; k++){
            display_FSRL[k] = new ArrayList<>();
        }        display_IMUL = (ArrayList<Entry>[]) new ArrayList[9];
        for(int k=0; k<9; k++){
            display_IMUL[k] = new ArrayList<>();
        }
        fsrArrayShiftL = false;
        imuArrayShiftL = false;
        displayLogCountL = 1;
    }

    public static void resetDisplayR(){
        display_FSRR = (ArrayList<Entry>[]) new ArrayList[7];
        for(int k=0; k<7; k++){
            display_FSRR[k] = new ArrayList<>();
        }
        display_IMUR = (ArrayList<Entry>[]) new ArrayList[9];
        for(int k=0; k<9; k++){
            display_IMUR[k] = new ArrayList<>();
        }
        fsrArrayShiftR = false;
        imuArrayShiftR = false;
    }
    public static void initialiseData(){

        resetAnalysisL();
        resetAnalysisR();
        resetDisplayL();
        resetDisplayR();

        dataLogProcess = false;

        firstEntryL = true;
        firstIMUL = true;
        beginning_systimeL = -1;

        firstEntryR = true;
        firstIMUR = true;
        beginning_systimeR = -1;
    }

}
