package com.example.insoleapp;

import android.util.Log;
import android.util.Pair;

import java.util.ArrayList;
import java.util.Arrays;

public class ToolMath {
    private final static String TAG = "ToolMath";

    public ToolMath(){ }
    //mode  // delete this angle part since angle processed in "imuFeaturesthetaReset" (Line 297~335)
    public double mode(double [] data){
        if (data != null){
            double maxValue = data[0];
            int maxCount = 1;
            for (int i = 0; i < data.length; i++){
                int count = 1;
                for (int j = 0; j< data.length; j++){
                    if (data[j] == data[i]){
                        count ++;
                    }
                }
                if (count > maxCount){
                    maxCount = count;
                    maxValue = data[i];
                }
            }
            return maxValue;
        }
        return 0;
    }

    //mad
    public double mad (double [] data){
        double mean = average(data);
        double [] absoluteDe = new double[data.length];
        for (int i = 0 ; i < data.length ; i++){
            absoluteDe[i] = Math.abs(data[i] - mean);
        }
        return average(absoluteDe);
    }

    public double average (double [] data){
        double sum = 0;
        for (double num: data){
            sum += num;
        }
        double average = sum/data.length;
        return average;
    }

    public double median (double [] data){
        Arrays.sort(data);
        int index_mid = data.length/2;
        if (data.length%2 == 1){
            return data[index_mid];
        }else {
            return (data[index_mid-1] + data[index_mid])/2.0;
        }
    }

    //rms
    public double rms(double [] data){
        double square = 0;
        int datalength = data.length;
        for (int i = 0; i < datalength; i++){
            square += Math.pow(data[i], 2);
        }
        double mean = square/datalength;
        return  Math.sqrt(mean);
    }

    //max of an array and its index
    public Pair<Integer, Double> maxInArray (double[] data){
        int index = 0;
        double max = data[0];
   //     Log.d(TAG, "maxInArray index: " + index + "max: " + max);
        for (int i = 0; i< data.length; i++){
            if (data[i] > max){
                index = i;
                max = data[i];
            }
        }
        return new Pair <Integer, Double> (index, max);
    }

    public Pair<Integer, Double> minInArray (double[] data){
        int index = 0;
        double min = data[0];
        for (int i = 0; i< data.length; i++){
            if (data[i] < min){
                index = i;
                min = data[i];
            }
        }
        return new Pair <Integer, Double> (index, min);
    }
    //trapz
    public double trapz (double [] data){
        double sum = 0;
        sum += data[0];

        for(int i = 1; i< data.length-1; i++){
            sum += data[i] * 2;
        }
        // last one
        sum += data[data.length-1];
        sum = sum/2; //assuming all heights are 1 unit
        return  sum;
    }

    //mean according to last dimension (col)
    public double[] mean1D(double [][] data){
        double [] avg = new double [data[0].length];
        Log.d(TAG,"data.length " + data.length );//10
        Log.d(TAG,"data[0].length " + data[0].length );//8
        for (int j = 0; j < data[0].length; j++){
            for (int i = 0; i<data.length; i++){
                avg[j] += data[i][j];
            }
            avg[j] = avg[j]/data.length;
        }
        return avg;
    }

    public double sum (double [] data){
        double sum = 0;
        for (int i = 0; i < data.length; i++){
            sum += data[i];
        }
        return sum;
    }

    public double mean2D(double [][] data, int maxIndex, int region){
        double sum = 0;
        for (int i = 0; i< maxIndex; i++){
                sum += data[i][region];
            }
        return sum/(maxIndex );
    }

    public double meanSelectedIndex(double[][] data, ArrayList<Integer> right_index, int region_index){
        double sum = 0;
        for (int i = 0; i<right_index.size(); i++){
                sum += data[right_index.get(i)][region_index];
            }
        return sum/right_index.size();
    }

    public double [] setZero1DArray(int n){
        double [] zeros = new double[n];
        for (int i = 0; i < n; i++){
            zeros[i] = 0;
        }
        return zeros;
    }

}
