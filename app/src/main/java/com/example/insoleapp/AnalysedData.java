package com.example.insoleapp;

import android.util.Log;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;

public class AnalysedData {
    //this class is a singleton
    private static AnalysedData instance;

    public static SensorData sensorData = SensorData.getInstance();
    private ExportData exportData;
    private SVModel svModel;
    private ToolMath toolMath = new ToolMath();

    public final static int dataPoint = 40; // every 5-min window is treated as one datapoint i.e. when datapoint = 1, T = 5min
    private static int sensorDataNum = 0;
    private static double [][][] sensorRaw = new double[dataPoint][sensorData.datalength][17];
    private static int [] segmentStartT = new int [dataPoint];
    private static int [] segmentEndT = new int [dataPoint];
    //index 0 = time from circuit box, 1-7 = FSR (Toe, M1, M5, Mid, March, Larch, Heel), 8-16 = IMU (acc_x,y,z; gyo_x,y,z; angle_x,y,z), 17 = time from 0 i.e. sensorTmp[i] - sensorTmp[1], 18 = FSR total
    public static double [][] sensorTmp = new double[sensorData.datalength][17];
    private static double [][] sensorTmpReverse = new double[17][sensorData.datalength];
    private static double [] timeDiff = new double[sensorData.datalength];
    private static double [][] sensorFSR = new double[8][sensorData.datalength];
    private static double [][] sensorIMU = new double[9][sensorData.datalength];
    public static ArrayList<Integer>[] gait_index_rough;
    public static ArrayList<Integer>[] gait_index;

    public static double[][] sub_ct_previous = new double [3][8];
    public static double[][] sub_f_m_previous= new double [3][8];
    public static double[][] sub_f_mad_previous = new double [3][8];
    public static double[][] sub_fti_previous = new double [3][8];
    public static double[][] all_ct = new double [dataPoint][8];
    public static double[][] all_f_m= new double [dataPoint][8];
    public static double[][] all_f_mad = new double [dataPoint][8];
    public static double[][] all_fti = new double [dataPoint][8];
    public static double [][] all_imu = new double[dataPoint][21];
    public static double [] firstFeatures = new double [53];
    public static int id_pre;

    public static double [] svm_yPre  = new double[dataPoint];
    public static int svmFatigueCount = 0;

    public static StringBuilder errorMsg = new StringBuilder();
    public static StringBuilder yPreMsg = new StringBuilder();
    public static StringBuilder debugMsg = new StringBuilder();
    public static double [][] logImuConversion = new double[17][sensorData.datalength];
    public static double [][] log3rdfiltered = new double[17][sensorData.datalength];
    public static double [][] log4thfiltered = new double[17][sensorData.datalength];
    public static double [][] log2ndfiltered = new double[17][sensorData.datalength];
    public static double [][] log1stfiltered = new double[17][sensorData.datalength];

   // private static int[] timeDiff = new int [sensorData.datalength];

    private static final String TAG = "Analysed Data";

    private AnalysedData(){}

    public static AnalysedData getInstance(){
        if (instance == null){
            instance = new AnalysedData();
        }
        return instance;
    }

    public void setSensorRaw(double[][] d){
        long anaStart = System.currentTimeMillis();
        errorMsg = new StringBuilder();
        debugMsg = new StringBuilder();
        sensorTmp = d;
        if (sensorDataNum != dataPoint) {
            sensorFSR = new double[8][sensorData.datalength];
            sensorIMU = new double[9][sensorData.datalength];
            //sensorRaw[sensorDataNum] = sensorTmp;
            setSegmentStartT((int)sensorTmp[0][0]);
            setSegmentEndT((int) sensorTmp[sensorData.datalength-1][0]);
            sortData();
         //   MainActivity.exportData.singleEntry_analysis(sensorTmp,  false, "sorted");
            if (!dataChecker()){
                MainActivity.dataCheckerFail = true;
            }else{
                dataColumnReverse();

                //**** for data processing ****
                DataPreprocessing dataPreprocessing= new DataPreprocessing(sensorTmpReverse, timeDiff);
                dataPreprocessing.imuConversion();
                dataPreprocessing.filter_continuousSignalError();
                dataPreprocessing.filter_butter();
                dataPreprocessing.totalForce();

                //**** for gait extraction ****

                GaitExtraction gaitExtraction = new GaitExtraction(timeDiff, sensorFSR, sensorIMU);
               if(!gaitExtraction.isWalking_imu()){
                   MainActivity.dataCheckerFail = true;
                   errorMsg.append( "Gait Extraction: not walking #" +sensorDataNum);
                }else{
                   gaitExtraction.gaitRecognitionMid(gaitExtraction.gaitRecognitionRough());
                   gait_index = gaitExtraction.gaitRecognitionFine();
                   if (!gaitExtraction.isGaitRegValid(gait_index)){
                       MainActivity.dataCheckerFail = true;
                       errorMsg.append("Gait Extraction: index not valid #" +sensorDataNum);
                   }else{

                //**** for feature extraction ****
                     FeatureExtraction featureExtraction = new FeatureExtraction(timeDiff,sensorFSR, sensorIMU, gait_index, sub_ct_previous, sub_f_m_previous, sub_f_mad_previous, sub_fti_previous, id_pre);
                     if (!featureExtraction.featureCalculation()){
                         MainActivity.dataCheckerFail = true;
                         errorMsg.append("Feature Extraction error #" +sensorDataNum);
                     }else{
                         if (!featureExtraction.featureEntry()) {
                             MainActivity.dataCheckerFail = true;
                             errorMsg.append("Feature Ex entry error #" +sensorDataNum);
                         }else{
                             MainActivity.dataCheckerFail = false;

                             long anaEnd = System.currentTimeMillis();
                             long diff = anaEnd-anaStart;
                             Log.d(TAG, "counter ana: " +sensorDataNum+ "is "+ (int)diff);
                         //perform SMV predict

                             long smvStart = System.currentTimeMillis();
                             if (svModel == null) {
                                 try {
                                     svModel = SVModel.getInstance();
                                     Log.d(TAG,"svm is null");
                                 } catch (IOException e) {
                                     e.printStackTrace();
                                 }
                             }
                             try{
/*                                 double [] svmFeatures = new double[7];
                                 svmFeatures[0] = all_ct[sensorDataNum][7]/firstFeatures[7];
                                 svmFeatures[1] = all_f_m[sensorDataNum][3]/firstFeatures[11];
                                 svmFeatures[2] = all_f_m[sensorDataNum][5]/firstFeatures[13];
                                 svmFeatures[3] = all_f_m[sensorDataNum][7]/firstFeatures[15];
                                 svmFeatures[4] = all_imu[sensorDataNum][1]/firstFeatures[33];
                                 svmFeatures[5] = all_imu[sensorDataNum][2]/firstFeatures[34];
                                 svmFeatures[6] = all_imu[sensorDataNum][12]/firstFeatures[44];*/
                                 double [] svmFeatures = new double[32];
                                 // 0-7: ct, 8-15: fm, 16-23: fmad, 24-31: fti
                                 for (int i = 0; i < 8; i++){
                                     svmFeatures[i] = all_ct[sensorDataNum][i]/firstFeatures[i];
                                 }
                                 for (int i = 0; i < 8; i++){
                                     svmFeatures[i+8] = all_f_m[sensorDataNum][i]/firstFeatures[i+8];
                                 }
                                 for (int i = 0; i < 8; i++){
                                     svmFeatures[i+16] = all_f_mad[sensorDataNum][i]/firstFeatures[i+16];
                                 }
                                 for (int i = 0; i < 8; i++){
                                     svmFeatures[i+24] = all_fti[sensorDataNum][i]/firstFeatures[i+24];
                                 }
                                 svm_yPre[sensorDataNum] = svModel.svmPredict(svmFeatures);
                                 Log.d(TAG, "svm predict: " +svm_yPre[sensorDataNum]);

                                 long smvEnd = System.currentTimeMillis();
                                 Log.d(TAG, "counter smv diff " + (int)(smvEnd-smvStart));
                                 updateYpreMsg();

                                 if (MainActivity.exportData.outputStreamSMV == null) {
                                     MainActivity.exportData.startSMVLog();
                                 }
                                 MainActivity.exportData.logSMV(segmentStartT,segmentEndT,id_pre,sensorDataNum,sub_ct_previous,sub_f_m_previous,sub_f_mad_previous,sub_fti_previous, firstFeatures,all_imu[sensorDataNum], svm_yPre);
                                 //do another 2 svm test in next one min if current svm predict is true,
                                 //new update: clock is cancelled, only available to press manually
                                 if (MainActivity.analysisOn) {
                                     if (svm_yPre[sensorDataNum] == 2) {
                                         svmFatigueCount++;
                                     } else if (svm_yPre[sensorDataNum] == 1 && svmFatigueCount > 0) {
                                         MainActivity.analysisLoopStop1min();
                                         MainActivity.isNextIn5min = true;
                                         svmFatigueCount = 0;
                                     }
                                     if (svmFatigueCount == 1) {
                                         MainActivity.analysisLoopCount1min();
                                         MainActivity.isNextIn5min = false;
                                     } else if (svmFatigueCount == 3) {
                                         // motor activation and stop the 1min counter
                                         MainActivity.analysisLoopStop1min();
                                         MainActivity.isNextIn5min = true;
                                     }
                                 }else{
                                     if (svm_yPre[sensorDataNum] == 2) {
                                         svmFatigueCount++;
                                     }if (svmFatigueCount == 3){
                                         //send motor msg
                                     }
                                 }
                                 sensorDataNum++;
                                 Log.d(TAG, "sensorDataNum: " +sensorDataNum);
                            } catch (Exception e) {
                               e.printStackTrace();
                               errorMsg.append("svm Predict: " +e);
                            }
                         }
                     }
                   }
               }
            }
            MainActivity.analysisInProcess = false;
        }
    }

    //setter
    public static void setSegmentStartT(int time){segmentStartT[sensorDataNum] = time;}
    public static void setSegmentEndT(int time){segmentEndT[sensorDataNum] = time;}
    public static void setSensorFSR(double[][] fsr){sensorFSR = fsr;}
    public static void setSensorIMU(double[][] imu){sensorIMU = imu;}
    public static void setSub_ct_previous  (double [][] ct){sub_ct_previous = ct;}
    public static void setSub_f_m_previous  (double [][] fm){sub_f_m_previous = fm;}
    public static void setSub_f_mad_previous  (double [][] fmad){sub_f_mad_previous = fmad;}
    public static void setSub_fti_previous  (double [][] fti){sub_fti_previous = fti;}
    public static void setId_pre (int index){id_pre = index;}

    public static void setAll_ct  (double [] ct){
        all_ct[sensorDataNum] = ct;
        if (sensorDataNum == 0){
            for (int i = 0; i<8; i++)
                firstFeatures[i] = ct[i];
        }
    }
    public static void setAll_f_m  (double [] fm){
        all_f_m[sensorDataNum] = fm;
        if (sensorDataNum == 0){
            for (int i = 0; i<8; i++)
                firstFeatures[i+8] = fm[i];
        }
    }
    public static void setAll_f_mad  (double [] fmad){
        all_f_mad[sensorDataNum] = fmad;
        if (sensorDataNum == 0){
            for (int i = 0; i<8; i++)
                firstFeatures[i+16] = fmad[i];
        }
    }
    public static void setAll_fti  (double [] fti){
        all_fti[sensorDataNum] = fti;
        if (sensorDataNum == 0){
            for (int i = 0; i<8; i++)
                firstFeatures[i+24] = fti[i];
        }
    }

    public static void setSub_imu_ave  (double[] imuave){
        all_imu[sensorDataNum]= imuave;
        if (sensorDataNum == 0){
            for (int i = 0; i<21; i++)
                firstFeatures[i+32] = imuave[i];
        }
    }

    //getter
    public static int getSensorDataNum(){ return sensorDataNum; }
    public static double[] getFSRTotal(){ return sensorFSR[7]; }
    public static double[] getTimeDiff(){ return timeDiff; }
    public static double [] getSvm_yPre(){return svm_yPre;}
    public static double[] getLatestCT (){
        if (id_pre>2){
            return sub_ct_previous[2];
        }else
            return sub_ct_previous[id_pre-1];

    }
    public static double[] getFirstCT(){
        double[] tmp = new double[8];
        for (int i = 0; i< 8;i++){
            tmp[i] = firstFeatures[i];
        }
        return tmp;
    }
    public static double[] getResultantAcc(){
        double[] combinedAcc = new double[sensorData.datalength];
        for (int i=0; i<sensorData.datalength; i++ ){
            combinedAcc[i] = sensorIMU[0][i]*sensorIMU[0][i] + sensorIMU[1][i]*sensorIMU[1][i] + sensorIMU[2][i]*sensorIMU[2][i];
        }
        return combinedAcc;
    }

    public static double[] getFatigueTime(){
        double[] time = new double[sensorDataNum];
        for(int i = 0; i<sensorDataNum; i++){
            time[i] = (segmentStartT[i]-segmentStartT[0])/1000/60;
        }
        return time;
    }

    public static double[] getFatigueState(){
        double[] svm = new double[sensorDataNum];
        for (int i = 0; i< sensorDataNum; i++){
            svm[i] = svm_yPre[i];
        }
        return svm;
    }


    public void export(String note){
        double [][] log = new double[17][sensorData.datalength];
        log[0] = timeDiff;
        for (int i= 1; i < 8; i++){
            log[i] = sensorFSR [i];
        }
        for (int i = 0; i<9; i++){
            log[i+8] = sensorIMU[i];
        }

        MainActivity.exportData.singleEntry_analysis(log, true,note);
    }

    public void sortData(){
        Arrays.sort(sensorTmp, new SortbyTime());
    }

    public boolean dataChecker(){
        double[] timeDiffDelta = new double [sensorData.datalength-1];
        for (int i = 0; i< sensorData.datalength; i++) {
            timeDiff[i] = (sensorTmp[i][0]- sensorTmp[0][0]) /1000; //find time difference in s
            if (i > 0){
                timeDiffDelta[i-1] =  Math.abs(timeDiff[i] - timeDiff[i-1]);
                if (timeDiffDelta[i-1]>0.028) {//which means that there's no data recorded for more than 3 frames
                    return false;
                }
            }
        }
        return true;
    }

    public void dataColumnReverse(){
        Log.d(TAG," Reverse Start Time:" +  System.currentTimeMillis() + "\n" );
        for (int point= 0; point < sensorTmp.length; point++){
            for (int sensor = 0; sensor < 17; sensor ++){
                sensorTmpReverse[sensor][point] = sensorTmp [point][sensor];
            }
        }
        Log.d(TAG," Reverse End Time:" +  System.currentTimeMillis() + "\n" );
    }
// parsing data that is read from imported file
    public void parseAnalysisData(String line){
        ArrayList<Integer> index = new ArrayList<>();
        index.add(line.indexOf(","));
        if (index.get(0) != -1){
            id_pre = Integer.parseInt(line.substring(0,index.get(0)));
            Log.d(TAG, "start" + id_pre);
        }else{
            errorMsg.append("Parse Failure");
        }

        while (index.get(index.size()-1) != -1){
            index.add(line.indexOf(",", index.get(index.size()-1)+1));
        }
        sensorDataNum = Integer.parseInt(line.substring(index.get(0)+2, index.get(1)));

        for (int i = 0; i < dataPoint; i++) {
            segmentStartT[i] = Integer.parseInt(line.substring(index.get(1+i)+2, index.get(1+i+1)));
        }

        for (int i = 0; i < dataPoint; i++) {
            segmentEndT[i] = Integer.parseInt(line.substring(index.get(41+i)+2, index.get(41+i+1)));
        }

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 8; j++) {
                sub_ct_previous[i][j] = Double.parseDouble(line.substring(index.get(i*8+j+81)+2, index.get(i*8+j+81+1)));
            }
        }

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 8; j++) {
                sub_f_m_previous[i][j] = Double.parseDouble(line.substring(index.get(i*8+j+105)+2, index.get(i*8+j+105+1)));
            }
        }
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 8; j++) {
                sub_f_mad_previous[i][j] = Double.parseDouble(line.substring(index.get(i*8+j+129)+2, index.get(i*8+j+129+1)));
            }
        }

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 8; j++) {
                sub_fti_previous[i][j] = Double.parseDouble(line.substring(index.get(i*8+j+153)+2, index.get(i*8+j+153+1)));
            }
        }
        for (int i = 0; i < 53; i++) {
                firstFeatures[i] = Double.parseDouble(line.substring(index.get(177+i)+2, index.get(177+i+1)));
        }

        for (int i = 0; i < 21; i++) {
            all_imu[sensorDataNum][i] = Double.parseDouble(line.substring(index.get(230+i)+2, index.get(230+i+1)));
        }

        for (int i = 0; i < dataPoint; i++) {
            svm_yPre[i] = Double.parseDouble(line.substring(index.get(251+i)+2, index.get(251+i+1)));
        }
        sensorDataNum++;
  /*      Log.d(TAG, "line received: " + line);
        line = line.trim();
        int i = line.indexOf(",");
        if (i != -1){
            Log.d(TAG, "found , ");
            i = line.indexOf(",");
        }*/
/*        if (line != null) {
            Pattern dataA = Pattern.compile("-?\\d+(\\.\\d+)?");
            Matcher matcherA = dataA.matcher(line);
            if(matcherA.find()) {
                int startSeg = Integer.parseInt(matcherA.group());
            }else{
                errorMsg = "Parse Failure";
            }
            if(matcherA.find()) {
                int endSeg = Integer.parseInt(matcherA.group());
            }else{
                errorMsg = "Parse Failure";
            }
            if(matcherA.find()) {
                id_pre = Integer.parseInt(matcherA.group());
            }else{
                errorMsg = "Parse Failure";
            }
            if(matcherA.find()) {
                sensorDataNum = Integer.parseInt(matcherA.group());
            }else{
                errorMsg = "Parse Failure";
            }*/

/*            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < 8; j++) {
                    if(matcherA.find()) {
                        sub_ct_previous[i][j] = Double.parseDouble(matcherA.group());
                    }else{
                        errorMsg = "Parse Failure";
                    }
                }
            }
            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < 8; j++) {
                    if(matcherA.find()) {
                        sub_f_m_previous[i][j] = Double.parseDouble(matcherA.group());
                    }else{
                        errorMsg = "Parse Failure";
                    }
                }
            }
            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < 8; j++) {
                    if(matcherA.find()) {
                        sub_f_mad_previous[i][j] = Double.parseDouble(matcherA.group());
                    }else{
                        errorMsg = "Parse Failure";
                    }
                }
            }
            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < 8; j++) {
                    if(matcherA.find()) {
                        sub_fti_previous[i][j] = Double.parseDouble(matcherA.group());
                    }else{
                        errorMsg = "Parse Failure";
                    }
                }
            }
            for (int i = 0; i < 32; i++) {
                if(matcherA.find()) {
                    firstFeatures[i] = Double.parseDouble(matcherA.group());
                }else{
                    errorMsg = "Parse Failure";
                }
            }
            for (int i = 0; i < 21; i++) {
                if(matcherA.find()) {
                    all_imu[sensorDataNum][i] = Double.parseDouble(matcherA.group());
                }else{
                    errorMsg = "Parse Failure";
                }
            }
            for (int i = 0; i < dataPoint; i++) {
                if(matcherA.find()) {
                    svm_yPre[i] = Double.parseDouble(matcherA.group());
                }else{
                    errorMsg = "Parse Failure";
                }
            }
        }*/
    }

    public void resetAnalysisVar(){
        sensorDataNum = 0;
        sensorRaw = new double[dataPoint][sensorData.datalength][17];
        segmentStartT = new int [dataPoint];
        segmentEndT = new int [dataPoint];
        sensorTmp = new double[sensorData.datalength][17];
        sensorTmpReverse = new double[17][sensorData.datalength];
        timeDiff = new double[sensorData.datalength];
        sensorFSR = new double[8][sensorData.datalength];
        sensorIMU = new double[9][sensorData.datalength];

        sub_ct_previous = new double [3][8];
        sub_f_m_previous= new double [3][8];
        sub_f_mad_previous = new double [3][8];
        sub_fti_previous = new double [3][8];
        all_ct = new double [dataPoint][8];
        all_f_m= new double [dataPoint][8];
        all_f_mad = new double [dataPoint][8];
        all_fti = new double [dataPoint][8];
        all_imu = new double[dataPoint][21];
        firstFeatures = new double [53];
        id_pre = 0;

        svm_yPre  = new double[dataPoint];

         errorMsg = new StringBuilder();
    }

    public static void updateYpreMsg(){
        yPreMsg = new StringBuilder();
        if (sensorDataNum > 0) {
            for (int i = 0; i < sensorDataNum; i++) {
                yPreMsg.append(segmentStartT[i] + "  " + svm_yPre[i] + "\n");
            }
        }
    }
}

class  SortbyTime implements Comparator<double[]> {
    public int compare (double[] one, double[] two){
        return (int) (one[0] - two[0]);
    }
}
