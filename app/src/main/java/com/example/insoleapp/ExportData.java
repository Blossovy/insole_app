package com.example.insoleapp;

import android.content.Context;
import android.os.Handler;
import android.util.Log;


import com.github.mikephil.charting.data.Entry;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ExportData {

    private Context context;
    final private String TAG = "Export Data";
    private SensorData sensorData;
    private AnalysedData analysedData;
    private PersonalInfo personalInfo;
    private ArrayList<Entry>[] data;
    FileOutputStream outputStreamL = null;
    FileOutputStream outputStreamR = null;
    public static FileOutputStream outputStream_analysis = null;
    FileOutputStream outputStream_predict = null;
    public static FileOutputStream outputStreamSMV = null;
    Handler handler = new Handler();
    public boolean exportSortedFlag = false;
    public boolean exportAnalysedFlag;
    ExecutorService exAnalysisService = Executors.newSingleThreadExecutor();


    public ExportData(Context c) {
        context = c;
        personalInfo = PersonalInfo.getInstance();
        sensorData = SensorData.getInstance();
        analysedData = AnalysedData.getInstance();
    }

    public void startSMVLog() {

            File directory = new File(context.getExternalFilesDir(null) + "/Insole");

            Log.d(TAG, "file directory: " + directory);
            boolean var = false;
            if (!directory.exists()) {
                var = directory.mkdirs();
            }

            Log.d(TAG, "directory exists - " + var);

            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
            String timeStamp = simpleDateFormat.format(new Date());

            File file = new File(directory.toString() + "/" + timeStamp + "SMVLog.txt");

            try {
                outputStreamSMV = new FileOutputStream(file, true);
                outputStreamSMV.write("id_pre, sensorDataNum, SegStartT, SegEndT, sub_ct_previous, sub_f_m_previous, sub_f_mad_previous, sub_fti_previous, fsrFirstFeatures0-31, imu, yPre  \n".getBytes());

            } catch (IOException e) {
                e.printStackTrace();
            }

    }
    public void logSMV(int[] segStart, int[] segEnd, int id_pre, int sensorNum, double[][] ct_pre, double[][]fm_pre, double [][] fmad_pre, double[][] fti_pre, double [] firstSet, double[] imu, double[] y_pre) {
        final int[] segStar = segStart;
        final int[] segEn = segEnd;
        final int id = id_pre;
        final int sensorNo = sensorNum;
        final double[][] ct_p = ct_pre;
        final double[][] fm_p = fm_pre;
        final double[][] fmad_p = fmad_pre;
        final double[][] fti_p = fti_pre;
        final double[] first = firstSet;
        final double[] imuData = imu;
        final double[] ypre = y_pre;

        Thread logThreadSMV = new Thread() {
            public void run() {
                try {

                    outputStreamSMV.write((id + ", ").getBytes());
                    outputStreamSMV.write((sensorNo + ", ").getBytes());
                    for (int i = 0; i < AnalysedData.dataPoint; i++){
                        outputStreamSMV.write((segStar[i] + ", ").getBytes());
                    }
                    for (int i = 0; i < AnalysedData.dataPoint; i++){
                        outputStreamSMV.write((segEn[i] + ", ").getBytes());
                    }
                    for (int i = 0; i < 3; i++){
                        for (int j = 0; j <8; j++){
                            outputStreamSMV.write((ct_p[i][j] + ", ").getBytes());
                        }
                    }
                    for (int i = 0; i < 3; i++){
                        for (int j = 0; j <8; j++){
                            outputStreamSMV.write((fm_p[i][j] + ", ").getBytes());
                        }
                    }
                    for (int i = 0; i < 3; i++){
                        for (int j = 0; j <8; j++){
                            outputStreamSMV.write((fmad_p[i][j] + ", ").getBytes());
                        }
                    }
                    for (int i = 0; i < 3; i++){
                        for (int j = 0; j <8; j++){
                            outputStreamSMV.write((fti_p[i][j] + ", ").getBytes());
                        }
                    }
                    for (int i = 0; i < 53; i++){
                        outputStreamSMV.write((first[i] + ", ").getBytes());
                    }
                    for (int i = 0; i <21; i++){
                        outputStreamSMV.write((imuData[i] + ", ").getBytes());
                    }
                    for (int i = 0; i < AnalysedData.dataPoint; i++){
                        outputStreamSMV.write((ypre[i] + ", ").getBytes());
                    }
                    outputStreamSMV.write(("\n").getBytes());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        };
        logThreadSMV.start();
    }
    public void endSMVLog() throws IOException {
        if (outputStreamSMV != null)
            outputStreamSMV.close();
    }

    public void startloggingL() {
        Thread logThread = new Thread() {
            public void run() {
                File directory = new File(context.getExternalFilesDir(null) + "/Insole");

                Log.d(TAG, "file directory: " + directory);
                boolean var = false;
                if (!directory.exists()) {
                    var = directory.mkdirs();
                }

                Log.d(TAG, "directory exists - " + var);

                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
                String timeStamp = simpleDateFormat.format(new Date());

                File file = new File(directory.toString() + "/" + timeStamp + "Lflow.txt");

                try {
                    outputStreamL = new FileOutputStream(file, true);
                    outputStreamL.write(("Subject:, " + personalInfo.getName() + ",\n").getBytes());
                    outputStreamL.write(("Age:, " + personalInfo.getAge() + ",\n").getBytes());
                   outputStreamL.write(("Weight:, " + personalInfo.getWeight() + ",\n").getBytes());
                    outputStreamL.write(("@@ \n").getBytes());
                    outputStreamL.write("Time, A, B, C, D, E, F, G, Ax1, Ay1, Az1, Gx1, Gy1, Gz1, ∠x1, ∠y1, ∠z1 \n".getBytes());

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };
        logThread.start();
    }

    public void logEntryL(int[] fsr, float[] imu) {
        final int[] logFsr = fsr;
        final float[] logImu = imu;
        Thread logThread = new Thread() {
            public void run() {
                try {
                    for (int i = 0; i < 8; i++) {
                        outputStreamL.write((logFsr[i] + ", ").getBytes());
                    }
                    if (logImu != null) {
                        for (int i = 1; i < 10; i++) {
                            outputStreamL.write((logImu[i] + ", ").getBytes());
                        }
                    }

                    outputStreamL.write("\n ".getBytes());
                    Log.d(TAG, "logged: " + logFsr[0]);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        };
        logThread.start();
    }

    public void logEntryNPL(boolean isSensorData, String data) {
        final String logData = data;
        final boolean logIsSensorData = isSensorData;
        Thread logThreadL = new Thread() {
            public void run() {
                try {
                    if (logIsSensorData) {
                        String exData = logData;
                        exData = exData.replaceAll(" B= \\d*;", "");
                        exData = exData.replaceAll("T= ", "");
                        exData = exData.replaceAll("F= ", "");
                        exData = exData.replaceAll("II= ", "");
                        exData = exData.replaceAll("I= ", "");
                        exData = exData.replaceAll(";", ",");
                        exData = exData.replaceAll("#", "\n");
                        outputStreamL.write((exData).getBytes());
                        //outputStreamL.write("\n ".getBytes());
                        Log.d(TAG, "logged: " + exData);
                    } else {
                        outputStreamL.write(logData.getBytes());
                        Log.d(TAG, "logged: " + logData);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        };
        logThreadL.start();
    }

    public void logEntryNPR(final boolean isSensorData, String data) {
        final String logData = data;
        final boolean logIsSensorData = isSensorData;
        Thread logThreadR = new Thread() {
            public void run() {
                try {
                    if (isSensorData) {
                        String exData = logData;
                        exData = exData.replaceAll(".*B=\\d*", "");
                        exData = exData.replaceAll("T=", "");
                        exData = exData.replaceAll("F=", "");
                        exData = exData.replaceAll("II=", "");
                        exData = exData.replaceAll("I=", "");
                        exData = exData.replaceAll(";", ",");
                        exData = exData.replaceAll("#", "\n");
                        outputStreamR.write((exData).getBytes());
                        //outputStreamR.write("\n ".getBytes());
                        Log.d(TAG, "logged: " + exData);
                    } else {
                        outputStreamL.write(logData.getBytes());
                        Log.d(TAG, "logged: " + logData);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        };
        logThreadR.start();
    }

    public void endLoggingL() throws IOException {
        if (outputStreamL != null)
            outputStreamL.close();
    }

    public void startloggingR() {
        Thread logThread = new Thread() {
            public void run() {
                File directory = new File(context.getExternalFilesDir(null) + "/Insole");

                Log.d(TAG, "file directory: " + directory);
                boolean var = false;
                if (!directory.exists()) {
                    var = directory.mkdirs();
                }

                Log.d(TAG, "directory exists - " + var);

                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
                String timeStamp = simpleDateFormat.format(new Date());

                File file = new File(directory.toString() + "/" + timeStamp + "Rflow.txt");

                try {
                    outputStreamR = new FileOutputStream(file, true);
                    outputStreamR.write(("Subject:, " + personalInfo.getName() + ",\n").getBytes());
                    outputStreamR.write(("Age:, " + personalInfo.getAge() + ",\n").getBytes());
                    outputStreamR.write(("Weight:, " + personalInfo.getWeight() + ",\n").getBytes());
                    outputStreamR.write(("@@ \n").getBytes());
                    outputStreamR.write("Time, A, B, C, D, E, F, G, Ax1, Ay1, Az1, Gx1, Gy1, Gz1, ∠x1, ∠y1, ∠z1 \n".getBytes());

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };
        logThread.start();

    }

//    public void logEntryR(long time, float[] sensor) {
//        final long logTime = time;
//        final float[] logSensor = sensor;
//        Thread logThread = new Thread() {
//            public void run() {
//                try {
//                    outputStreamR.write((logTime + ", ").getBytes());
//                    for (int j = 0; j < 25; j++) {
//                        outputStreamR.write((logSensor[j] + ", ").getBytes());
//                    }
//                    outputStreamR.write("\n ".getBytes());
//                    Log.d(TAG, "logged: " + logTime);
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//        };logThread.start();
//    }

    public void endLoggingR() throws IOException {
        if (outputStreamR != null)
            outputStreamR.close();
    }

    public void startLogging_analysis() {
        Thread logThread = new Thread() {
            public void run() {
                File directory = new File(context.getExternalFilesDir(null) + "/Insole");

                Log.d(TAG, "file directory: " + directory);
                boolean var = false;
                if (!directory.exists()) {
                    var = directory.mkdirs();
                }

                Log.d(TAG, "directory exists - " + var);

                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
                String timeStamp = simpleDateFormat.format(new Date());

                File file = new File(directory.toString() + "/" + timeStamp + "Analysis.txt");

                try {
                    outputStream_analysis = new FileOutputStream(file, true);
                    outputStream_analysis.write(("Subject:, " + personalInfo.getName() + ",\n").getBytes());
         //           outputStream_analysis.write(("Start Time:, " + analysedData.getSessionStartT() + ",\n").getBytes());
                    outputStream_analysis.write(("@@ \n").getBytes());
                    outputStream_analysis.write("Time, A, B, C, D, E, F, G, Ax1, Ay1, Az1, Gx1, Gy1, Gz1, ∠x1, ∠y1, ∠z1 \n".getBytes());

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };
        logThread.start();
    }
    public void smvPredictLog(double[] l, double[] p) {
        final double[] predict = p;
        final double[] label = l;
        Thread logThread = new Thread() {
            public void run() {
                File directory = new File(context.getExternalFilesDir(null) + "/Insole");
                Log.d(TAG, "file directory: " + directory);
                boolean var = false;
                if (!directory.exists()) {
                    var = directory.mkdirs();
                }
                Log.d(TAG, "directory exists - " + var);
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
                String timeStamp = simpleDateFormat.format(new Date());

                File file = new File(directory.toString() + "/" + timeStamp + "Predict.txt");

                try {
                    outputStream_predict = new FileOutputStream(file, true);
                    if (predict.length != label.length){
                        outputStream_predict.write(("wrong size, p: " + predict.length + "l: "+ label.length +",\n").getBytes());
                    }else{
                        for (int i = 0; i < predict.length; i++){
                            outputStream_predict.write((label[i] + " " + predict[i] + ",\n").getBytes());
                        }
                    }
                 outputStream_predict.close();
                    Log.d(TAG, "predict log is done");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };
        logThread.start();
    }
    public void singleEntry_analysis( double[][] data, boolean reversedColumn, String note) {

//        Log.d(TAG, "log3rdfiltered analysis data" + data[0][0]);
 //       Log.d(TAG, "log3rdfiltered analysis data" + data[1][0]);

        Log.d(TAG, "arrive export analysis");
        final double[][] anaData = data;
        final boolean reversed = reversedColumn;
        final String remarks = note;

        Runnable exportRunnable = new Runnable() {
            @Override
            public void run() {
                try {
                     Log.d(TAG, "anaData null" + (anaData == null? "True" : "False"));
                    if (anaData != null) {
                        outputStream_analysis.write(remarks.getBytes() );
                        outputStream_analysis.write(("\n").getBytes());
                        Log.d(TAG, "log3rdfiltered analysis" + anaData[0][0]);
                        Log.d(TAG," export start: " + remarks+ " " + System.currentTimeMillis() );
                     //   Log.d(TAG, "log3rdfiltered analysis" + anaData[1][0]);


                        for (int i = 0; i < sensorData.datalength; i++) {

                                for (int j = 0; j <18; j++) {
                                    if (!reversed) {
                                        outputStream_analysis.write((anaData[i][j] + ", ").getBytes());
                                    }else {
                                        outputStream_analysis.write((anaData[j][i] + ", ").getBytes());

                                    }
                                }

                            outputStream_analysis.write(("\n").getBytes());
                        }
                        outputStream_analysis.write(("\n").getBytes());

                        Log.d(TAG, "analysis data logged");
                        Log.d(TAG, "from single entry analysis");
/*                        if (exportSortedFlag) {
                            analysedData.sortData();
                            MainActivity.exportData.singleEntry_analysis(AnalysedData.sensorTmpReverse, true);
                            exportSortedFlag = false;
                        }*/

                    }

                    Log.d(TAG," export ends: " + remarks+ " " + System.currentTimeMillis() );
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };
        exAnalysisService.submit(exportRunnable);
    }
    /*    Thread logAnalysis = new Thread() {
            public void run() {
                try {
                    if (anaData != null) {

                        Log.d(TAG, "log3rdfiltered analysis"+ anaData[0][0]);
                        Log.d(TAG, "log3rdfiltered analysis"+ anaData[1][0]);

                            for (int i = 0; i < sensorData.datalength; i++) {
                                for (int j = 0; j < 17; j++) {
                                    if (!reversed) {
                                        outputStream_analysis.write((anaData[i][j] + ", ").getBytes());
                                    }else {
                                        outputStream_analysis.write((anaData[j][i] + ", ").getBytes());
                                    }
                                }
                                outputStream_analysis.write(("\n").getBytes());
                            }
                            outputStream_analysis.write(("\n").getBytes());

                            Log.d(TAG, "analysis data logged");
                            Log.d(TAG, "from single entry analysis");
                            if (exportSortedFlag) {
                                analysedData.sortData();
                                MainActivity.exportData.singleEntry_analysis(AnalysedData.sensorTmpReverse, true);
                                exportSortedFlag = false;
                            }

                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };

        logAnalysis.start();
    }*/

    public void endLogging_analysis() throws IOException {
        if (outputStream_analysis != null)
            outputStream_analysis.close();
    }
}

