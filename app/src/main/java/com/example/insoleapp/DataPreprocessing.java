package com.example.insoleapp;
import android.util.Log;

import org.apache.commons.math3.analysis.interpolation.LinearInterpolator;
import org.apache.commons.math3.analysis.polynomials.PolynomialSplineFunction;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import uk.me.berndporr.iirj.*;
public class DataPreprocessing {

    private final static String TAG = "DataPreprocessing";

    private static SensorData sensorData = SensorData.getInstance();
    private ToolMath toolMath = new ToolMath();

    Butterworth butterworth = new Butterworth();
    LinearInterpolator linearInterpolator = new LinearInterpolator();
    //index 0-6 = FSR (Toe, M1, M5, Mid, March, Larch, Heel),7 = total force
    public static double [][] sensorFSR_sub = new double[8][sensorData.datalength];
    //index IMU (0-2: acc_x,y,z; 3-5: gyo_x,y,z; 6-8:angle_x,y,z)
    public static double [][] sensorIMU_sub = new double[9][sensorData.datalength];
    private int sensorsLen;
    private double [] time;
    double max;
    private AnalysedData analysedData = AnalysedData.getInstance();


    public DataPreprocessing(double[][] data, double[] timeDiff){
        //index 0 = time from circuit box, 1-7 = FSR (Toe, M1, M5, Mid, March, Larch, Heel), 8-16 = IMU (acc_x,y,z; gyo_x,y,z; angle_x,y,z)

        final double [][] d = data;
        final double [] tdiff = timeDiff;
        for (int i = 0; i < 7; i++) {
            sensorFSR_sub[i] = d[i+1];
        }
        for (int i = 0; i < 9; i++){
            sensorIMU_sub[i] = d[i+8];
        }

        time = tdiff;
        sensorsLen = d[0].length;
        Log.d(TAG," sensor Length" + sensorsLen);
    }

    public void imuConversion(){
        try{
        Log.d(TAG," Conversion start Time:" +  System.currentTimeMillis() );
        //index 0 = time from circuit box, 1-7 = FSR (Toe, M1, M5, Mid, March, Larch, Heel), 8-16 = IMU (acc_x,y,z; gyo_x,y,z; angle_x,y,z), 17 = time from 0 i.e. sensorTmp[i] - sensorTmp[1], 18 = FSR total
      //  double [] modeOfAngle = {0,0,0};

     /*   for (int i = 0; i < 3; i++){
            modeOfAngle[i] = toolMath.mode(sensorIMU_sub[i+6]);
            Log.d(TAG, "mode of Angle " + i + " = " + modeOfAngle[i]);
        }*/

        for (int i = 0; i< sensorsLen; i++){
            // acceleration/gravity
            sensorIMU_sub[0][i] =  (sensorIMU_sub[0][i]/9.8);
            sensorIMU_sub[1][i] =  -(sensorIMU_sub[1][i]/9.8);
            sensorIMU_sub[2][i] =  (sensorIMU_sub[2][i]/9.8);

            // angle  // delete this angle part since angle processed in "imuFeaturesthetaReset" (Line 297~335)
/*            for (int j = 6; j < 9; j++) {
               // 1. offset removal with "mode"
                sensorIMU_sub[j][i] -= modeOfAngle [j-6];

                // condition with > -180 or < 180
                if (sensorIMU_sub[j][i] > 180){
                    sensorIMU_sub[j][i] = sensorIMU_sub[j][i]-360;
                }else if (sensorIMU_sub[j][i] < -180){
                    sensorIMU_sub[j][i] = sensorIMU_sub[j][i]+360;
                }
            }*/

            //set min and ceilling
            for (int j = 0; j < 3; j++) { //acc
                sensorIMU_sub[j][i] = Math.max(-10, sensorIMU_sub[j][i]);
                sensorIMU_sub[j][i] = Math.min(10, sensorIMU_sub[j][i]);
            }
            for (int j = 3; j < 6; j++) { //gyro
                sensorIMU_sub[j][i] = Math.max(-15, sensorIMU_sub[j][i]);
                sensorIMU_sub[j][i] = Math.min(15, sensorIMU_sub[j][i]);
            }

        }
        }catch (Exception e){
            Log.d(TAG, "error at imu conversion");
            AnalysedData.errorMsg.append( "IMU conversion: " + e +"\n");
            e.printStackTrace();
        }
       // analysedData.setSensorIMU(sensorIMU_sub);
/*        AnalysedData.logImuConversion[0] = time;
        for (int p = 0; p < 7; p++)
            AnalysedData.logImuConversion[p+1] = sensorFSR_sub[p];
        for (int p = 0; p < 9; p++)
            AnalysedData.logImuConversion[p+8] = sensorIMU_sub[p];
        Log.d(TAG, "export data, imu conversion");
        MainActivity.exportData.singleEntry_analysis(AnalysedData.logImuConversion, true, "imu conversion done");*/
    }

    public void filter_continuousSignalError() {
        //index 0 = time from circuit box, 1-7 = FSR (Toe, M1, M5, Mid, March, Larch, Heel), 8-16 = IMU (acc_x,y,z; gyo_x,y,z; angle_x,y,z), 17 = time from 0 i.e. sensorTmp[i] - sensorTmp[1], 18 = FSR total

        // For continuous signal error
        int [] force_thre = {120, 120, 120, 90, 14, 14, 120}; //N
        int [] force_thre_2 = {90, 75, 80, 30, 10, 10, 100}; //N
        int [] force_thre_3 = {70, 70, 50, 25, 10, 10, 90}; //N
        double delta_f = 2.5;

        //1st filter: condition for FSR extreme values, single error correction
        try{
        double [] force_thre_max = {170, 170, 170, 170, 24, 24, 170};
        for (int i = 0; i < 7; i++){
            for (int j = 1; j< sensorsLen; j++){
                if ( sensorFSR_sub[i][j] > force_thre_max[i]){
                    sensorFSR_sub[i][j]= Math.max(sensorFSR_sub[i][j-1], force_thre_max[i]+0.01);
                }
            }
        }
        Log.d(TAG, "1st filter done");
        // analysedData.setSensorFSR(sensorFSR_sub);
/*        AnalysedData.log1stfiltered[0] = time;
        for (int i= 0; i < 8; i++){
            AnalysedData.log1stfiltered[i+1] = sensorFSR_sub[i];
        }
        for (int i = 0; i<9; i++){
            AnalysedData.log1stfiltered[i+8] = sensorIMU_sub[i];
        }
        Log.d(TAG, "export data, 1st filter");
        MainActivity.exportData.singleEntry_analysis(AnalysedData.log1stfiltered, true,"1st filtered");*/
        }catch (Exception e){
            Log.d(TAG, "filter_continuousSignalError error at 1st filter");
            AnalysedData.errorMsg.append("1st filter: " + e +"\n");
            e.printStackTrace();
        }

        // 2nd filter: condition for FSR extreme values, 3-point peak correction
        for (int i = 0; i < 7; i++){
            try {
                int flag_start = 0;
                for (int j = 0; j < sensorsLen ; j++) {
                    if ((flag_start < 1) && (sensorFSR_sub[i][j] > force_thre[i])) {
                        continue;
                    }
                    flag_start = 1;
                    if (sensorFSR_sub[i][j] > force_thre[i]) {
                        int id_last = j;
                        int n_count = 0;
                        double[] tmp = Arrays.copyOfRange(sensorFSR_sub[i], j, id_last+1);
                        while ((sensorFSR_sub[i][id_last] > force_thre[i]) && maxDeviation(Arrays.copyOfRange(sensorFSR_sub[i], j, id_last+1)) && id_last < sensorsLen-1) {
                            id_last += 1;
                            n_count += 1;
                        }
                        if (n_count >= 3) {
                            int id_end =-1, id_last2=-1, id_star=-1, id_star2=-1;

                            double [] arrayAfter3Errors = Arrays.copyOfRange(sensorFSR_sub[i], id_last, Math.min(id_last +111, sensorsLen));
                            for (int k = 0; k < arrayAfter3Errors.length; k++){
                                if (arrayAfter3Errors[k] < force_thre_3[i]){
                                    id_end = k;
                                    break;
                                }
                            }

                            id_last2 = id_last + id_end;
                            double [] arrayBefore3Errors = Arrays.copyOfRange(sensorFSR_sub[i], Math.max(id_last-111,0), id_last-1);
                            for (int k = arrayBefore3Errors.length-1; k >= 0; k --){
                                if(arrayBefore3Errors[k] < force_thre_3[i]){
                                    id_star = k;
                                    break;
                                }
                            }
                            id_star2 = Math.max(id_last-111, 0) + (id_star);
                            // check id_end, id_last2, id_star, id_star2 have values assigned
                            if (id_end == -1 || id_last2 == -1 || id_star == -1 || id_star2 == -1) {
                                Log.d(TAG, "no data assigned for " + (id_end == -1? "id_end": "" ) + (id_last2 == -1? "id_last2": "" ) + (id_star == -1? "id_star": "" ) +(id_star2 == -1? "id_star2": "" )) ;
                                break;
                            }

                            double [] x_base, y_base;
                            if (id_star2 >2)
                                x_base = new double []{id_star2-1, id_star2, Math.min(id_last2,sensorsLen-1)};
                            else
                                x_base = new double []{id_star2, id_last2, id_last2+1};
                            y_base = new double[] {sensorFSR_sub[i][(int)x_base[0]], sensorFSR_sub[i][(int)x_base[1]], sensorFSR_sub[i][(int)x_base[2]]};

                            PolynomialSplineFunction fittedFunction = linearInterpolator.interpolate(x_base, y_base);
                            for (int m = id_star2+1; m < id_last2; m++){
                                sensorFSR_sub[i][m] = fittedFunction.value(m);
                            }
                            //continue;
                        }
                    }
                }
            }catch (Exception e){
                Log.d(TAG, "filter_continuousSignalError error at 2nd filter");
                AnalysedData.errorMsg.append("2nd filter: " + e +"\n");
                e.printStackTrace();
            }

        }
        Log.d(TAG, "2nd filter done");
        // analysedData.setSensorFSR(sensorFSR_sub);
/*        AnalysedData.log2ndfiltered[0] = time;
        for (int i= 0; i < 8; i++){
            AnalysedData.log2ndfiltered[i+1] = sensorFSR_sub[i];
        }
        for (int i = 0; i<9; i++){
            AnalysedData.log2ndfiltered[i+8] = sensorIMU_sub[i];
        }
        Log.d(TAG, "export data, 2nd filter");
        MainActivity.exportData.singleEntry_analysis(AnalysedData.log2ndfiltered, true,"2nd filtered");*/
        //3rd filter: condition for FSR extreme values, 2-point peak correction

        try{
            for (int i = 0; i < 7; i++){
                int flag_start = 0;
                for (int j = 1 ; j < sensorsLen; j++) {
                    if (flag_start < 1 && sensorFSR_sub[i][j] > force_thre[i]) // ensure two points are normal before filtering
                        continue;
                    flag_start = 1;

                    int id_last = j;
                    int n_count = 0;
                    while(sensorFSR_sub[i][id_last] > force_thre_3[i] && id_last < sensorsLen-1){
                        id_last += 1;
                        n_count += 1;
                    }
                    // (1) error exists in only one data point
                    if (n_count == 1){
                        if ((sensorFSR_sub[i][j] - sensorFSR_sub[i][j-1] >delta_f) && ((sensorFSR_sub[i][j] - sensorFSR_sub[i][Math.min(j+1, sensorsLen-1)])>delta_f)){ //data_f(ii,jj)-data_f( min(ii+1,m),jj )>delta_f
                            sensorFSR_sub[i][j] = (sensorFSR_sub[i][j-1] + sensorFSR_sub[i][j+1])/2;
                            continue;
                        }

                    }
                    // (2) error exists in >= 2 data points
                    if (n_count >1){
                        if ((Math.abs(sensorFSR_sub[i][j]- sensorFSR_sub[i][j+1])<1 ) && ((sensorFSR_sub[i][j] - sensorFSR_sub[i][j-1]) >delta_f) && ((sensorFSR_sub[i][j] - sensorFSR_sub[i][Math.min(j+2, sensorsLen-1)]) > delta_f)){
                            double [] x_base = new double[]{j-2, j-1, id_last};
                            double [] y_base = new double[]{sensorFSR_sub[i][(int)x_base[0]], sensorFSR_sub[i][(int)x_base[1]], sensorFSR_sub[i][(int)x_base[2]]};
                            PolynomialSplineFunction fittedFunction = linearInterpolator.interpolate(x_base, y_base);
                            for (int n = j; n < id_last; n ++){
                                sensorFSR_sub[i][n] = fittedFunction.value(n);
                            }
                            //continue;

                        }
                    }

                }
            }
        Log.d(TAG, "3rd filter done");
        //analysedData.setSensorFSR(sensorFSR_sub);
/*        double[][] log1 = new double[17][sensorData.datalength];
        log1[0] = time;
        for (int i= 0; i < 8; i++){
            log1[i+1] = sensorFSR_sub[i];
        }
        for (int i = 0; i<9; i++){
            log1[i+8] = sensorIMU_sub[i];
        }
        Log.d(TAG, "export data, 3rd filter");
        final double [][] log_final = log1;
        MainActivity.exportData.singleEntry_analysis(log_final, true,"3rd filtered");*/
        }catch (Exception e){
            Log.d(TAG, "filter_continuousSignalError error at 3rd filter");
            AnalysedData.errorMsg.append( "3rd filter: " + e +"\n");
            e.printStackTrace();
       }

        // 4th filter
        try{
            for (int i = 0; i < 7; i++){
                int flag_start = 0;
                for (int j = 1; j < sensorsLen-1; j++){
                    if (flag_start <1 && sensorFSR_sub[i][j] >force_thre[i]){ // ensure two points are normal before filtering
                        continue;
                    }
                    flag_start = 1;
                    if ((sensorFSR_sub[i][j] - sensorFSR_sub[i][j-1]) > delta_f  && (sensorFSR_sub[i][j] - sensorFSR_sub[i][Math.min(j+1, sensorsLen-1)]) >delta_f){
                        sensorFSR_sub[i][j] = (sensorFSR_sub[i][j-1] + sensorFSR_sub[i][j+1])/2;
                    }
                }
            }
            Log.d(TAG, "4th filter done");
           // analysedData.setSensorFSR(sensorFSR_sub);
/*            AnalysedData.log4thfiltered[0] = time;
            for (int i= 0; i < 8; i++){
                AnalysedData.log4thfiltered[i+1] = sensorFSR_sub[i];
            }
            for (int i = 0; i<9; i++){
                AnalysedData.log4thfiltered[i+8] = sensorIMU_sub[i];
            }
            Log.d(TAG, "export data, 4th filter");
            MainActivity.exportData.singleEntry_analysis(AnalysedData.log4thfiltered, true,"4th filtered");*/
            //analysedData.setSensorFSR(sensorFSR_sub);
        }catch (Exception e){
            Log.d(TAG, "filter_continuousSignalError error at 4th filter");
            AnalysedData.errorMsg.append( "4th filter: " + e +"\n");
            e.printStackTrace();
        }

    }

    public void filter_butter(){
        try{
        // All frequency values are in Hz.
        //Fs = 110;  % Sampling Frequency

        int order = 2;
        int samplingFreq = sensorData.sensorFreq;
        int cutOffFreq = 6; //Hz

        butterworth.lowPass(order, samplingFreq, cutOffFreq);
        for (int i = 0; i < 7; i++){
            for (int j = 0; j < sensorFSR_sub[0].length; j++) {
                sensorFSR_sub[i][j] = butterworth.filter(sensorFSR_sub[i][j]);
            }
        }

        Log.d(TAG, "butter filter done");
        //analysedData.setSensorFSR(sensorFSR_sub);
        }catch (Exception e){
            Log.d(TAG, "filter_continuousSignalError error at butter filter");
            AnalysedData.errorMsg.append( "Butterworth filter: " + e +"\n");
            e.printStackTrace();
        }


    }

    public void totalForce(){
        for (int j = 0; j < sensorsLen; j++) {
            sensorFSR_sub[7][j] = 0;
            for (int i = 0; i < 7; i++)
                sensorFSR_sub[7][j] = sensorFSR_sub[7][j] + sensorFSR_sub[i][j];
        }

        analysedData.setSensorFSR(sensorFSR_sub);
        analysedData.setSensorIMU(sensorIMU_sub);
        double [][] log3 = new double[18][sensorData.datalength];
        log3[0] = time;
        for (int i= 0; i < 8; i++){
            log3[i+1] = sensorFSR_sub[i];
        }
        for (int i = 0; i<9; i++){
            log3[i+9] = sensorIMU_sub[i];
        }

        Log.d(TAG, "export data, butterworth");
        MainActivity.exportData.singleEntry_analysis(log3, true,"butter filtered");
    }

    private boolean maxDeviation(double [] data){
        double avg = toolMath.average(data);
        List<Double> deviation = new ArrayList<Double>();
        for (int i = 0; i < data.length; i++){
            deviation.add(data[i] - avg);
        }
        max = Collections.max(deviation);
        return (max < 1);
    }

}
