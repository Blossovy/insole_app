package com.example.insoleapp;

import android.bluetooth.BluetoothDevice;

public class ShoeInfo {


    private static ShoeInfo instance;
    private BluetoothDevice bluetoothDeviceL, bluetoothDeviceR;
    // sensor available: FSR, IMU1, IMU2
    private boolean [] sensorsAvailL = {false, false, false};
    private boolean [] sensorsAvailR = {false, false, false};

    //battery level: unknown = -1, low, med, full
    private int batterylvlL = -1, batterylvlR = -1;

    //insole stiffness mode
    private int insoleModeL = MainActivity.MODE_UNKNOWN;
    private int insoleModeR = MainActivity.MODE_UNKNOWN;

    private boolean isModeChangedL = false;
    private boolean isModeChangedR = false;

    private final static String TAG = "Shoe Info";

    private ShoeInfo(){}

    public static ShoeInfo getInstance(){
        if (instance == null){
            instance = new ShoeInfo();
        }
        return instance;
    }

    public BluetoothDevice getBluetoothDeviceL() { return bluetoothDeviceL; }
    public BluetoothDevice getBluetoothDeviceR() { return bluetoothDeviceR; }
    public boolean getSensorsAvailL(int index) { return sensorsAvailL[index]; }
    public boolean getSensorsAvailR(int index) { return sensorsAvailR[index]; }
    public int getBatterylvlL() { return batterylvlL; }
    public int getBatterylvlR() { return batterylvlR; }
    public int getInsoleModeL() { return insoleModeL; }
    public int getInsoleModeR() { return insoleModeR; }
    public boolean isModeChangedL() { return isModeChangedL; }
    public boolean isModeChangedR() { return isModeChangedR; }

    public void setBluetoothDeviceL(BluetoothDevice bluetoothDeviceL) { this.bluetoothDeviceL = bluetoothDeviceL; }
    public void setBluetoothDeviceR(BluetoothDevice bluetoothDeviceR) { this.bluetoothDeviceR = bluetoothDeviceR; }
    public void setSensorsAvailL(int index, boolean sensorAvail) { this.sensorsAvailL[index] = sensorAvail; }
    public void setSensorsAvailR(int index, boolean sensorAvail)  { this.sensorsAvailR[index] = sensorAvail; }
    public void setBatterylvlL(int batterylvlL) { this.batterylvlL = batterylvlL; }
    public void setBatterylvlR(int batterylvlR) { this.batterylvlR = batterylvlR; }
    public void setInsoleModeL(int insoleModeL) { this.insoleModeL = insoleModeL; }
    public void setInsoleModeR(int insoleModeR) { this.insoleModeR = insoleModeR; }
    public void setModeChangedL(boolean modeChangedL) { isModeChangedL = modeChangedL; }
    public void setModeChangedR(boolean modeChangedR) { isModeChangedR = modeChangedR; }

    public void shoeInfoRestL(){
        bluetoothDeviceL = null;
        sensorsAvailL =  new boolean[]{false, false, false};
        batterylvlL = -1;
        insoleModeL = MainActivity.MODE_UNKNOWN;
        isModeChangedL = false;
    }
    public void shoeInfoRestR(){
        bluetoothDeviceR = null;
        sensorsAvailR =  new boolean[]{false, false, false};
        batterylvlR = -1;
        insoleModeR = MainActivity.MODE_UNKNOWN;
        isModeChangedR = false;
    }
}
