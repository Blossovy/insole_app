package com.example.insoleapp;

public class PersonalInfo {
    //this class is a singleton
    private String name = "";
    private int age = 0;
    private float weight = 0, height = 0;
    private boolean isFemale = false;
    private static PersonalInfo instance;

    private PersonalInfo(){}

    public static PersonalInfo getInstance(){
        if (instance == null){
            instance = new PersonalInfo();
        }
        return instance;
    }

    public String getName(){return name;}
    public int getAge(){return age;}
    public float getWeight(){return weight;}
    public float getHeight(){return height;}
    public boolean getGender(){return isFemale;}

    public void setName(String text){name = text;}
    public void setAge(int num){age = num;}
    public void setWeight (float num) {weight = num;}
    public void setHeight (float num) {height = num;}
    public void setGender (boolean gender) {isFemale = gender;}
}
