package com.example.insoleapp;

import android.app.Service;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class BluetoothService extends Service {

	private static final String TAG = "Bluetooth Service";

	private Handler mHandler;
	private final IBinder binder = new LocalBinder();
	ShoeInfo shoeinfo = ShoeInfo.getInstance();

	//Bluetooth 1
	BluetoothSocket btSocket1;
	OutputStream outputStream1;
	InputStream inputStream1;
	Thread btThread1;
	Thread btSendThread1;
	byte[] readBuffer1;
	int readBufferPosition;
	volatile boolean stopWorker;
	public boolean firstEntry1 = true;

	//Bluetooth 2
	BluetoothSocket btSocket2;
	OutputStream outputStream2;
	InputStream inputStream2;
	Thread btThread2;
	byte[] readBuffer2;
	int readBufferPosition2;
	volatile boolean stopWorker2;
	public boolean firstEntry2 = true;

	private SensorData sensorData;
	private ExportData exportData;

	ExecutorService dataParseExecutorService = Executors.newSingleThreadExecutor();

	public BluetoothService(){
		Log.d(TAG,"Bluetooth service created");
		sensorData = SensorData.getInstance();
		exportData = new ExportData(this);
	}

	public void btCommunicateL(BluetoothSocket mmSocket, BluetoothDevice mmDevice, final InputStream mmInputStream, final OutputStream mmOutputStream){
		//outputStream1 = mmOutputStream;
		if (exportData == null){
			exportData = new ExportData(this);
		}
        exportData.startloggingL();
		Log.d(TAG,"begin for listening");
		final byte delimiter = 10; //This is the ASCII code for a newline character

		stopWorker = false;
		readBufferPosition = 0;
		readBuffer1 = new byte[1024];
		btThread1 = new Thread(new Runnable()
		{
			public void run()
			{
				Log.d(TAG,"runnable thread");
				while(!Thread.currentThread().isInterrupted() && !stopWorker) {
					try {
						int bytesAvailable = mmInputStream.available();
						if (bytesAvailable > 0) {
							if (firstEntry1){
								String logBeginTime = "Start Time:" +  System.currentTimeMillis() + "\n";
								exportData.logEntryNPL(false, logBeginTime);
								firstEntry1 = false;
							}
							byte[] packetBytes = new byte[bytesAvailable];
							mmInputStream.read(packetBytes);
							for (int i = 0; i < bytesAvailable; i++) {
								byte b = packetBytes[i];
								if (b == delimiter) {
//                                    Log.d(TAG, "delimiter");
									byte[] encodedBytes = new byte[readBufferPosition];
									System.arraycopy(readBuffer1, 0, encodedBytes, 0, encodedBytes.length);
									 String data = new String(encodedBytes, "US-ASCII");
									readBufferPosition = 0;
									Log.d(TAG, "sensorL = " + data);
									data = data.trim();
//                                    Log.d(TAG, "data2 = " + data.trim());
									if (data.matches("^T=.*#$")) {
										Log.d(TAG, "data format Left matches");
										SensorData.rawDataL = data;
										try {
										    //Log.d(TAG,"BtService Thread ID: " + Thread.currentThread().getId());
                                            final String dataT = data;

                                            Thread dataParseThread = new Thread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    //Log.d(TAG,"data parse Thread ID: " + Thread.currentThread().getId());
                                                    //Log.d(TAG, "data at handler: " + data);
                                                    SensorData.parseDataL(dataT);
                                                }
                                            });
                                            dataParseThread.start();

                                            Thread dataExportThread = new Thread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    //Log.d(TAG,"data export Thread ID: " + Thread.currentThread().getId());
                                                    exportData.logEntryNPL(true, dataT);
                                                }
                                            });
                                            dataExportThread.start();

										} catch (Exception e) {
											e.printStackTrace();
										}
									}

								} else {
									// Log.d(TAG, "size =" + readBuffer1.length);
									readBuffer1[readBufferPosition++] = b;
								}
							}
						}
						if (shoeinfo.isModeChangedL()) {
							String code = Integer.toString(shoeinfo.getInsoleModeL());
							mmOutputStream.write(code.getBytes());
							//Log.d(TAG,"get output stream sent LEFT id: "+ mmOutputStream);
							Log.d(TAG, "bluetooth actuator LEFT sent " + code);
							String logActuator = "Act: " + code + "\n";
							exportData.logEntryNPL(false, logActuator);
							shoeinfo.setModeChangedL(false);
						}
					} catch (IOException ex) {
						stopWorker = true;
					}
				}
			}
		});

		btThread1.start();

	}


	public void btCommunicateR (BluetoothSocket mmSocket2, BluetoothDevice mmDevice2, final InputStream mmInputStream2, final OutputStream mmOutputStream2){
		//outputStream2 = mmOutputStream2;
		if (exportData == null){
			exportData = new ExportData(this);
		}
		exportData.startloggingR();
//        Log.d(TAG,"begin for listening");
		final byte delimiter = 10; //This is the ASCII code for a newline character

		stopWorker2 = false;
		readBufferPosition2 = 0;
		readBuffer2 = new byte[1024];
		btThread2 = new Thread(new Runnable()
		{
			public void run()
			{
				Log.d(TAG,"runnable thread second");
				while(!Thread.currentThread().isInterrupted() && !stopWorker2)
				{
					try
					{
						int bytesAvailable = mmInputStream2.available();
						if(bytesAvailable > 0) {
							if (firstEntry2){
								String logBeginTime = "Start Time:" +  System.currentTimeMillis() + "\n";
								exportData.logEntryNPR(false, logBeginTime);
								firstEntry2 = false;
							}
							byte[] packetBytes = new byte[bytesAvailable];
							mmInputStream2.read(packetBytes);
							for(int i=0;i<bytesAvailable;i++)
							{
								byte b = packetBytes[i];
								if(b == delimiter)
								{
//                                    Log.d(TAG, "delimiter");
									byte[] encodedBytes = new byte[readBufferPosition2];
									System.arraycopy(readBuffer2, 0, encodedBytes, 0, encodedBytes.length);
									String data = new String(encodedBytes, "US-ASCII");
									readBufferPosition2 = 0;
									Log.d(TAG, "sensorR = " + data);
									data = data.trim();
									if (data.matches("^T=.*#$")) {
										Log.d(TAG, "data format Right matches");
										SensorData.rawDataR = data;
										try {
											//Log.d(TAG,"BtService Thread ID: " + Thread.currentThread().getId());
											final String dataT = data;

/*											Thread dataParseThread = new Thread(new Runnable() {
												@Override
												public void run() {
													//Log.d(TAG,"data parse Thread ID: " + Thread.currentThread().getId());
													//Log.d(TAG, "data at handler: " + data);
													SensorData.parseDataR(dataT);
												}
											});
											dataParseThread.start();*/

											Runnable dataParseRunnable = new Runnable() {
												@Override
												public void run() {
													Log.d(TAG,"data parse Thread ID: " + Thread.currentThread().getId());
													SensorData.parseDataR(dataT);

												}
											};
											dataParseExecutorService.submit(dataParseRunnable);

											Thread dataExportThread = new Thread(new Runnable() {
												@Override
												public void run() {
													//Log.d(TAG,"data export Thread ID: " + Thread.currentThread().getId());
													exportData.logEntryNPR(true, dataT);
												}
											});
											dataExportThread.start();

										} catch (Exception e) {
											e.printStackTrace();
										}
									}
								}
								else {
									readBuffer2[readBufferPosition2++] = b;
								}
							}
						}
						if (shoeinfo.isModeChangedR()) {
							String code = Integer.toString(shoeinfo.getInsoleModeR());
							mmOutputStream2.write(code.getBytes());
							Log.d(TAG, "bluetooth actuator RIGHT sent " + code);
							String logActuator = "Act: " + code + "\n";
							exportData.logEntryNPR(false, logActuator);
							//Log.d(TAG,"get output stream sent Right id: "+ mmOutputStream2);
							shoeinfo.setModeChangedR(false);
						}
					}
					catch (IOException ex)
					{
						stopWorker2 = true;
					}
				}
			}
		});
		btThread2.start();
	}


public class LocalBinder extends Binder {
		 public BluetoothService getService() {
			return BluetoothService.this;
		}
	}

	public void setHandler(Handler handler){
		mHandler = handler;
		Log.d(TAG, "set handler");
		Log.d(TAG, (mHandler == null)? "null": "not null");
	}

	@Override
	public IBinder onBind(Intent intent) {
		return binder;
	}

}